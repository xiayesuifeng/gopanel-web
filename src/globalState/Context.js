import React from 'react'

export const GlobalContext = React.createContext({
    apps: [],
    configuration: {
        general: {
            HTTPPort: 80,
            HTTPSPort: 443,
            allowH2C: false
        },
        tls: {
            dnsChallenges: {},
            wildcardDomains: []
        }
    },
    caddyModules: {
        nonStandard: {},
        standard: {}
    },
    netdataEnable: false
})