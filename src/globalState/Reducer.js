export function GlobalReducer (state, action) {
    switch (action.type) {
        case 'SET_APPS':
            return {...state, apps: action.data}
        case 'SET_CONFIGURATION':
            return {...state, configuration: action.data}
        case 'SET_NETDATA':
            return {...state, netdataEnable: action.enable}
        case 'SET_CADDY_MODULES':
            return {...state, caddyModules: action.data}
        default:
            return state
    }
}