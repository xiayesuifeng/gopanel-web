import React, { Suspense, useEffect, useReducer } from 'react'
import { Route, Routes, useNavigate } from 'react-router-dom'
import './App.css'
import CssBaseline from '@mui/material/CssBaseline'
import LoadingFrame from './component/LoadingFrame'
import { GlobalReducer } from './globalState/Reducer'
import { GlobalContext } from './globalState/Context'
import { SnackbarProvider } from 'notistack'
import { InstallApi } from './api/install'

const Home = React.lazy(() => import('./page/Home'))
const Login = React.lazy(() => import('./page/Login'))
const Install = React.lazy(() => import('./page/Install'))

function App () {
    const [globalState, globalDispatch] = useReducer(GlobalReducer, {apps: [], netdataEnable: false})

    const navigate = useNavigate()

    useEffect(() => {
        InstallApi.getStatus()
            .then(data => {
                if (!data.status)
                    navigate('/install')
            })
    }, [])

    return (
        <div className="App">
            <CssBaseline/>
            <Suspense fallback={<LoadingFrame/>}>
                <GlobalContext.Provider value={{
                    ...globalState,
                    globalDispatch
                }}>
                    <SnackbarProvider>
                        <Routes>
                            <Route path="/login" element={<Login/>}/>
                            <Route path="/install" element={<Install/>}/>
                            <Route path="/*" element={<Home/>}/>
                        </Routes>
                    </SnackbarProvider>
                </GlobalContext.Provider>
            </Suspense>
        </div>
    )
}

export default App