import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter as Router } from 'react-router-dom'
import { ThemeProvider, StyledEngineProvider, createTheme, adaptV4Theme } from '@mui/material/styles'

const theme = createTheme({
    palette: {
        primary: {
            light: '#6ec6ff',
            main: '#2196F3',
            dark: '#0069c0',
            contrastText: '#FFF',
        },
        secondary: {
            light: '#ff616f',
            main: '#ff1744',
            dark: '#c4001d',
            contrastText: '#FFF',
        },
    },
})

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(<StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme}>
        <Router>
            <App/>
        </Router>
    </ThemeProvider>
</StyledEngineProvider>)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
