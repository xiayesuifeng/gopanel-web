import {http} from './http'
import axios from 'axios'
import qs from 'qs'

const baseUrl = '/api/caddy'

export const CaddyApi = {

    getConfiguration() {
        return http.get(`${baseUrl}/configuration`)
    },

    setConfiguration(configuration) {
        return http.put(`${baseUrl}/configuration`, configuration)
    },

    getDynamicDNS() {
        return http.get(`${baseUrl}/ddns`)
    },

    setDynamicDNS(data) {
        return http.put(`${baseUrl}/ddns`, data)
    },

    getModules() {
        return http.get(`${baseUrl}/plugin/module`)
    },

    getOfficialPlugins() {
        return http.get(`${baseUrl}/plugin/repo`)
    },

    installPlugin(data) {
        return http.post(`${baseUrl}/plugin`, data)
    },

    removePlugin(packages) {
        return http.delete(`${baseUrl}/plugin`, {
            params: {
                package: packages
            },
            paramsSerializer: (params) => qs.stringify(params, { indices: false }),
        })
    }
}
