import { http } from './http'

const baseUrl = '/api/containify'

export const ContainifyApi = {
    getConfiguration () {
        return http.get(baseUrl)
    },

    setConfiguration (configuration) {
        return http.put(baseUrl, configuration)
    },

    getImageList() {
        return http.get(`${baseUrl}/images`)
    },

    removeImage(nameOrID) {
        return http.delete(`${baseUrl}/image/${nameOrID}`)
    },

    inspectImage(name) {
        return http.get(`${baseUrl}/image/${name}`)
    },

    getContainerList() {
        return http.get(`${baseUrl}/containers`)
    },

    startContainer(nameOrID) {
        return http.post(`${baseUrl}/container/${nameOrID}/start`)
    },

    restartContainer(nameOrID) {
        return http.post(`${baseUrl}/container/${nameOrID}/restart`)
    },

    stopContainer(nameOrID) {
        return http.post(`${baseUrl}/container/${nameOrID}/stop`)
    },

    createContainer(param) {
        return http.post(`${baseUrl}/container`, param)
    },
}