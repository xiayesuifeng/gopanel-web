import { http } from './http'

const baseUrl = '/api/install'

export const InstallApi = {
    install (param) {
        return http.post(baseUrl, param)
    },

    getStatus() {
        return http.get(baseUrl)
    },

}