import { http } from './http'

const baseUrl = '/api/network'

export const NetworkApi = {

    getDevices () {
        return http.get(`${baseUrl}/devices`)
    },
}