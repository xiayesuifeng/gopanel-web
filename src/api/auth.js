import { http } from './http'

const baseUrl = '/api/auth'

export const AuthApi = {
    login (password) {
        return http.post(baseUrl+ '/login', {
            'password': password
        })
    },

    getToken() {
        return http.get(baseUrl + '/token')
    },

}