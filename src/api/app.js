import { http } from './http'

const baseUrl = '/api/app'

export const AppApi = {

    getApp () {
        return http.get(baseUrl)
    },

    addApp (param) {
        return http.post(baseUrl, param)
    },

    editApp (name, param) {
        return http.put(`${baseUrl}/${name}`, param)
    },

    deleteApp (name) {
        return http.delete(`${baseUrl}/${name}`)
    }
}