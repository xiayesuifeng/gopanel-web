import { http } from './http'

const baseUrl = '/api/service'

export const ServiceApi = {

    getService () {
        return http.get(baseUrl)
    },

    startService (name) {
        return http.post(`${baseUrl}/${name}/start`)
    },

    stopService (name,stopTriggeredBy) {
        return http.post(`${baseUrl}/${name}/stop?stopTriggeredBy=${stopTriggeredBy}`)
    },

    restartService (name) {
        return http.post(`${baseUrl}/${name}/restart`)
    },

    enableService (name) {
        return http.post(`${baseUrl}/${name}/enable`)
    },

    disableService (name) {
        return http.post(`${baseUrl}/${name}/disable`)
    },

}