import axios from 'axios'
import Cookies from 'js-cookie'

const request = function (url, params, method) {
    return new Promise((resolve, reject) => {
        axios[method](url, params)
            .then(response => {
                resolve(response.data)
            })
            .catch(err => {
                if (err.response.data.message)
                    reject(new Error(err.response.data.message))
                else
                    reject(err)
            })
    })
}

export const http = {
    get(url) {
        return request(url, undefined, 'get')
    },

    post(url, params = undefined) {
        return request(url, params, 'post')
    },

    put(url, params = undefined) {
        return request(url, params, 'put')
    },

    patch(url, params = undefined) {
        return request(url, params, 'patch')
    },

    delete(url, param = undefined) {
        return request(url, param, 'delete')
    }
}

let isRefreshing = false
let requests = []

axios.interceptors.request.use(config => {
    let token = Cookies.get('token')
    config.headers['Authorization'] = token

    if (config.url.indexOf('/api/auth/token') >= 0 || config.url.indexOf('/api/auth/login') >= 0) {
        return config
    }

    let tokenExpire = Cookies.get('tokenExpireTime')

    if (token && tokenExpire) {
        let now = Date.now()
        if (tokenExpire - now > 0 && tokenExpire - now < 18000000) {
            if (!isRefreshing) {
                isRefreshing = true
                http.get('/api/auth/token').then(res => {
                    const {token} = res.data
                    let time = new Date()
                    time.setDate(time.getDate() + 3)

                    Cookies.set('token', token, {expires: 3})
                    Cookies.set('tokenExpireTime', time.valueOf(), {expires: 3})
                    isRefreshing = false
                    return token
                }).then(token => {
                    requests.forEach(cb => cb(token))
                    requests = []
                }).catch(() => {
                    isRefreshing = false
                    requests.forEach(cb => cb(token))
                    requests = []
                })
            }
            return new Promise((resolve) => {
                requests.push((token) => {
                    config.headers['Authorization'] = token
                    resolve(config)
                })
            })
        }
    }
    return config
})