import { http } from './http'

const baseUrl = '/api/firewall'

export const FirewallApi = {
    getConfig () {
        return http.get(`${baseUrl}`)
    },

    updateConfig (config) {
        return http.post(`${baseUrl}`, config)
    },

    reload () {
        return http.post(`${baseUrl}/reload`)
    },

    reset () {
        return http.post(`${baseUrl}/reset`)
    },

    getZoneNames (permanent) {
        return http.get(`${baseUrl}/zone/names?permanent=${permanent}`)
    },

    getZones (permanent) {
        return http.get(`${baseUrl}/zone?permanent=${permanent}`)
    },

    getZone (name, permanent) {
        return http.get(`${baseUrl}/zone/${name}?permanent=${permanent}`)
    },

    addZone (zone) {
        return http.post(`${baseUrl}/zone`, zone)
    },

    updateZone (name, zone, permanent) {
        return http.put(`${baseUrl}/zone/${name}?permanent=${permanent}`, zone)
    },

    removeZone (name) {
        return http.delete(`${baseUrl}/zone/${name}`)
    },

    getTrafficRules (zone, permanent) {
        return http.get(`${baseUrl}/zone/${zone}/trafficRule?permanent=${permanent}`)
    },

    addTrafficRule (zone, rule, permanent) {
        return http.post(`${baseUrl}/zone/${zone}/trafficRule?permanent=${permanent}`, rule)
    },

    removeTrafficRule (zone, rule, permanent) {
        return http.delete(`${baseUrl}/zone/${zone}/trafficRule?permanent=${permanent}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(rule)
        })
    },

    getPortForwards (zone, permanent) {
        return http.get(`${baseUrl}/zone/${zone}/portForward?permanent=${permanent}`)
    },

    addPortForward (zone, portForward, permanent) {
        return http.post(`${baseUrl}/zone/${zone}/portForward?permanent=${permanent}`, portForward)
    },

    removePortForward (zone, portForward, permanent) {
        return http.delete(`${baseUrl}/zone/${zone}/portForward?permanent=${permanent}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(portForward)
        })
    },

    getServiceNames (permanent) {
        return http.get(`${baseUrl}/service/names?permanent=${permanent}`)
    },

    getICMPTypeNames (permanent) {
        return http.get(`${baseUrl}/icmpType/names?permanent=${permanent}`)
    },

    getPolicies (permanent) {
        return http.get(`${baseUrl}/policy?permanent=${permanent}`)
    },

    addPolicy (policy) {
        return http.post(`${baseUrl}/policy`, policy)
    },

    updatePolicy (name, policy, permanent) {
        return http.put(`${baseUrl}/policy/${name}?permanent=${permanent}`, policy)
    },

}
