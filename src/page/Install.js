import React, { useEffect, useState } from 'react'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'

import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import {
    StepContent,
    ToggleButtonGroup,
    ToggleButton,
    FormLabel,
    FormGroup,
    FormHelperText,
    Stack
} from '@mui/material'
import CheckBox from '@mui/icons-material/CheckBox'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import { elGR } from '@mui/material/locale'
import CircularProgress from '@mui/material/CircularProgress'
import { InstallApi } from '../api/install'

const image = {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme => theme.palette.mode === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
}
const paper = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
}

const CustomTextField = (props) => {
    const {name, label, type, autoComplete} = props

    const {data, setData} = props.data
    const {message, setMessage} = props.message

    return (<TextField
        variant="outlined"
        margin="normal"
        fullWidth
        type={type}
        autoComplete={autoComplete}
        label={message[name] ? message[name] : label}
        error={!!message[name]}
        value={data[name] ?? ''}
        name={name}
        onChange={e => {
            if (setMessage)
                setMessage({})
            if (setData)
                if (type === 'number')
                    setData({...data, [name]: Number(e.target.value)})
                else {
                    setData({...data, [name]: e.target.value})
                }
        }}
    />)
}

export default function Install (props) {
    const [data, setData] = useState({
        caddyAPI: 'unix//run/caddy/admin.socket',
        caddyConf: '/etc/caddy/conf.d',
        caddyData: '/var/lib/caddy',
        httpPort: 80,
        httpsPort: 443,
        disableSSL: false
    })

    const [message, setMessage] = useState({})

    const [activeStep, setActiveStep] = useState(0)

    const [accessMethod, setAccessMethod] = useState(0)

    const [installing, setInstalling] = useState(false)
    const [installError, setInstallError] = useState('')
    const [redirectURL, setRedirectURL] = useState('')
    const [waitOnline, setWaitOnline] = useState(false)
    const [redirectCountdown, setRedirectCountdown] = useState(3)

    useEffect(() => {
        const interval = setInterval(() => {
            if (redirectURL) {
                if (redirectCountdown > 0) {
                    setRedirectCountdown(redirectCountdown - 1)
                } else {
                    window.location.href = redirectURL
                }
            }
        }, 1000)

        return () => clearInterval(interval)
    }, [redirectCountdown, redirectURL])

    const handleInstall = () => {
        setInstalling(true)
        setRedirectURL('')
        setInstallError('')

        let param = {
            password: data.password,
            caddy: {
                httpPort: data.httpPort,
                httpsPort: data.httpsPort,
                adminAddress: data.caddyAPI,
                confPath: data.caddyConf,
                dataPath: data.caddyData
            },
            panel: {
                disableSSL: data.disableSSL
            }
        }

        if (accessMethod === 0)
            param.panel.domain = data.domain
        else
            param.panel.port = data.port

        InstallApi.install(param)
            .then(async data => {
                setWaitOnline(true)
                const startTime = Date.now()
                while (true) {
                    try {
                        const response = await InstallApi.getStatus()
                        console.log(response)
                        if (response.status) {
                            setRedirectURL(data.redirectURL)
                        } else if (response.lastError) {
                            setInstallError(response.lastError)
                        } else {
                            setInstallError('未知错误')
                        }
                        break
                    } catch (error) {
                    }
                    if (Date.now() - startTime > 60000) {
                        setRedirectURL(data.redirectURL)
                        break
                    }
                    await new Promise((resolve) => setTimeout(resolve, 1000))
                }

                setWaitOnline(false)
            })
            .catch(err => setInstallError(err.message))
            .finally(() => setInstalling(false))
    }

    const handleNext = () => {
        let next = false
        // eslint-disable-next-line default-case
        switch (activeStep) {
            case 0:
                next = checkLoginConfig()
                break
            case 1:
                next = checkCaddyConfig()
                break
            case 2:
                next = checkPanelConfig()
                break
        }
        if (next)
            setActiveStep((prevActiveStep) => prevActiveStep + 1)

        if (activeStep === 2 && next)
            handleInstall()
    }

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1)
    }

    const checkLoginConfig = () => {
        if (!data.password) {
            setMessage({password: '密码不能为空'})
            return false
        }

        if (!data.againPassword) {
            setMessage({againPassword: '确认密码不能为空'})
            return false
        }

        if (data.password !== data.againPassword) {
            setMessage({againPassword: '确认密码不一致'})
            return false
        }

        return true
    }

    const checkCaddyConfig = () => {
        let result = true

        let message = {}

        if (!data.httpPort) {
            message = {httpPort: 'HTTP 端口不能为 0'}
            result = false
        }

        if (!data.httpsPort) {
            message = {httpsPort: 'HTTPS 端口不能为 0', ...message}
            result = false
        }

        if (!data.caddyAPI) {
            message = {caddyAPI: 'Caddy Admin API 地址不能为空', ...message}
            result = false
        }

        setMessage(message)

        return result
    }

    const checkPanelConfig = () => {
        if (accessMethod === 0 && !data.domain) {
            setMessage({domain: '域名不能为空'})
            return false
        } else if (accessMethod === 1 && !data.port) {
            setMessage({port: '端口不能为空'})
            return false
        }

        return true
    }

    return (
        <Grid container component="main" sx={{
            height: '100vh',
            textAlign: 'left'
        }}>
            <Grid item xs={false} sm={5} md={7} sx={image}/>
            <Grid item xs={12} sm={7} md={5} component={Paper} elevation={6} square>
                <Box sx={paper}>
                    <Box display="flex" alignItems="center" flexDirection="column" width={'100%'}>
                        <Typography variant="h1">
                            GoPanel
                        </Typography>
                        <Typography variant="h5">
                            安装向导
                        </Typography>
                        <Box sx={{
                            width: '100%',
                            mt: 3,
                            p: {
                                xs: 3,
                                xl: 15
                            }
                        }}>
                            <Stepper activeStep={activeStep} orientation="vertical">
                                <Step>
                                    <StepLabel>登录设置</StepLabel>
                                    <StepContent>
                                        <CustomTextField
                                            name={'password'}
                                            type="password"
                                            autoComplete="current-password"
                                            label={'密码'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                        <CustomTextField
                                            label={'确认密码'}
                                            type="password"
                                            name={'againPassword'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                    </StepContent>
                                </Step>
                                <Step>
                                    <StepLabel>Caddy 设置</StepLabel>
                                    <StepContent>
                                        <CustomTextField
                                            type={'number'}
                                            name={'httpPort'}
                                            label={'HTTP 端口'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                        <CustomTextField
                                            type={'number'}
                                            name={'httpsPort'}
                                            label={'HTTPS 端口'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                        <CustomTextField
                                            name={'caddyAPI'}
                                            label={'Admin API 地址'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                        <CustomTextField
                                            name={'caddyConf'}
                                            label={'conf.d 路径'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                        <CustomTextField
                                            name={'caddyData'}
                                            label={'存储路径'}
                                            message={{message: message, setMessage: setMessage}}
                                            data={{data: data, setData: setData}}
                                        />
                                    </StepContent>
                                </Step>
                                <Step>
                                    <StepLabel
                                        optional={
                                            <Typography variant="caption">最后一步</Typography>
                                        }
                                    >
                                        面板设置
                                    </StepLabel>
                                    <StepContent>
                                        <FormLabel component="legend">访问方式</FormLabel>
                                        <FormGroup>
                                            <ToggleButtonGroup
                                                color="primary"
                                                value={accessMethod}
                                                exclusive
                                                size={'small'}
                                                onChange={(event, value) => {if (value != null) setAccessMethod(value)}}
                                            >
                                                <ToggleButton value={0}>域名</ToggleButton>
                                                <ToggleButton value={1}>端口</ToggleButton>
                                            </ToggleButtonGroup>
                                            {accessMethod === 0 && <>
                                                <CustomTextField
                                                    name={'domain'}
                                                    label={'域名'}
                                                    message={{message: message, setMessage: setMessage}}
                                                    data={{data: data, setData: setData}}
                                                />
                                                <FormControlLabel control={<Switch checked={data.disableSSL}
                                                                                   onChange={e => setData({
                                                                                       ...data,
                                                                                       disableSSL: e.target.checked
                                                                                   })}/>}
                                                                  label="禁用 SSL"/>
                                            </>}
                                            {accessMethod === 1 && <CustomTextField
                                                type={'number'}
                                                name={'port'}
                                                label={'端口'}
                                                message={{message: message, setMessage: setMessage}}
                                                data={{data: data, setData: setData}}
                                            />}
                                            <FormHelperText>不推荐禁用(仅当无法自动申请证书时禁用)</FormHelperText>
                                        </FormGroup>
                                    </StepContent>
                                </Step>
                                <Step>
                                    {installing ? <StepLabel icon={<CircularProgress size={20}/>}>安装中...</StepLabel>
                                        : <StepLabel>完成安装</StepLabel>}
                                    <StepContent>
                                        <Stack alignItems={'center'} spacing={1}>
                                            {installError && <>
                                                <Typography>安装失败: {installError}</Typography>
                                                <Button color={'error'} onClick={() => setActiveStep(0)}>重试</Button>
                                            </>}
                                            {waitOnline && <Typography>等待服务端重启</Typography>}
                                            {!installing && !waitOnline && !installError &&
                                                <>
                                                    <Typography>即将在 {redirectCountdown}s 后跳转至:</Typography>
                                                    <Typography component={'a'} target={'_blank'}
                                                                href={redirectURL}>{redirectURL}</Typography>
                                                </>}
                                        </Stack>
                                    </StepContent>
                                </Step>
                            </Stepper>

                            {activeStep !== 3 &&
                                <Box sx={{mb: 2}} display={'flex'} justifyContent={'flex-end'}>
                                    <Button
                                        disabled={activeStep === 0}
                                        onClick={handleBack}
                                        sx={{mt: 1, mr: 1}}
                                    >
                                        返回
                                    </Button>
                                    <Button
                                        variant="contained"
                                        onClick={handleNext}
                                        sx={{mt: 1, mr: 1}}
                                    >
                                        {activeStep === 2 ? '开始安装' : '继续'}
                                    </Button>
                                </Box>}
                        </Box>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    )
}