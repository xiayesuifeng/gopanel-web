import React, { useEffect, useState } from 'react'
import TabList from '@mui/lab/TabList'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import TabContext from '@mui/lab/TabContext'
import TabPanel from '@mui/lab/TabPanel'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import { ContainifyApi } from '../api/containify'
import { useSnackbar } from 'notistack'
import TextField from '@mui/material/TextField'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import CircularProgress from '@mui/material/CircularProgress'
import DoneIcon from '@mui/icons-material/Done'
import Fab from '@mui/material/Fab'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Button from '@mui/material/Button'
import Chip from '@mui/material/Chip'
import IconButton from '@mui/material/IconButton'
import DeleteIcon from '@mui/icons-material/Delete'
import StopIcon from '@mui/icons-material/Stop'
import PlayArrowIcon from '@mui/icons-material/PlayArrow'
import TerminalIcon from '@mui/icons-material/Terminal'
import SettingsIcon from '@mui/icons-material/Settings'
import RestartAltIcon from '@mui/icons-material/RestartAlt'
import AddIcon from '@mui/icons-material/Add'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import { Stack, ToggleButton, ToggleButtonGroup } from '@mui/material'
import Typography from '@mui/material/Typography'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import AccordionDetails from '@mui/material/AccordionDetails'
import useMediaQuery from '@mui/material/useMediaQuery'
import { useTheme } from '@mui/material/styles'
import CloseIcon from '@mui/icons-material/Close'
import ErrorIcon from '@mui/icons-material/Error'
import LabelPaper from '../component/LabelPaper'
import Paper from '@mui/material/Paper'

const fabStyle = {
    position: 'fixed',
    bottom: 24,
    right: 24,
    zIndex: (theme) => theme.zIndex.fab
}

export default function Containify () {
    const [tabValue, setTabValue] = useState('1')
    const [enabled, setEnabled] = useState(false)
    const [containerEngine, setContainerEngine] = useState('')
    const [containerEngineSetting, setContainerEngineSetting] = useState({})

    const [loading, setLoading] = useState(false)

    const {enqueueSnackbar} = useSnackbar()

    useEffect(() => getConfiguration(), [])

    const getConfiguration = () => {
        ContainifyApi.getConfiguration()
            .then(data => {
                setEnabled(data.enabled)
                setContainerEngine(data.containerEngine)
                setContainerEngineSetting(data.containerEngineSetting)

                if (!data.enabled && tabValue !== '4')
                    setTabValue('4')
            })
            .catch(msg => enqueueSnackbar('获取配置失败，错误:' + msg, {variant: 'error'}))
    }

    const handleDoneFabClick = () => {
        if (enabled) {
            if (!containerEngine) {
                enqueueSnackbar('容器引擎不能为空', {variant: 'error'})
                return
            }

            if (!containerEngineSetting.endpoint) {
                enqueueSnackbar('API 端点不能为空', {variant: 'error'})
                return
            }
        }

        setLoading(true)

        ContainifyApi.setConfiguration({
            enabled: enabled,
            containerEngine: containerEngine,
            containerEngineSetting: containerEngineSetting
        }).then(data => {
            enqueueSnackbar('更新成功', {variant: 'success'})

            getConfiguration()
        }).catch(err => enqueueSnackbar('更新失败，错误:' + err, {variant: 'error'}))
            .finally(() => setLoading(false))
    }

    return (
        <Box>
            <TabContext value={tabValue}>
                <Paper sx={{borderRadius: 4, pl: 1, pr: 1}}>
                    <TabList onChange={(event, newValue) => setTabValue(newValue)}>
                        <Tab label={'容器'} value={'1'} disabled={!enabled}/>
                        <Tab label={'镜像'} value={'2'} disabled={!enabled}/>
                        <Tab label={'配置'} value={'4'}/>
                    </TabList>
                </Paper>
                <ContainerTabPanel engineEnabled={enabled}/>
                <ImagesTabPanel engineEnabled={enabled}/>
                <TabPanel value={'4'} sx={{p: 0}}>
                    <LabelPaper>
                        <FormControlLabel control={<Switch/>} label="启用"
                                          checked={enabled}
                                          onChange={e => setEnabled(e.target.checked)}/>
                        <FormControl margin={'normal'} sx={{minWidth: 150}}>
                            <InputLabel>容器引擎</InputLabel>
                            <Select
                                value={containerEngine}
                                disabled={!enabled}
                                onChange={e => setContainerEngine(e.target.value)}
                            >
                                <MenuItem value={'podman'}>Podman</MenuItem>
                            </Select>
                        </FormControl>
                        <TextField
                            sx={{minWidth: 500}}
                            margin={'normal'}
                            label={'API 端点'}
                            helperText={'例: unix:///var/run/podman/podman.sock'}
                            value={containerEngineSetting.endpoint ?? ''}
                            onChange={e => setContainerEngineSetting({
                                ...containerEngineSetting,
                                endpoint: e.target.value
                            })}
                            disabled={!enabled}/>
                    </LabelPaper>
                    <Fab color={'primary'} sx={fabStyle} onClick={handleDoneFabClick} disabled={loading}>
                        {loading ? <CircularProgress size={31}/> : <DoneIcon/>}
                    </Fab>
                </TabPanel>
            </TabContext>
        </Box>
    )
}

function ContainerTabPanel (props) {
    const {engineEnabled} = props

    const {enqueueSnackbar} = useSnackbar()

    const [containers, setContainers] = useState([])

    const [starting, setStarting] = useState({})
    const [restarting, setRestarting] = useState({})
    const [stopping, setStopping] = useState({})

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'))

    const [dialogOpen, setDialogOpen] = useState(false)
    const [image, setImage] = useState('')
    const [activeStep, setActiveStep] = useState(0)
    const [inspecting, setInspecting] = useState(false)
    const [containerInfo, setContainerInfo] = useState({})
    const [newContainerParam, setNewContainerParam] = useState({})
    const [createError, setCreateError] = useState('')

    useEffect(() => {
        if (engineEnabled) {
            getContainerList()
        }
    }, [engineEnabled])

    const getContainerList = () => {
        ContainifyApi.getContainerList()
            .then(data => setContainers(data))
            .catch(err => enqueueSnackbar('获取容器失败，错误:' + err, {variant: 'error'}))
    }

    const getStateString = state => {
        switch (state) {
            case 'running':
                return '正在运行'
            case 'exited':
                return '已停止'
            default:
                return state
        }
    }

    const handleStartClick = nameOrID => {
        starting[nameOrID] = true
        setStarting(starting)

        ContainifyApi.startContainer(nameOrID)
            .then(() => getContainerList())
            .catch(err => enqueueSnackbar('启动容器失败，错误:' + err, {variant: 'error'}))
            .finally(() => {
                starting[nameOrID] = false
                setStarting(starting)
            })
    }

    const handleStopClick = nameOrID => {
        stopping[nameOrID] = true
        setStopping(stopping)

        ContainifyApi.stopContainer(nameOrID)
            .then(() => getContainerList())
            .catch(err => enqueueSnackbar('停止容器失败，错误:' + err, {variant: 'error'}))
            .finally(() => {
                stopping[nameOrID] = false
                setStopping(stopping)
            })
    }

    const handleRestartClick = nameOrID => {
        restarting[nameOrID] = true
        setRestarting(restarting)

        ContainifyApi.restartContainer(nameOrID)
            .then(() => getContainerList())
            .catch(err => enqueueSnackbar('重启容器失败，错误:' + err, {variant: 'error'}))
            .finally(() => {
                restarting[nameOrID] = false
                setRestarting(restarting)
            })
    }

    const handleCreateStepNextClick = () => {
        switch (activeStep) {
            case 0:
                if (image) {
                    let strs = image.split('/')
                    let name = strs[strs.length - 1].split(':')[0]
                    let i = 1
                    for (; containers.find(c => c.name === `${name}-${i}`); i++) {
                    }

                    setContainerInfo({
                        name: `${name}-${i}`,
                        image: image,
                        env: {},
                        ports: [],
                        mounts: [],
                        labels: {},
                        newLabels: [],
                        newEnv: []
                    })

                    setActiveStep(activeStep + 1)
                    setInspecting(true)
                    ContainifyApi.inspectImage(image)
                        .then(data => {
                            let env = Object.fromEntries(data.env?.map(e => e.split('=')))

                            let ports = Object.keys(data.exposedPorts ?? {}).map(port => {
                                let tmp = port.split('/')
                                return {
                                    containerPort: Number(tmp[0]),
                                    hostPort: Number(tmp[0]),
                                    protocol: tmp[1]
                                }
                            })

                            let volumes = Object.keys(data.volumes ?? {}).map(volume => {
                                return {
                                    type: 'bind',
                                    destination: volume,
                                    source: '',
                                    rw: true,
                                }
                            })

                            setContainerInfo({
                                name: `${name}-${i}`,
                                image: image,
                                entrypoint: data.entrypoint ?? [],
                                command: data.cmd ?? [],
                                env: env,
                                ports: ports,
                                mounts: volumes,
                                labels: data.labels ?? {},
                                newLabels: [],
                                newEnv: []
                            })
                        })
                        .catch(err => enqueueSnackbar('检索镜像失败，错误:' + err, {variant: 'error'}))
                        .finally(() => setInspecting(false))
                }
                break
            case 1:
                let env = containerInfo.env
                containerInfo.newEnv.filter(env => env.key !== '').map(e => env[e.key] = e.value)

                let labels = containerInfo.labels
                containerInfo.newLabels.filter(env => env.key !== '').map(e => env[e.key] = e.value)

                setNewContainerParam({
                    name: containerInfo.name,
                    image: containerInfo.image,
                    entrypoint: containerInfo.entrypoint.filter(entrypoint => entrypoint !== ''),
                    command: containerInfo.command.filter(entrypoint => entrypoint !== ''),
                    env: env,
                    labels: labels,
                    ports: containerInfo.ports.filter(port => port.hostPort !== ''),
                    mounts: containerInfo.mounts.filter(mount => (mount.type === 'bind' && mount.source !== '') || (mount.type === 'volume' && mount.name !== '')),
                })
                setCreateError('')
                setActiveStep(activeStep + 1)
                break
            case 2:
                setActiveStep(activeStep + 1)
                ContainifyApi.createContainer(newContainerParam)
                    .then(() => {
                        getContainerList()
                        setDialogOpen(false)
                    })
                    .catch(err => setCreateError(err.toString()))
                break
        }
    }

    return (
        <TabPanel value={'1'} sx={{p: 0}}>
            <TableContainer component={LabelPaper}>
                <Table stickyHeader sx={{minWidth: 650}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>名称</TableCell>
                            <TableCell align={'center'}>状态</TableCell>
                            <TableCell align={'center'}>关联应用</TableCell>
                            <TableCell>镜像</TableCell>
                            <TableCell>端口</TableCell>
                            <TableCell align={'center'}>操作</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {containers.map((container) => {
                            return (<TableRow
                                key={container.id}
                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                            >
                                <TableCell component="th" scope="row">
                                    {container.name}
                                </TableCell>
                                <TableCell align={'center'}>{getStateString(container.state)}</TableCell>
                                <TableCell align={'center'}>--</TableCell>
                                <TableCell>{container.image}</TableCell>
                                <TableCell>
                                    {container.ports.map(port => {
                                        let hostIP = port.hostIP
                                        if (!hostIP)
                                            hostIP = '0.0.0.0'

                                        return <Chip key={port.containerPort} size="small" variant="outlined"
                                                     color="info" sx={{ml: 0.5, mr: 0.5}}
                                                     label={`${hostIP}:${port.hostPort} -> ${port.containerPort}/${port.protocol}`}/>
                                    })}
                                </TableCell>
                                <TableCell align={'center'}>
                                    <IconButton disabled={container.state === 'running' || starting[container.id]}
                                                onClick={() => handleStartClick(container.id)}>
                                        {starting[container.id] ? <CircularProgress size={22}/> : <PlayArrowIcon/>}
                                    </IconButton>
                                    <IconButton disabled={container.state === 'exited' || stopping[container.id]}
                                                onClick={() => handleStopClick(container.id)}>
                                        {stopping[container.id] ? <CircularProgress size={22}/> : <StopIcon/>}
                                    </IconButton>
                                    <IconButton disabled={restarting[container.id]}
                                                onClick={() => handleRestartClick(container.id)}>
                                        {restarting[container.id] ? <CircularProgress size={22}/> : <RestartAltIcon/>}
                                    </IconButton>
                                    <IconButton disabled={container.state === 'exited'}>
                                        <TerminalIcon/>
                                    </IconButton>
                                    <IconButton>
                                        <DeleteIcon/>
                                    </IconButton>
                                    <IconButton>
                                        <SettingsIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <Dialog open={dialogOpen}
                    onClose={() => activeStep !== 3 && setDialogOpen(false)}
                    fullWidth maxWidth={'md'}
                    fullScreen={fullScreen}>
                <DialogTitle>创建容器</DialogTitle>
                {activeStep !== 3 && <IconButton
                    onClick={() => setDialogOpen(false)}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon/>
                </IconButton>}
                <DialogContent>
                    <Stepper activeStep={activeStep} sx={{m: 2}}>
                        <Step>
                            <StepLabel>{'选择镜像'}</StepLabel>
                        </Step>
                        <Step>
                            {inspecting ? <StepLabel icon={<CircularProgress size={20}/>}>检索镜像中...</StepLabel>
                                : <StepLabel>配置容器</StepLabel>}
                        </Step>
                        <Step>
                            <StepLabel>摘要</StepLabel>
                        </Step>
                        <Step>
                            <StepLabel>创建容器</StepLabel>
                        </Step>
                    </Stepper>
                    {activeStep === 0 && <TextField label={'镜像'} fullWidth helperText={'image:version'} value={image}
                                                    onChange={e => setImage(e.target.value)}/>}
                    {activeStep === 1 && !inspecting && <>
                        <TextField fullWidth label={'名称'} value={containerInfo.name} margin="normal"
                                   onChange={e => setContainerInfo({...containerInfo, name: e.target.value})}/>
                        <TextField fullWidth label={'Entrypoint'} value={containerInfo.entrypoint?.join(' ') ?? ''}
                                   margin="normal"
                                   onChange={e => setContainerInfo({
                                       ...containerInfo,
                                       entrypoint: e.target.value.split(' ')
                                   })}/>
                        <TextField fullWidth label={'Command'} value={containerInfo.command?.join(' ') ?? ''}
                                   margin="normal"
                                   onChange={e => setContainerInfo({
                                       ...containerInfo,
                                       command: e.target.value.split(' ')
                                   })}/>
                        <Typography>端口</Typography>
                        {containerInfo.ports.map((port, index) => <Stack key={index} direction="row" spacing={2} mt={2}>
                            <TextField fullWidth label={'本机端口'} value={port.hostPort} size={'small'}
                                       type="number"
                                       onChange={e => {
                                           containerInfo.ports[index].hostPort = Number(e.target.value)
                                           setContainerInfo({
                                               ...containerInfo,
                                               ports: containerInfo.ports,
                                           })
                                       }}/>
                            <TextField fullWidth label={'容器端口'} value={port.containerPort} size={'small'}
                                       type="number"
                                       onChange={e => {
                                           containerInfo.ports[index].containerPort = Number(e.target.value)
                                           setContainerInfo({
                                               ...containerInfo,
                                               ports: containerInfo.ports,
                                           })
                                       }}/>
                            <FormControl sx={{minWidth: 100}} size={'small'}>
                                <InputLabel>协议</InputLabel>
                                <Select
                                    value={port.protocol}
                                    onChange={e => {
                                        containerInfo.ports[index].protocol = e.target.value
                                        setContainerInfo({
                                            ...containerInfo,
                                            ports: containerInfo.ports,
                                        })
                                    }}
                                >
                                    <MenuItem value={'tcp'}>TCP</MenuItem>
                                    <MenuItem value={'udp'}>UDP</MenuItem>
                                </Select>
                            </FormControl>
                            <IconButton onClick={() => {
                                containerInfo.ports.splice(index, 1)
                                setContainerInfo({
                                    ...containerInfo,
                                    ports: containerInfo.ports
                                })
                            }}>
                                <DeleteIcon/>
                            </IconButton>
                        </Stack>)}
                        <Button onClick={() => setContainerInfo({
                            ...containerInfo,
                            ports: [...containerInfo.ports, {hostPort: '', containerPort: '', protocol: 'tcp'}]
                        })}>添加</Button>
                        <Typography>挂载</Typography>
                        {containerInfo.mounts.map((mount, index) => <Stack key={index} spacing={2} mt={2}>

                            <ToggleButtonGroup
                                color="primary"
                                value={mount.type}
                                exclusive
                                sx={{minWidth: 150}}
                                onChange={e => {
                                    containerInfo.mounts[index].type = e.target.value
                                    setContainerInfo({...containerInfo, mounts: containerInfo.mounts})
                                }}
                            >
                                <ToggleButton value="bind">本机路径</ToggleButton>
                                <ToggleButton value="volume">存储卷</ToggleButton>
                            </ToggleButtonGroup>
                            <Stack direction="row" spacing={2}>
                                {mount.type === 'bind'
                                    ? <TextField fullWidth label={'本机路径'} value={mount.source} size={'small'}
                                                 onChange={e => {
                                                     containerInfo.mounts[index].source = e.target.value
                                                     setContainerInfo({
                                                         ...containerInfo,
                                                         mounts: containerInfo.mounts,
                                                     })
                                                 }}/>
                                    : <TextField fullWidth label={'存储卷名'} value={mount.name} size={'small'}
                                                 onChange={e => {
                                                     containerInfo.mounts[index].name = e.target.value
                                                     setContainerInfo({
                                                         ...containerInfo,
                                                         mounts: containerInfo.mounts,
                                                     })
                                                 }}/>}
                                <TextField fullWidth label={'容器路径'} value={mount.destination} size={'small'}
                                           onChange={e => {
                                               containerInfo.mounts[index].destination = e.target.value
                                               setContainerInfo({
                                                   ...containerInfo,
                                                   mounts: containerInfo.mounts,
                                               })
                                           }}/>
                                <FormControl sx={{minWidth: 100}} size={'small'}>
                                    <InputLabel>权限</InputLabel>
                                    <Select
                                        value={mount.rw}
                                        onChange={e => {
                                            containerInfo.mounts[index].rw = e.target.value
                                            setContainerInfo({
                                                ...containerInfo,
                                                mounts: containerInfo.mounts
                                            })
                                        }}
                                    >
                                        <MenuItem value={true}>读写</MenuItem>
                                        <MenuItem value={false}>只读</MenuItem>
                                    </Select>
                                </FormControl>
                                <IconButton onClick={() => {
                                    containerInfo.mounts.splice(index, 1)
                                    setContainerInfo({
                                        ...containerInfo,
                                        mounts: containerInfo.mounts
                                    })
                                }}>
                                    <DeleteIcon/>
                                </IconButton>
                            </Stack>
                        </Stack>)}
                        <Button onClick={() => setContainerInfo({
                            ...containerInfo,
                            mounts: [...containerInfo.mounts, {source: '', destination: '', rw: true, type: 'bind'}]
                        })}>添加</Button>
                        <Accordion>
                            <AccordionSummary
                                expandIcon={<ArrowDropDownIcon/>}
                            >
                                <Typography>标签</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                {Object.keys(containerInfo.labels).map(key => <Stack key={key} direction="row"
                                                                                     spacing={2} mt={2}>
                                    <TextField fullWidth label={'名称'} value={key} size={'small'} disabled/>
                                    <TextField fullWidth label={'值'} value={containerInfo.labels[key]} size={'small'}
                                               onChange={e => {
                                                   containerInfo.labels[key] = e.target.value
                                                   setContainerInfo({
                                                       ...containerInfo,
                                                       labels: containerInfo.labels,
                                                   })
                                               }}/>
                                    <IconButton onClick={() => {
                                        delete containerInfo.labels[key]
                                        setContainerInfo({...containerInfo, labels: containerInfo.labels})
                                    }}><DeleteIcon/></IconButton>
                                </Stack>)}
                                {containerInfo.newLabels.map((label, index) => <Stack key={index} direction="row"
                                                                                      spacing={2} mt={2}>
                                    <TextField fullWidth label={'名称'} value={label.key} size={'small'}
                                               onChange={e => {
                                                   containerInfo.newLabels[index].key = e.target.value
                                                   setContainerInfo({
                                                       ...containerInfo,
                                                       newLabels: containerInfo.newLabels,
                                                   })
                                               }}/>
                                    <TextField fullWidth label={'值'} value={label.value} size={'small'}
                                               onChange={e => {
                                                   containerInfo.newLabels[index].value = e.target.value
                                                   setContainerInfo({
                                                       ...containerInfo,
                                                       newLabels: containerInfo.newLabels,
                                                   })
                                               }}/>
                                    <IconButton onClick={() => {
                                        containerInfo.newLabels.splice(index, 1)
                                        setContainerInfo({
                                            ...containerInfo,
                                            newLabels: containerInfo.newLabels
                                        })
                                    }}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </Stack>)}
                                <Button onClick={() => setContainerInfo({
                                    ...containerInfo,
                                    newLabels: [...containerInfo.newLabels, {key: '', value: ''}]
                                })}>添加</Button>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary
                                expandIcon={<ArrowDropDownIcon/>}
                            >
                                <Typography>环境变量</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                {Object.keys(containerInfo.env).map(key => <Stack key={key} direction="row" spacing={2}
                                                                                  mt={2}>
                                    <TextField fullWidth label={'名称'} value={key} size={'small'} disabled/>
                                    <TextField fullWidth label={'值'} value={containerInfo.env[key]} size={'small'}
                                               onChange={e => {
                                                   containerInfo.env[key] = e.target.value
                                                   setContainerInfo({
                                                       ...containerInfo,
                                                       env: containerInfo.env,
                                                   })
                                               }}/>
                                    <IconButton onClick={() => {
                                        delete containerInfo.env[key]
                                        setContainerInfo({...containerInfo, env: containerInfo.env})
                                    }}><DeleteIcon/></IconButton>
                                </Stack>)}
                                {containerInfo.newEnv.map((env, index) => <Stack key={index} direction="row" spacing={2}
                                                                                 mt={2}>
                                    <TextField fullWidth label={'名称'} value={env.key} size={'small'} onChange={e => {
                                        containerInfo.newEnv[index].key = e.target.value
                                        setContainerInfo({
                                            ...containerInfo,
                                            newEnv: containerInfo.newEnv,
                                        })
                                    }}/>
                                    <TextField fullWidth label={'值'} value={env.value} size={'small'} onChange={e => {
                                        containerInfo.newEnv[index].value = e.target.value
                                        setContainerInfo({
                                            ...containerInfo,
                                            newEnv: containerInfo.newEnv,
                                        })
                                    }}/>
                                    <IconButton onClick={() => {
                                        containerInfo.newEnv.splice(index, 1)
                                        setContainerInfo({
                                            ...containerInfo,
                                            newEnv: containerInfo.newEnv
                                        })
                                    }}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </Stack>)}
                                <Button onClick={() => setContainerInfo({
                                    ...containerInfo,
                                    newEnv: [...containerInfo.newEnv, {key: '', value: ''}]
                                })}>添加</Button>
                            </AccordionDetails>
                        </Accordion>
                    </>}
                    {activeStep === 2 && <>
                        摘要 TODO
                    </>}
                    {activeStep === 3 && <Stack mt={5} spacing={2} alignItems={'center'} direction={'column'}>
                        {createError
                            ? <>
                                <ErrorIcon color={'error'} sx={{fontSize: 60}}/>
                                <Typography color={'error'} variant={'h6'}>创建失败</Typography>
                                <Typography>{createError}</Typography>
                                <Box>
                                    <Button sx={{m: 1}} variant={'outlined'}
                                            onClick={() => {setActiveStep(1)}}>返回</Button>
                                    <Button sx={{m: 1}} variant={'outlined'}
                                            onClick={() => {setDialogOpen(false)}}>关闭</Button>
                                </Box>
                            </>
                            : <>
                                <CircularProgress size={60}/>
                                <Typography>创建中...</Typography>
                            </>}
                    </Stack>}
                </DialogContent>
                <DialogActions>
                    {activeStep > 0 && activeStep !== 3 && !inspecting &&
                        <Button onClick={() => {setActiveStep(activeStep - 1)}}>上一步</Button>}
                    {!inspecting && activeStep !== 3 && <Button autoFocus onClick={() => handleCreateStepNextClick()}>
                        下一步
                    </Button>}
                </DialogActions>
            </Dialog>
            <Fab color={'primary'} sx={fabStyle} onClick={() => {
                setActiveStep(0)
                setDialogOpen(true)
            }}>
                <AddIcon/>
            </Fab>
        </TabPanel>
    )
}

function ImagesTabPanel (props) {
    const {engineEnabled} = props

    const {enqueueSnackbar} = useSnackbar()

    const [images, setImages] = useState([])

    const [removing, setRemoving] = useState(false)

    useEffect(() => {
        if (engineEnabled) {
            getImageList()
        }
    }, [engineEnabled])

    const getImageList = () => {
        ContainifyApi.getImageList()
            .then(data => setImages(data))
            .catch(err => enqueueSnackbar('获取镜像失败，错误:' + err, {variant: 'error'}))
    }

    const UNIT = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']

    const formatSizeString = (size, unit = 0) => {
        if (size < 1024) {
            return `${size.toFixed(2)} ${UNIT[unit]}`
        } else {
            return formatSizeString(size / 1024, unit + 1)
        }
    }

    const removeImage = id => {
        setRemoving(true)

        ContainifyApi.removeImage(id)
            .then(() => getImageList())
            .catch(err => enqueueSnackbar('删除镜像失败，错误:' + err, {variant: 'error'}))
            .finally(() => setRemoving(false))
    }

    return (
        <TabPanel value={'2'} sx={{p: 0}}>
            <TableContainer component={LabelPaper}>
                <Table stickyHeader sx={{minWidth: 650}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>状态</TableCell>
                            <TableCell>名称</TableCell>
                            <TableCell>版本</TableCell>
                            <TableCell>大小</TableCell>
                            <TableCell>创建时间</TableCell>
                            <TableCell align={'center'}>操作</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {images.map((image) => {
                            let name
                            let version = ''
                            if (image.repoTags.length >= 0) {
                                let strs = image.repoTags[0].split(':')
                                name = strs[0]
                                if (strs.length >= 2) {
                                    version = strs[1]
                                }
                            }

                            return (<TableRow
                                key={image.id}
                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                            >
                                <TableCell component="th" scope="row">
                                    {image.id.slice(0, 12)}
                                </TableCell>
                                <TableCell>{image.containers === 0 ? '未使用' : `${image.containers} 个容器在使用`}</TableCell>
                                <TableCell>{name}</TableCell>
                                <TableCell>{version}</TableCell>
                                <TableCell>{formatSizeString(image.size)}</TableCell>
                                <TableCell>{new Date(image.created * 1000).toLocaleString('af')}</TableCell>
                                <TableCell align={'center'}>
                                    {removing ? <CircularProgress size={31}/>
                                        : <Button size={'small'} onClick={() => removeImage(image.id)}>删除</Button>}
                                </TableCell>
                            </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </TabPanel>
    )
}