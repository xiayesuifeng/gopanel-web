import React, { useState } from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import cookies from 'js-cookie'

import { AuthApi } from '../api/auth'
import { useNavigate } from 'react-router-dom'

const image = {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme => theme.palette.mode === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
}
const paper = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
}
const avatar = {
    margin: 1,
    backgroundColor: theme => theme.palette.secondary.main,
}

export default function Login (props) {
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')

    const navigate = useNavigate()

    const handleLogin = () => {
        if (password === '') {
            setMessage('密码不能为空')
            return
        }

        AuthApi.login(password)
            .then(data => {
                let time = new Date()
                time.setDate(time.getDate() + 3)

                cookies.set('token', data.token, {expires: 3})
                cookies.set('tokenExpireTime', time.valueOf(), {expires: 3})
                navigate('/')
            })
            .catch(err => {
                if (err.toString() === 'Error: password error') {
                    setMessage('密码错误')
                } else {
                    setMessage(err.toString())
                }
            })
    }

    const handlePasswordChange = e => {
        setPassword(e.target.value)

        if (message !== '')
            setMessage('')
    }

    return (
        <Grid container component="main" sx={{
            height: '100vh',
            textAlign: 'left'
        }}>
            <Grid item xs={false} sm={6} md={9} sx={image}/>
            <Grid item xs={12} sm={6} md={3} component={Paper} elevation={6} square>
                <Box sx={paper}>
                    <Box display="flex" alignItems="center" flexDirection="column">
                        <Avatar sx={avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            登录
                        </Typography>
                        <Box sx={{
                            width: '100%',
                            marginTop: 1
                        }}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                name="password"
                                label={message !== '' ? message : '密码'}
                                type="password"
                                autoComplete="current-password"
                                error={message !== ''}
                                value={password}
                                onChange={handlePasswordChange}
                            />
                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                sx={{margin: theme => theme.spacing(3, 0, 2)}}
                                onClick={handleLogin}
                            >
                                登录
                            </Button>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    )
}