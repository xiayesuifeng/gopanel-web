import React, { useContext, useEffect } from 'react'
import Paper from '@mui/material/Paper'
import AppOverview from '../component/AppOverview'
import { GlobalContext } from '../globalState/Context'

   const paper= {
        margin: 5,
        padding: 5,
    }

export default function Overview () {
    const {netdataEnable} = useContext(GlobalContext)

    useEffect(() => {
        if (netdataEnable) {
            let script = document.createElement('script')
            script.type = 'text/javascript'
            script.src = '/netdata/dashboard.js'
            document.head.appendChild(script)

            return () => {
                let elements = document.head.getElementsByTagName('link')
                for (let i = 0; i < elements.length; i++) {
                    if (elements[i].href.indexOf('/netdata') >= 0) {
                        document.head.removeChild(elements[i])
                    }
                }

                elements = document.head.getElementsByTagName('script')
                for (let i = 0; i < elements.length; i++) {
                    if (elements[i].src.indexOf('/netdata') >= 0) {
                        document.head.removeChild(elements[i])
                    }
                }
            }
        }
    },[netdataEnable])

    return (
        <div>
            <Paper id='app' sx={paper}>
                <AppOverview sx={paper} />
            </Paper>
            {netdataEnable && <div>
                <Paper sx={paper}>
                    <div data-netdata="system.swap" data-dimensions="used" data-append-options="percentage"
                         data-chart-library="easypiechart" data-title="Used Swap" data-units="%"
                         data-easypiechart-max-value="100" data-width="9%" data-before="0" data-after="-780"
                         data-points="780" data-colors="#DD4400" role="application">
                    </div>

                    <div data-netdata="system.io" data-dimensions="in" data-chart-library="easypiechart"
                         data-title="Disk Read" data-width="11%" data-before="0" data-after="-780" data-points="780"
                         data-common-units="system.io.mainhead" role="application">
                    </div>

                    <div data-netdata="system.io" data-dimensions="out" data-chart-library="easypiechart"
                         data-title="Disk Write" data-width="11%" data-before="0" data-after="-780" data-points="780"
                         data-common-units="system.io.mainhead" role="application">
                    </div>

                    <div data-netdata="system.cpu"
                         data-chart-library="gauge"
                         data-title="CPU"
                         data-units="%"
                         data-gauge-max-value="100"
                         data-width="20%"
                         data-after="-780"
                         data-points="780"
                         data-colors="#AA5500" />
                    <div data-netdata="system.net" data-dimensions="received" data-chart-library="easypiechart"
                         data-title="Net Inbound" data-width="11%" data-before="0" data-after="-780" data-points="780"
                         data-common-units="system.net.mainhead" role="application">
                    </div>
                    <div data-netdata="system.net" data-dimensions="sent" data-chart-library="easypiechart"
                         data-title="Net Outbound" data-width="11%" data-before="0" data-after="-780" data-points="780"
                         data-common-units="system.net.mainhead" role="application">
                    </div>
                    <div data-netdata="system.ram" data-dimensions="used|buffers|active|wired"
                         data-append-options="percentage" data-chart-library="easypiechart" data-title="Used RAM"
                         data-units="%" data-easypiechart-max-value="100" data-width="9%" data-after="-780"
                         data-points="780" data-colors="#EE9911" role="application">
                    </div>
                </Paper>
                <Paper id='cpu' sx={paper}>
                    <div data-netdata="system.cpu"
                         data-title="CPU"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                         data-dygraph-valuerange="[0, 100]"
                    />
                </Paper>
                <Paper id='io' sx={paper}>
                    <div data-netdata="system.io"
                         data-title="I/O"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
                <Paper id='ram' sx={paper}>
                    <div data-netdata="system.ram"
                         data-title="物理内存"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
                <Paper id='swap' sx={paper}>
                    <div data-netdata="system.swap"
                         data-title="虚拟内存"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
                <Paper id='net' sx={paper}>
                    <div data-netdata="system.net"
                         data-title="网络流量"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
                <Paper id='processes' sx={paper}>
                    <div data-netdata="system.processes"
                         data-title="Processes"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
                <Paper id='uptime' sx={paper}>
                    <div data-netdata="system.uptime"
                         data-title="开机时间"
                         data-chart-library="dygraph"
                         data-height="200px"
                         data-after="-300"
                    />
                </Paper>
            </div>}
        </div>
    )
}