import React, { useContext, useState } from 'react'
import { GlobalContext } from '../globalState/Context'
import AppOverview from '../component/AppOverview'
import Paper from '@mui/material/Paper'
import Box from '@mui/material/Box'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import Typography from '@mui/material/Typography'
import AccordionDetails from '@mui/material/AccordionDetails'
import IconButton from '@mui/material/IconButton'
import CodeIcon from '@mui/icons-material/Code'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import Fab from '@mui/material/Fab'
import AddIcon from '@mui/icons-material/Add'
import AppEditorDialog from '../component/AppEditorDialog/AppEditorDialog'
import { AppApi } from '../api/app'
import { useSnackbar } from 'notistack'
import AppLogDialog from '../component/AppLogDialog'
import Grid from '@mui/material/Unstable_Grid2'
import Chip from '@mui/material/Chip'

const defaultAppConfig = {
    editName: '',
    name: '',
    caddyConfig: {},
    type: 1,
    backendType: 'none',
    backendConfig: {}
}

export default function Application () {
    const {apps,configuration, globalDispatch} = useContext(GlobalContext)

    const {enqueueSnackbar} = useSnackbar()

    const [expanded, setExpanded] = useState(false)
    const [editorDialogOpen, setEditorDialogOpen] = useState(false)
    const [appConfig, setAppConfig] = useState(defaultAppConfig)
    const [dialogLoading, setDialogLoading] = useState(false)
    const [appLogDialog, setAppLogDialog] = useState({name: undefined, open: false})

    const handlePanelChange = panel => (event, isExpanded) => setExpanded(isExpanded ? panel : false)

    const type = ['Go', 'Java', 'PHP', '其他', '反向代理', '静态网页']

    const handleAdd = () => {
        if (appConfig.editName !== '')
            setAppConfig(defaultAppConfig)

        setEditorDialogOpen(true)
    }

    const handleDelete = name => () => {
        console.log(name)
        AppApi.deleteApp(name)
            .then(() => {
                AppApi.getApp().then(data => globalDispatch({type: 'SET_APPS', data: data.apps}))
                enqueueSnackbar('删除成功', {variant: 'success'})
            })
            .catch(err => enqueueSnackbar(err.toString(), {variant: 'error'}))
    }

    const handleEditorDone = () => {
        setDialogLoading(true)
        if (appConfig.editName) {
            AppApi.editApp(appConfig.editName, appConfig)
                .then(() => {
                    AppApi.getApp().then(data => {
                        globalDispatch({type: 'SET_APPS', data: data.apps})
                        setEditorDialogOpen(false)
                        enqueueSnackbar('编辑成功', {variant: 'success'})
                    })
                })
                .catch(err => enqueueSnackbar(err.toString(), {variant: 'error'}))
                .finally(() => setDialogLoading(false))
        } else {
            AppApi.addApp({...appConfig, caddyConfig: appConfig.caddyConfig})
                .then(() => {
                    AppApi.getApp().then(data => {
                        globalDispatch({type: 'SET_APPS', data: data.apps})
                        setEditorDialogOpen(false)
                        enqueueSnackbar('添加成功', {variant: 'success'})
                    })
                })
                .catch(err => enqueueSnackbar(err.toString(), {variant: 'error'}))
                .finally(() => setDialogLoading(false))
        }
    }

    return (
        <div>
            <Paper sx={{
                margin: 5,
                padding: 3
            }}>
                <AppOverview/>
            </Paper>

            <AppEditorDialog config={appConfig} onConfigChange={setAppConfig} open={editorDialogOpen}
                             loading={dialogLoading}
                             onClose={() => setEditorDialogOpen(false)} onDone={handleEditorDone}/>

            <AppLogDialog name={appLogDialog.name} open={appLogDialog.open}
                          onClose={() => setAppLogDialog({name: undefined, open: false})}/>

            <Box sx={{margin: 5}}>
                {apps.map(app => (
                    <Accordion expanded={expanded === app.name} onChange={handlePanelChange(app.name)}>
                        <AccordionSummary
                            sx={{p: 1}}
                            expandIcon={<ExpandMoreIcon/>}
                        >
                            <Grid container spacing={2} pl={3} width="100%">
                                <Grid container xs={4}>
                                    <Grid xs={12} display={'flex'}>
                                        <Typography>{app.name}</Typography>
                                    </Grid>
                                    <Grid xs={12} display={'flex'}>
                                        <div onClick={event => event.stopPropagation()}
                                             onFocus={event => event.stopPropagation()}>
                                            <Chip clickable
                                                  label={app.caddyConfig.domain}
                                                  size={'small'}
                                                  component="a"
                                                  target={'_blank'}
                                                  href={`${app.caddyConfig.disableSSL ? 'http' : 'https'}://${app.caddyConfig.domain}:${app.caddyConfig.listenPort
                                                      ? app.caddyConfig.listenPort 
                                                      : app.caddyConfig.disableSSL
                                                          ? configuration?.general?.HTTPPort
                                                          : configuration?.general?.HTTPSPort}`}/>
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid xs={3}
                                      display={'flex'}
                                      alignItems={'center'}>
                                    <Typography>类型： {type[app.type - 1]}</Typography>
                                </Grid>
                                <Grid xs={3}
                                      display={'flex'}
                                      alignItems={'center'}>
                                    <Typography>SSL: {app.caddyConfig.disableSSL ? 'HTTP Only' : 'HTTPS'}</Typography>
                                </Grid>
                                <Grid xs={2}
                                      display={'flex'}
                                      alignItems={'center'}
                                      justifyContent={'end'}
                                      onClick={event => event.stopPropagation()}
                                      onFocus={event => event.stopPropagation()}>
                                    <IconButton onClick={() => setAppLogDialog({name: app.name, open: true})}
                                                size="large">
                                        <CodeIcon/>
                                    </IconButton>
                                    <IconButton
                                        onClick={() => {
                                            setAppConfig({...app, editName: app.name})
                                            setEditorDialogOpen(true)
                                        }}
                                        size="large">
                                        <EditIcon/>
                                    </IconButton>
                                    <IconButton onClick={handleDelete(app.name)} size="large">
                                        <DeleteIcon/>
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Box display="flex" flexDirection="column" alignItems="flex-start" pl={10}>

                                <Typography>后端类型： {app.backendType}</Typography>
                                {app.backendType === 'exec' &&
                                    <>
                                        <Typography>工作目录： {app.backendConfig.workingDirectory}</Typography>
                                        <Typography>程序路径： {app.backendConfig.path}</Typography>
                                        <Typography>运行参数： {app.backendConfig.argument}</Typography>
                                        <Typography>自动重启： {app.backendConfig.autoReboot ? '是' : '否'}</Typography>
                                    </>}
                            </Box>
                        </AccordionDetails>
                    </Accordion>
                ))}
            </Box>

            <Fab color={'primary'} sx={{
                position: 'fixed',
                bottom: 24,
                right: 24,
                zIndex: (theme) => theme.zIndex.drawer
            }} onClick={handleAdd}>
                <AddIcon/>
            </Fab>
        </div>
    )
}