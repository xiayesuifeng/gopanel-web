import React, { useContext, useEffect, useState } from 'react'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import Typography from '@mui/material/Typography'
import Drawer from '@mui/material/Drawer'
import Divider from '@mui/material/Divider'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import { Link, Route, Routes, useRoutes, useLocation, useNavigate } from 'react-router-dom'
import Button from '@mui/material/Button'
import Cookies from 'js-cookie'
import { AppApi } from '../api/app'
import { GlobalContext } from '../globalState/Context'
import axios from 'axios'
import { Collapse, ListItemIcon } from '@mui/material'
import { ExpandLess, ExpandMore } from '@mui/icons-material'
import Service from './system/Service'
import Box from '@mui/material/Box'
import { CaddyApi } from '../api/caddy'
import { ApplicationIcon } from '../component/icons/application'
import {
    ViewDashboard as OverviewIcon,
    Server as ServerIcon,
    ServerNetwork as NetworkIcon,
    ServerSecurity as ServerSecurityIcon,
    Docker as ContainifyIcon
} from 'mdi-material-ui'

const Overview = React.lazy(() => import('./Overview'))
const Application = React.lazy(() => import('./Application'))
const Containify = React.lazy(() => import('./Containify'))
const Caddy = React.lazy(() => import('./system/Caddy'))
const Firewall = React.lazy(() => import('./network/firewall/Firewall'))

const drawerWidth = 240

function HomeRoutes (props) {
    const {routeConfig} = props

    return useRoutes(routeConfig)
}

export default function Home (props) {
    const [mobileOpen, setMobileOpen] = useState(false)
    const [drawerLastMenuClick, setDrawerLastMenuClick] = useState(0)

    const {netdataEnable, globalDispatch} = useContext(GlobalContext)

    const location = useLocation()
    const navigate = useNavigate()

    const routeConfig = [
        {
            path: '/', name: '总览', icon: <OverviewIcon/>, element: <Overview/>
        },
        {
            path: '/application', name: '应用', icon: <ApplicationIcon/>, element: <Application/>
        },
        {
            path: '/containify', name: '容器化', icon: <ContainifyIcon/>, element: <Containify/>
        },
        {
            path: '/system', name: '系统', icon: <ServerIcon/>, children: [
                {
                    path: 'caddy', name: 'Caddy', element: <Caddy/>
                },
                {
                    path: 'service', name: '服务', element: <Service/>
                }
            ]
        },
        {
            path: '/network', name: '网络', icon: <NetworkIcon/>, children: [
                {
                    path: 'firewall', name: '防火墙', icon: <ServerSecurityIcon/>, element: <Firewall/>
                }
            ]
        }
    ]

    const overviewList = [{name: '应用', id: 'app'}, {name: 'CPU', id: 'cpu'}, {
        name: 'I/O', id: 'io'
    }, {name: '物理内存', id: 'ram'}, {name: '虚拟内存', id: 'swap'}, {name: '网络流量', id: 'net'}, {
        name: 'Processes', id: 'processes'
    }, {name: '开机时间', id: 'uptime'}]

    useEffect(() => {
        let token = Cookies.get('token')
        if (token === undefined || token === '') {
            navigate('/login')
        } else {
            if (!Cookies.get('tokenExpireTime')) {
                navigate('/login')
            }
            AppApi.getApp()
                .then(data => globalDispatch({type: 'SET_APPS', data: data.apps}))
                .catch(() => navigate('/login'))

            CaddyApi.getConfiguration()
                .then(data => globalDispatch({type: 'SET_CONFIGURATION', data: data.configuration}))

            CaddyApi.getModules()
                .then(data => globalDispatch({type: 'SET_CADDY_MODULES', data: data.modules}))
        }

        axios.get('/netdata/dashboard.js')
            .then(() => globalDispatch({type: 'SET_NETDATA', enable: true}))
            .catch(() => globalDispatch({type: 'SET_NETDATA', enable: false}))

    }, [])

    const handleLogout = () => {
        Cookies.remove('token')
        Cookies.remove('tokenExpireTime')
        navigate('/login')
    }

    const scrollToAnchor = anchorName => () => {
        if (anchorName) {
            let anchorElement = document.getElementById(anchorName)
            if (anchorElement) {
                let bodyTop = document.body.getBoundingClientRect().top
                window.scrollTo({
                    top: anchorElement.getBoundingClientRect().top - bodyTop - 100, behavior: 'smooth'
                })
            }
        }
    }

    const drawer = (<div>
        <Toolbar>
            <Typography variant={'h4'} color={'primary'} align={'center'} width={'100%'}>Gopanel</Typography>
        </Toolbar>
        <Divider/>
        <List>
            {routeConfig.map((route, index) => (<div>
                {(route.children && route.children.length > 0)
                    ? <ListItemButton key={route.name} onClick={() => setDrawerLastMenuClick(index)}>
                        <ListItemIcon>{route.icon}</ListItemIcon>
                        <ListItemText primary={route.name}/>
                        {drawerLastMenuClick === index ? <ExpandLess/> : <ExpandMore/>}
                    </ListItemButton>
                    : <ListItemButton key={route.name} selected={route.path === location.pathname}
                                      component={Link} to={route.path}
                                      onClick={() => setDrawerLastMenuClick(index)}>
                        <ListItemIcon>{route.icon}</ListItemIcon>
                        <ListItemText primary={route.name}/>
                    </ListItemButton>
                }
                {(netdataEnable && route.path === '/' && location.pathname === '/') &&
                    <List component="div" disablePadding>
                        {overviewList.map(list => (
                            <ListItemButton sx={{paddingLeft: 6}} onClick={scrollToAnchor(list.id)}>
                                <ListItemText primary={list.name}/>
                            </ListItemButton>))}
                    </List>}
                {(route.children && route.children.length > 0) &&
                    <Collapse in={drawerLastMenuClick === index} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            {route.children.map(subRoute => (
                                <ListItemButton sx={{pl: 4}}
                                                selected={`${route.path}/${subRoute.path}` === location.pathname}
                                                component={Link}
                                                to={`${route.path}/${subRoute.path}`}>
                                    <ListItemIcon>{subRoute.icon}</ListItemIcon>
                                    <ListItemText primary={subRoute.name}/>
                                </ListItemButton>
                            ))}
                        </List>
                    </Collapse>
                }
            </div>))}
        </List>
    </div>)

    return (<Box display={'flex'}>
        <AppBar position="fixed" sx={{
            width: {sm: `calc(100% - ${drawerWidth}px)`}, ml: {sm: `${drawerWidth}px`},
        }}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={() => setMobileOpen(!mobileOpen)}
                    sx={{mr: 2, display: {sm: 'none'}}}
                    size="large">
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" noWrap sx={{
                    textAlign: 'left', flexGrow: 1
                }}>
                    {routeConfig[drawerLastMenuClick].name}
                </Typography>
                {netdataEnable && location.pathname === '/' && <Button color="inherit" variant="outlined"
                                                                       component="a" href={'/netdata/'}
                                                                       target="view_window">NETDATA
                    DASHBOARD</Button>}
                <Button color="inherit" onClick={handleLogout}>退出</Button>
            </Toolbar>
        </AppBar>
        <Box component="nav" sx={{width: {sm: drawerWidth}, flexShrink: {sm: 0}}} aria-label="mailbox folders">
            <Drawer
                variant="temporary"
                open={mobileOpen}
                onClose={() => setMobileOpen(!mobileOpen)}
                sx={{
                    display: {xs: 'block', sm: 'none'},
                    '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                }}
                ModalProps={{
                    keepMounted: true,
                }}
            >
                {drawer}
            </Drawer>
            <Drawer
                sx={{
                    display: {xs: 'none', sm: 'block'},
                    '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                }}
                variant="permanent"
                open
            >
                {drawer}
            </Drawer>
        </Box>
        <Box component="main" sx={{
            flexGrow: 1,
            p: 3,
            width: {sm: `calc(100% - ${drawerWidth}px)`},
            backgroundColor: 'rgb(249, 250, 251)',
            minHeight: '100vh'
        }}>
            <Toolbar/>
            <HomeRoutes routeConfig={routeConfig}/>
        </Box>
    </Box>)
}
