export const protocols = [
    'hopopt', 'icmp', 'igmp', 'ggp', 'ipv4', 'st', 'tcp', 'cbt', 'egp',
    'igp', 'bbn-rcc-mon', 'nvp-ii', 'pup', 'emcon', 'xnet', 'chaos',
    'udp', 'mux', 'dcn-meas', 'hmp', 'prm', 'xns-idp', 'trunk-1', 'trunk-2',
    'leaf-1', 'leaf-2', 'rdp', 'irtp', 'iso-tp4', 'netblt', 'mfe-nsp',
    'merit-inp', 'dccp', '3pc', 'idpr', 'xtp', 'ddp', 'idpr-cmtp', 'tp++',
    'il', 'ipv6', 'sdrp', 'ipv6-route', 'ipv6-frag', 'idrp', 'rsvp', 'gre',
    'dsr', 'bna', 'esp', 'ah', 'i-nlsp', 'narp', 'min-ipv4', 'tlsp', 'skip',
    'ipv6-icmp', 'ipv6-nonxt', 'ipv6-opts', 'cftp', 'sat-expak', 'kryptolan',
    'rvd', 'ippc', 'sat-mon', 'visa', 'ipcv', 'cpnx', 'cphb', 'wsn', 'pvp',
    'br-sat-mon', 'sun-nd', 'wb-mon', 'wb-expak', 'iso-ip', 'vmtp', 'secure-vmtp',
    'vines', 'iptm', 'nsfnet-igp', 'dgp', 'tcf', 'eigrp', 'ospfigp', 'sprite-rpc',
    'larp', 'mtp', 'ax.25', 'ipip', 'scc-sp', 'etherip', 'encap', 'gmtp', 'ifmp',
    'pnni', 'pim', 'aris', 'scps', 'qnx', 'a/n', 'ipcomp', 'snp', 'compaq-peer',
    'ipx-in-ip', 'vrrp', 'pgm', 'l2tp', 'ddx', 'iatp', 'stp', 'srp', 'uti', 'smp',
    'ptp', 'fire', 'crtp', 'crudp', 'sscopmce', 'iplt', 'sps', 'pipe', 'sctp',
    'fc', 'rsvp-e2e-ignore', 'udplite', 'mpls-in-ip', 'manet', 'hip', 'shim6',
    'wesp', 'rohc', 'ethernet', 'aggfrag', 'nsh', 'reserved'
]