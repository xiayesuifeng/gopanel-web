import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react'
import { useSnackbar } from 'notistack'
import { FirewallApi } from '../../../api/firewall'
import TabPanel from '@mui/lab/TabPanel'
import LabelPaper from '../../../component/LabelPaper'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Button from '@mui/material/Button'
import TableFooter from '@mui/material/TableFooter'
import ZoneBar from './component/ZoneBar'
import DialogTitle from '@mui/material/DialogTitle'
import Dialog from '@mui/material/Dialog'
import { useTheme } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogContent from '@mui/material/DialogContent'
import Box from '@mui/material/Box'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import TextField from '@mui/material/TextField'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import DialogActions from '@mui/material/DialogActions'

const defaultPortForward = {
    port: '',
    protocol: 'tcp',
    toPort: '',
    toAddress: ''
}

const PortForwardPanel = forwardRef((props, ref) => {
    const {defaultZone, zoneNames} = props

    const [currentZone, setCurrentZone] = useState(null)
    const [permanent, setPermanent] = useState(false)

    const [portForwards, setPortForwards] = useState([])

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'))

    const [dialogOpen, setDialogOpen] = useState(false)
    const [newPortForward, setNewPortForward] = useState(defaultPortForward)

    const {enqueueSnackbar} = useSnackbar()

    useImperativeHandle(ref, () => {
        return {
            reloadTrigger () {
                if (!permanent && currentZone)
                    getPortForwards()
            },
            resetTrigger () {
                if (currentZone) {
                    getPortForwards()
                }
            }
        }
    }, [])

    useEffect(() => {
        setCurrentZone(defaultZone)
    }, [defaultZone])

    useEffect(() => {
        if (currentZone) {
            getPortForwards()
        }
    }, [currentZone, permanent])

    const getPortForwards = () => {
        FirewallApi.getPortForwards(currentZone, permanent)
            .then((data) => setPortForwards(data))
            .catch((msg) =>
                enqueueSnackbar('获取端口转发规则失败，错误:' + msg, {variant: 'error'}),
            )
    }


    const handleDelete = portForward => {
        FirewallApi.removePortForward(currentZone, portForward, permanent)
            .then(data => getPortForwards())
            .catch(err => enqueueSnackbar('删除失败, 错误: ' + err.toString(), {variant: 'error'}))
    }

    const handleAdd = () => {
        if (newPortForward.port === '') {
            enqueueSnackbar('源端口不能为空', {variant: 'warning'})
            return
        } else if (newPortForward.toPort === '') {
            enqueueSnackbar('目标端口不能为空', {variant: 'warning'})
            return
        }

        FirewallApi.addPortForward(currentZone, newPortForward, permanent)
            .then(data =>{
                getPortForwards()
                setDialogOpen(false)
                setNewPortForward(defaultPortForward)
            })
            .catch(err => enqueueSnackbar('添加失败, 错误: ' + err.toString(), {variant: 'error'}))
    }

    return (
        <TabPanel value={'3'} sx={{p: 0}}>
            <ZoneBar zones={zoneNames}
                     currentZone={currentZone}
                     onZoneChange={setCurrentZone}
                     permanent={permanent}
                     onPermanentChange={setPermanent}
            />
            <LabelPaper label={'端口转发'}>
                <TableContainer>
                    <Table sx={{minWidth: 650}} size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell>协议</TableCell>
                                <TableCell align={'center'}>端口</TableCell>
                                <TableCell align={'center'}>目标端口</TableCell>
                                <TableCell align={'center'}>目标 IP</TableCell>
                                <TableCell align={'right'}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {portForwards.map((portForward) => (
                                <TableRow
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {portForward.protocol}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {portForward.port}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {portForward.toPort}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {portForward.toAddress === '' ? '本机' : portForward.toAddress}
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <Button variant="contained" size="small" sx={{m: 1}} onClick={() => handleDelete(portForward)}>
                                            删除
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <Button variant="contained"
                                        size="small"
                                        sx={{m: 1}}
                                        onClick={() => setDialogOpen(true)}>
                                    添加
                                </Button>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </LabelPaper>
            <Dialog
                maxWidth={'xs'}
                fullWidth
                fullScreen={fullScreen}
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                aria-labelledby="dialog-title"
            >
                <DialogTitle id="dialog-title">
                    添加流量规则
                </DialogTitle>
                <Box display="flex" flexDirection="column" component={DialogContent}>
                    <FormControl sx={{minWidth: 100, mt: 2}}>
                        <InputLabel>协议</InputLabel>
                        <Select
                            value={newPortForward.protocol}
                            label="协议"
                            required
                            onChange={(event) => setNewPortForward({...newPortForward, protocol: event.target.value})}
                            variant={'outlined'}
                        >
                            <MenuItem value={'tcp'}>TCP</MenuItem>
                            <MenuItem value={'udp'}>UDP</MenuItem>
                            <MenuItem value={'sctp'}>SCTP</MenuItem>
                            <MenuItem value={'dccp'}>DCCP</MenuItem>
                        </Select>
                    </FormControl>
                    <TextField
                        fullWidth
                        required
                        sx={{mt: 2}}
                        label={'源端口'}
                        helperText="端口号或范围，如：8080 或 8080-8090"
                        placeholder="number[-number]"
                        value={newPortForward.port}
                        onChange={event => setNewPortForward({
                            ...newPortForward,
                            port: event.target.value
                        })}
                    />
                    <TextField
                        fullWidth
                        required
                        sx={{mt: 2}}
                        label={'目标端口'}
                        helperText="端口号或范围，如：8080 或 8080-8090"
                        placeholder="number[-number]"
                        value={newPortForward.toPort}
                        onChange={event => setNewPortForward({
                            ...newPortForward,
                            toPort: event.target.value
                        })}
                    />
                    <TextField
                        fullWidth
                        sx={{mt: 2}}
                        label={'目标 IP'}
                        helperText="目标 IP，留空则表示本机"
                        placeholder="127.0.0.1"
                        value={newPortForward.toAddress}
                        onChange={event => setNewPortForward({
                            ...newPortForward,
                            toAddress: event.target.value
                        })}
                    />
                </Box>
                <DialogActions>
                    <Button autoFocus onClick={() => setDialogOpen(false)}>
                        取消
                    </Button>
                    <Button onClick={() => handleAdd()} autoFocus>
                        添加
                    </Button>
                </DialogActions>
            </Dialog>
        </TabPanel>
    )
})

export default PortForwardPanel
