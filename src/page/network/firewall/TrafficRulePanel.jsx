import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react'
import { useSnackbar } from 'notistack'
import { FirewallApi } from '../../../api/firewall'
import TabPanel from '@mui/lab/TabPanel'
import LabelPaper from '../../../component/LabelPaper'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Button from '@mui/material/Button'
import TableFooter from '@mui/material/TableFooter'
import ZoneBar from './component/ZoneBar'
import useMediaQuery from '@mui/material/useMediaQuery'
import { useTheme } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import { FormLabel, Radio, RadioGroup, ToggleButton, ToggleButtonGroup } from '@mui/material'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import Checkbox from '@mui/material/Checkbox'
import Divider from '@mui/material/Divider'
import Chip from '@mui/material/Chip'
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined'
import IconButton from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import { protocols } from './const'

const defaultTrafficRule = {
    family: '',
    srcEnabled: false,
    srcAddr: '',
    srcAddrInvert: false,
    destEnabled: false,
    destAddr: '',
    destAddrInvert: false,
    strategy: 0,
    type: 'service',
    log: {
        enabled: false,
        prefix: '',
        level: '',
        limit: ''
    },
    audit: false,
    strValue: '',
    portValue: {
        port: '',
        protocol: 'tcp'
    }
}
const TrafficRulePanel = forwardRef((props, ref) => {
    const {defaultZone, zoneNames} = props

    const [currentZone, setCurrentZone] = useState(null)
    const [permanent, setPermanent] = useState(false)

    const [rules, setRules] = useState([])
    const [services, setServices] = useState([])

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'))

    const [dialogOpen, setDialogOpen] = useState(false)
    const [newRule, setNewRule] = useState(defaultTrafficRule)

    const {enqueueSnackbar} = useSnackbar()

    useImperativeHandle(ref, () => {
        return {
            reloadTrigger () {
                if (!permanent)
                    getAll()
            },
            resetTrigger () {
                getAll()
            }
        }
    }, [])

    useEffect(() => {
        setCurrentZone(defaultZone)
    }, [defaultZone])

    useEffect(() => getAll(), [currentZone, permanent])

    const getAll = () => {
        if (currentZone) {
            getTrafficRules()
        }

        FirewallApi.getServiceNames(permanent)
            .then(data => setServices(data))
            .catch((msg) => enqueueSnackbar('获取服务列表失败，错误:' + msg, {variant: 'error'}))
    }

    const getTrafficRules = () => {
        FirewallApi.getTrafficRules(currentZone, permanent)
            .then((data) => setRules(data))
            .catch((msg) =>
                enqueueSnackbar('获取流量规则失败，错误:' + msg, {variant: 'error'}),
            )
    }

    const getStrategyText = strategy => {
        switch (strategy) {
            case 0:
                return '允许'
            case 1:
                return '拒绝'
            case 2:
                return '丢弃'
            default:
                return ''
        }
    }

    const getTypeText = type => {
        switch (type) {
            case 'service':
                return '服务'
            case  'port':
                return '端口'
            case   'protocol':
                return '协议'
            case   'forward-port':
                return '转发端口'
            case   'source-port':
                return '源端口'
            case   'masquerade':
            case   'icmp-block':
            default:
                return type
        }
    }

    const handleDelete = rule => {
        FirewallApi.removeTrafficRule(currentZone, rule, permanent)
            .then(data => getTrafficRules())
            .catch(err => enqueueSnackbar('删除失败, 错误: ' + err.toString(), {variant: 'error'}))
    }

    const handleAdd = () => {
        if (newRule.type === 'service' && newRule.strValue === '') {
            enqueueSnackbar('服务不能为空', {variant: 'warning'})
            return
        } else if (newRule.type === 'protocol' && newRule.strValue === '') {
            enqueueSnackbar('协议不能为空', {variant: 'warning'})
            return
        } else if ((newRule.type === 'port' || newRule.type === 'source-port') && newRule.portValue.port === '') {
            enqueueSnackbar('端口不能为空', {variant: 'warning'})
            return
        } else if (newRule.log.enabled && newRule.log.limit === '') {
            enqueueSnackbar('日志记录启用时速率不能为空', {variant: 'warning'})
            return
        }

        let data = {
            family: (newRule.srcEnabled || newRule.destEnabled) ? newRule.family : '',
            srcAddr: newRule.srcEnabled ? newRule.srcAddr : '',
            srcAddrInvert: newRule.srcEnabled ? newRule.srcAddrInvert : false,
            destAddr: newRule.destEnabled ? newRule.destAddr : '',
            destAddrInvert: newRule.destEnabled ? newRule.destAddrInvert : false,
            strategy: newRule.strategy,
            type: newRule.type,
            value: (newRule.type === 'port' || newRule.type === 'source-port') ? newRule.portValue : newRule.strValue,
            log: newRule.log.enabled ? newRule.log : {},
            audit: newRule.audit
        }

        FirewallApi.addTrafficRule(currentZone, data, permanent)
            .then(data => {
                getTrafficRules()
                setDialogOpen(false)
                setNewRule(defaultTrafficRule)
            })
            .catch(err => enqueueSnackbar('添加失败, 错误: ' + err.toString(), {variant: 'error'}))
    }

    return (
        <TabPanel value={'2'} sx={{p: 0}}>
            <ZoneBar zones={zoneNames}
                     currentZone={currentZone}
                     onZoneChange={setCurrentZone}
                     permanent={permanent}
                     onPermanentChange={setPermanent}
            />
            <LabelPaper label={'规则'}>
                <TableContainer>
                    <Table sx={{minWidth: 650}} size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell>类型</TableCell>
                                <TableCell align={'center'}>策略</TableCell>
                                <TableCell align={'center'}>协议</TableCell>
                                <TableCell align={'center'}>值</TableCell>
                                <TableCell align={'center'}>来源 IP</TableCell>
                                <TableCell align={'center'}>目标 IP</TableCell>
                                <TableCell align={'center'}>日志记录</TableCell>
                                <TableCell align={'center'}>审计日志</TableCell>
                                <TableCell align={'right'}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rules.map((rule) => (
                                <TableRow
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {getTypeText(rule.type)}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {getStrategyText(rule.strategy)}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {(rule.type === 'port' || rule.type === 'source-port') ? rule.value.protocol : ''}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {(rule.type === 'port' || rule.type === 'source-port')
                                            ? rule.value.port
                                            : rule.value}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {rule.srcAddr ?? '所有 IP'}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {rule.destAddr ?? '所有 IP'}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {rule.log.enabled
                                            ? <>
                                                {rule.log.prefix &&
                                                    <Chip sx={{mr: 0.5}} label={`前缀: ${rule.log.prefix}`}/>}
                                                {rule.log.level &&
                                                    <Chip sx={{mr: 0.5}} label={`等级: ${rule.log.level}`}/>}
                                                {rule.log.limit &&
                                                    <Chip sx={{mr: 0.5}} label={`速率: ${rule.log.limit}`}/>}
                                            </>
                                            : '未启用'
                                        }
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {rule.audit ? '已启用' : '未启用'}
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <Button
                                            variant="contained"
                                            size={'small'}
                                            sx={{m: 1}}
                                            onClick={() => handleDelete(rule)}
                                        >
                                            删除
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <Button variant="contained" size={'small'} sx={{m: 1}}
                                        onClick={() => setDialogOpen(true)}>
                                    添加
                                </Button>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </LabelPaper>
            <Dialog
                maxWidth={'xs'}
                fullWidth
                fullScreen={fullScreen}
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                aria-labelledby="dialog-title"
            >
                <DialogTitle id="dialog-title">
                    添加流量规则
                </DialogTitle>
                <DialogContent>
                    <Box display="flex" flexDirection="column">
                        <FormControl sx={{mt: 1}}>
                            <FormLabel>来源</FormLabel>
                            <RadioGroup
                                row
                                value={newRule.srcEnabled ? newRule.family : 'all'}
                                onChange={(event, value) => {
                                    if (value === 'all') {
                                        setNewRule({...newRule, srcEnabled: false})
                                    } else {
                                        setNewRule({...newRule, srcEnabled: true, family: value})
                                    }
                                }}
                            >
                                <FormControlLabel value="all" control={<Radio/>} label="所有 IP"/>
                                <FormControlLabel value="ipv4" control={<Radio/>} label="IPv4"/>
                                <FormControlLabel value="ipv6" control={<Radio/>} label="IPv6"/>
                            </RadioGroup>
                        </FormControl>
                        {newRule.srcEnabled &&
                            <TextField
                                sx={{mt: 1}}
                                label="地址"
                                helperText="IP 或 IP/Mask，如: 192.168.1.1 或 192.168.1.0/24"
                                value={newRule.srcAddr}
                                onChange={event => setNewRule({...newRule, srcAddr: event.target.value})}
                            />
                        }
                        {(newRule.srcEnabled && newRule.srcAddr) && <FormControlLabel
                            label="匹配相反项"
                            control={
                                <Checkbox
                                    checked={newRule.srcAddrInvert}
                                    onChange={(event, checked) => setNewRule({
                                        ...newRule,
                                        srcAddrInvert: checked
                                    })}
                                />}
                        />}
                        <FormControl sx={{mt: 1}}>
                            <FormLabel>目的地</FormLabel>
                            <RadioGroup
                                row
                                value={newRule.destEnabled ? newRule.family : 'all'}
                                onChange={(event, value) => {
                                    if (value === 'all') {
                                        setNewRule({...newRule, destEnabled: false})
                                    } else {
                                        setNewRule({...newRule, destEnabled: true, family: value})
                                    }
                                }}
                            >
                                <FormControlLabel value="all" control={<Radio/>} label="所有 IP"/>
                                <FormControlLabel value="ipv4" control={<Radio/>} label="IPv4"/>
                                <FormControlLabel value="ipv6" control={<Radio/>} label="IPv6"/>
                            </RadioGroup>
                        </FormControl>
                        {newRule.destEnabled &&
                            <TextField
                                sx={{mt: 1}}
                                label="地址"
                                helperText="IP 或 IP/Mask，如: 192.168.1.1 或 192.168.1.0/24"
                                value={newRule.destAddr}
                                onChange={event => setNewRule({...newRule, destAddr: event.target.value})}
                            />}
                        {(newRule.destEnabled && newRule.destAddr) && <FormControlLabel
                            label="匹配相反项"
                            control={
                                <Checkbox
                                    checked={newRule.destAddrInvert}
                                    onChange={(event, checked) => setNewRule({
                                        ...newRule,
                                        destAddrInvert: checked
                                    })}
                                />}
                        />}
                        <FormControl sx={{mt: 1}}>
                            <FormLabel>策略</FormLabel>
                            <ToggleButtonGroup
                                fullWidth
                                color="primary"
                                value={newRule.strategy}
                                exclusive
                                onChange={(event, value) => {
                                    if (value)
                                        setNewRule({...newRule, strategy: value})
                                }}
                            >
                                <ToggleButton value={0}>接受</ToggleButton>
                                <ToggleButton value={1}>拒绝</ToggleButton>
                                <ToggleButton value={2}>丢弃</ToggleButton>
                            </ToggleButtonGroup>
                        </FormControl>
                        <FormControl sx={{minWidth: 100, mt: 2}}>
                            <InputLabel>类型</InputLabel>
                            <Select
                                value={newRule.type}
                                label="类型"
                                onChange={(event) => setNewRule({...newRule, type: event.target.value})}
                            >
                                <MenuItem value={'service'}>服务</MenuItem>
                                <MenuItem value={'port'}>端口</MenuItem>
                                <MenuItem value={'protocol'}>协议</MenuItem>
                                <MenuItem value={'source-port'}>源端口</MenuItem>
                            </Select>
                        </FormControl>
                        {newRule.type === 'service' && <FormControl sx={{minWidth: 100, mt: 1}}>
                            <Select
                                value={newRule.strValue}
                                onChange={(event) => setNewRule({...newRule, strValue: event.target.value})}
                            >
                                {services.map(service => (<MenuItem value={service}>{service}</MenuItem>))}
                            </Select>
                        </FormControl>}
                        {newRule.type === 'protocol' && <FormControl sx={{minWidth: 100, mt: 1}}>
                            <Select
                                value={newRule.strValue}
                                onChange={(event) => setNewRule({...newRule, strValue: event.target.value})}
                            >
                                {protocols.map(protocol => (<MenuItem value={protocol}>{protocol}</MenuItem>))}
                            </Select>
                        </FormControl>}
                        {(newRule.type === 'port' || newRule.type === 'source-port') &&
                            <Box sx={{mt: 1, display: 'flex', alignItems: 'center'}}>
                                <TextField sx={{flexGrow: 1}}
                                           helperText="端口号或范围，如：8080 或 8080-8090"
                                           placeholder="number[-number]"
                                           value={newRule.portValue.port}
                                           onChange={event => setNewRule({
                                               ...newRule,
                                               portValue: {...newRule.portValue, port: event.target.value}
                                           })}
                                />
                                <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                                <TextField
                                    select
                                    sx={{minWidth: 80}}
                                    helperText="协议"
                                    value={newRule.portValue.protocol}
                                    onChange={(event) => setNewRule({
                                        ...newRule,
                                        portValue: {...newRule.portValue, protocol: event.target.value}
                                    })}
                                >
                                    <MenuItem value={'tcp'}>TCP</MenuItem>
                                    <MenuItem value={'udp'}>UDP</MenuItem>
                                </TextField>
                            </Box>
                        }
                        <FormControlLabel
                            label="日志记录"
                            control={
                                <Checkbox
                                    checked={newRule.log.enabled}
                                    onChange={(event, checked) => setNewRule({
                                        ...newRule,
                                        log: {
                                            ...newRule.log,
                                            enabled: checked
                                        }
                                    })}
                                />}
                        />
                        {newRule.log.enabled && <Box sx={{display: 'flex', mt: 2}}>
                            <TextField
                                label={'前缀'}
                                value={newRule.log.prefix}
                                helperText={'可选'}
                                onChange={event => setNewRule({
                                    ...newRule,
                                    log: {...newRule.log, prefix: event.target.value}
                                })}
                            />
                            <TextField
                                select
                                sx={{minWidth: 100}}
                                label="级别"
                                helperText={'可选'}
                                value={newRule.log.level}
                                onChange={(event) => setNewRule({
                                    ...newRule,
                                    log: {...newRule.log, level: event.target.value}
                                })}
                            >
                                <MenuItem value={''}>无</MenuItem>
                                <MenuItem value={'emerg'}>emerg</MenuItem>
                                <MenuItem value={'alert'}>alert</MenuItem>
                                <MenuItem value={'crit'}>crit</MenuItem>
                                <MenuItem value={'error'}>error</MenuItem>
                                <MenuItem value={'warning'}>warning</MenuItem>
                                <MenuItem value={'notice'}>notice</MenuItem>
                                <MenuItem value={'info'}>info</MenuItem>
                                <MenuItem value={'debug'}>debug</MenuItem>
                            </TextField>
                            <TextField label={'速率'}
                                       value={newRule.log.limit}
                                       required
                                       helperText={
                                           <Tooltip
                                               title="速率是一个自然正数 [1, ..]，持续时间为 s,m,h,d。s 表示秒，m 表示分钟，h 表示小时和 d 天。最大限制值为 1/d，这表示每天最多一个日志条目">
                                               必填 <IconButton>
                                               <InfoOutlinedIcon sx={{fontSize: 18}} color="primary"/>
                                           </IconButton>
                                           </Tooltip>
                                       }
                                       onChange={event => setNewRule({
                                           ...newRule,
                                           log: {...newRule.log, limit: event.target.value}
                                       })}
                            />
                        </Box>}
                        <FormControlLabel
                            label="审计日志"
                            control={
                                <Checkbox
                                    checked={newRule.audit}
                                    onChange={(event, checked) => setNewRule({
                                        ...newRule,
                                        audit: checked
                                    })}
                                />}
                        />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => setDialogOpen(false)}>
                        取消
                    </Button>
                    <Button onClick={() => handleAdd()} autoFocus>
                        添加
                    </Button>
                </DialogActions>
            </Dialog>
        </TabPanel>
    )
})

export default TrafficRulePanel
