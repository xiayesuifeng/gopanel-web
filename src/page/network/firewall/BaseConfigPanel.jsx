import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react'
import { useSnackbar } from 'notistack'
import { FirewallApi } from '../../../api/firewall'
import TabPanel from '@mui/lab/TabPanel'
import LabelPaper from '../../../component/LabelPaper'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Chip from '@mui/material/Chip'
import Checkbox from '@mui/material/Checkbox'
import Button from '@mui/material/Button'
import TableFooter from '@mui/material/TableFooter'
import ToggleButton from '@mui/material/ToggleButton'
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'
import Box from '@mui/material/Box'
import Tooltip from '@mui/material/Tooltip'
import { NetworkApi } from '../../../api/network'
import Dialog from '@mui/material/Dialog'
import { useTheme } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogActions from '@mui/material/DialogActions'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import { FormLabel, OutlinedInput } from '@mui/material'
import { protocols } from './const'
import ListItemText from '@mui/material/ListItemText'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import DeleteIcon from '@mui/icons-material/Delete'
import IconButton from '@mui/material/IconButton'
import Divider from '@mui/material/Divider'
import AddCircleIcon from '@mui/icons-material/AddCircle'

const defaultZone = {
    name: '',
    description: '',
    target: 'default',
    ingressPriority: 0,
    egressPriority: 0,
    icmpBlocks: [],
    icmpBlockInversion: false,
    masquerade: false,
    forward: false,
    interfaces: [],
    protocols: [],
}

const defaultPolicy = {
    name: '',
    short: '',
    description: '',
    target: 'CONTINUE',
    ingressZones: [],
    egressZones: [],
    services: [],
    icmpBlocks: [],
    priority: 0,
    masquerade: false,
    forwardPorts: [],
    richRules: [],
    protocols: [],
    ports: [],
    sourcePorts: [],
}

const BaseConfigPanel = forwardRef((props, ref) => {
    const {config, zoneNames, onConfigChange} = props
    const [permanent, setPermanent] = useState(false)

    const [zones, setZones] = useState([])
    const [policies, setPolicies] = useState([])
    const [services, setServices] = useState([])
    const [icmpTypes, setICMPTypes] = useState([])

    const [interfaces, setInterfaces] = useState([])

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'))
    const [zoneDialogOpen, setZoneDialogOpen] = useState(false)
    const [zoneEdit, setZoneEdit] = useState(false)
    const [zone, setZone] = useState(defaultZone)

    const [policyDialogOpen, setPolicyDialogOpen] = useState(false)
    const [policyEdit, setPolicyEdit] = useState(false)
    const [policy, setPolicy] = useState(defaultPolicy)
    const [policyNewPort, setPolicyNewPort] = useState({port: '', protocol: 'tcp'})
    const [policyNewSourcePort, setPolicyNewSourcePort] = useState({port: '', protocol: 'tcp'})
    const [policyNewForwardPort, setPolicyNewForwardPort] = useState({
        port: '',
        protocol: 'tcp',
        toPort: '',
        toAddress: ''
    })
    const [policyNewRichRule, setPolicyNewRichRule] = useState('')

    const {enqueueSnackbar} = useSnackbar()

    useImperativeHandle(ref, () => {
        return {
            reloadTrigger () {
                if (!permanent)
                    getAll()
            },
            resetTrigger () {
                getAll()
            }
        }
    }, [])

    useEffect(() => {
        NetworkApi.getDevices().then(data => setInterfaces(data.map(device => device.name)))
    }, [])

    useEffect(() => getAll(), [permanent])

    const getAll = () => {
        getZones()
        getPolicies()

        FirewallApi.getServiceNames(permanent)
            .then(data => setServices(data))
            .catch((msg) => enqueueSnackbar('获取服务列表失败，错误:' + msg, {variant: 'error'}))

        FirewallApi.getICMPTypeNames(permanent)
            .then(data => setICMPTypes(data))
            .catch((msg) => enqueueSnackbar('获取 ICMP 类型列表失败，错误:' + msg, {variant: 'error'}))
    }

    const getZones = () => {
        FirewallApi.getZones(permanent)
            .then((data) => setZones(data))
            .catch((msg) =>
                enqueueSnackbar('获取区域列表失败，错误:' + msg, {variant: 'error'}),
            )
    }

    const getPolicies = () => {
        FirewallApi.getPolicies(permanent)
            .then((data) => setPolicies(data))
            .catch((err) =>
                enqueueSnackbar('获取策略列表失败，错误:' + err.toString(), {variant: 'error'}),
            )
    }

    const handleZoneTargetChange = (zone, target) => {
        zone.target = target

        handleZoneChange(zone)
    }

    const handleZoneForwardChange = (zone, forward) => {
        zone.forward = forward

        handleZoneChange(zone)
    }

    const handleZoneMasqueradeChange = (zone, masquerade) => {
        zone.masquerade = masquerade

        handleZoneChange(zone)
    }

    const handleZoneChange = (zone) => {
        let name = zone.name

        if (zone.newName)
            zone.name = zone.newName

        FirewallApi.updateZone(name, zone, permanent)
            .then(data => getZones())
            .catch((err) => enqueueSnackbar('修改区域失败，错误:' + err.toString(), {variant: 'error'}))
    }

    const handleZoneAdd = () => {
        FirewallApi.addZone(zone)
            .then(data => getZones())
            .catch((err) => enqueueSnackbar('添加区域失败，错误:' + err.toString(), {variant: 'error'}))
    }

    const handleZoneRemove = name => {
        FirewallApi.removeZone(name)
            .then(data => getZones())
            .catch((err) => enqueueSnackbar('删除区域失败，错误:' + err.toString(), {variant: 'error'}))
    }

    const handlePolicyAdd = () => {
        FirewallApi.addPolicy(policy)
            .then(data => getPolicies())
            .catch((err) => enqueueSnackbar('添加策略失败，错误:' + err.toString(), {variant: 'error'}))
    }

    const handlePolicyChange = () => {
        let name = policy.name

        if (policy.newName)
            policy.name = policy.newName

        FirewallApi.updatePolicy(name, policy, permanent)
            .then(data => getPolicies())
            .catch((err) => enqueueSnackbar('修改策略失败，错误:' + err.toString(), {variant: 'error'}))
    }

    const getPolicyTargetText = target => {
        switch (target) {
            case 'ACCEPT':
                return '接受'
            case 'REJECT':
                return '拒绝'
            case 'CONTINUE':
                return 'Continue'
            case 'DROP':
                return '丢弃'
            default:
                return ''
        }
    }

    return (
        <TabPanel value={'1'} sx={{p: 0}}>
            <Box sx={{mt: 3, display: 'flex', justifyContent: 'flex-end'}}>
                <ToggleButtonGroup
                    color="primary"
                    size="small"
                    value={permanent}
                    exclusive
                    onChange={(event, value) => {
                        if (value != null)
                            setPermanent(value)
                    }}
                >
                    <ToggleButton sx={{minWidth: 80}} value={false}>运行时</ToggleButton>
                    <ToggleButton sx={{minWidth: 80}} value={true}>持久性</ToggleButton>
                </ToggleButtonGroup>
            </Box>
            <LabelPaper label={'基础设置'}>
                <FormControl sx={{m: 2, minWidth: 150}}>
                    <InputLabel>默认区域</InputLabel>
                    <Select
                        value={config.defaultZone}
                        label="默认区域"
                        onChange={(event) => {
                            let oldZone = config.defaultZone
                            onConfigChange({defaultZone: event.target.value})
                            FirewallApi.updateConfig({defaultZone: event.target.value})
                                .catch(err => {
                                    onConfigChange({defaultZone: oldZone})
                                    enqueueSnackbar('修改默认区域失败，错误:' + err.toString(), {variant: 'error'})
                                })
                        }}
                    >
                        {zoneNames.map((name) => (
                            <MenuItem value={name}>{name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </LabelPaper>
            <LabelPaper label={'区域'}>
                <TableContainer>
                    <Table sx={{minWidth: 650}} size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell>名称</TableCell>
                                <TableCell align={'center'}>接口</TableCell>
                                <TableCell align={'center'}>入站策略</TableCell>
                                <TableCell align={'center'}>转发</TableCell>
                                <TableCell align={'center'}>IP 动态伪装</TableCell>
                                <TableCell align={'right'}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {zones.map((zone) => (
                                <TableRow
                                    key={zone.name}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {zone.name}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {zone.interfaces.map((iface) => (
                                            <Chip label={iface} sx={{m: 0.6}}/>
                                        ))}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        <Tooltip title={permanent ? '' : '仅支持在持久性下修改'}>
                                            <FormControl sx={{minWidth: 100}} size="small">
                                                <Select
                                                    variant={'standard'}
                                                    value={zone.target}
                                                    disabled={!permanent}
                                                    onChange={(event) =>
                                                        handleZoneTargetChange(zone, event.target.value)
                                                    }
                                                >
                                                    <MenuItem value={'default'}>默认</MenuItem>
                                                    <MenuItem value={'ACCEPT'}>接受</MenuItem>
                                                    <MenuItem value={'%%REJECT%%'}>拒绝</MenuItem>
                                                    <MenuItem value={'DROP'}>丢弃</MenuItem>
                                                    <MenuItem value={'CONTINUE'}>Continue</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Tooltip>
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        <Checkbox checked={zone.forward}
                                                  onChange={(event, checked) => handleZoneForwardChange(zone, checked)}/>
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        <Checkbox checked={zone.masquerade}
                                                  onChange={(event, checked) => handleZoneMasqueradeChange(zone, checked)}/>
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <Button variant="contained" size={'small'} sx={{m: 1}} onClick={() => {
                                            setZoneEdit(true)
                                            setZone(zone)
                                            setZoneDialogOpen(true)
                                        }}>
                                            编辑
                                        </Button>
                                        <Button variant="contained" size={'small'} sx={{m: 1}} disabled={!permanent}
                                                onClick={() => handleZoneRemove(zone.name)}>
                                            删除
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <Button disabled={!permanent}
                                        variant="contained"
                                        size={'small'}
                                        sx={{m: 1}}
                                        onClick={() => {
                                            setZoneEdit(false)
                                            setZone(defaultZone)
                                            setZoneDialogOpen(true)
                                        }}>
                                    添加
                                </Button>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </LabelPaper>
            <LabelPaper label={'策略'}>
                <TableContainer>
                    <Table sx={{minWidth: 650}} size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell>名称</TableCell>
                                <TableCell align={'center'}>短描述</TableCell>
                                <TableCell align={'center'}>入站 => 出站</TableCell>
                                <TableCell align={'center'}>策略</TableCell>
                                <TableCell align={'center'}>优先级</TableCell>
                                <TableCell align={'center'}>IP 动态伪装</TableCell>
                                <TableCell align={'right'}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {policies.map((policy) => (
                                <TableRow
                                    key={policy.name}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {policy.name}
                                    </TableCell>
                                    <TableCell align={'left'}>
                                        {policy.short}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        <>
                                            {policy.ingressZones.map((zone) => (
                                                <Chip label={zone} sx={{m: 0.6}}/>
                                            ))}
                                            =>
                                            {policy.egressZones.map((zone) => (
                                                <Chip label={zone} sx={{m: 0.6}}/>
                                            ))}
                                        </>
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {getPolicyTargetText(policy.target)}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        {policy.priority}
                                    </TableCell>
                                    <TableCell align={'center'}>
                                        <Checkbox checked={policy.masquerade} disabled/>
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <Button variant="contained" size={'small'} sx={{m: 1}} onClick={() => {
                                            setPolicyEdit(true)
                                            setPolicy(policy)
                                            setPolicyDialogOpen(true)
                                        }}>
                                            编辑
                                        </Button>
                                        <Button variant="contained" size={'small'} sx={{m: 1}} disabled={!permanent}>
                                            删除
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <Button disabled={!permanent}
                                        variant="contained"
                                        size={'small'}
                                        sx={{m: 1}}
                                        onClick={() => {
                                            setPolicyEdit(false)
                                            setPolicy(defaultPolicy)
                                            setPolicyDialogOpen(true)
                                        }}>
                                    添加
                                </Button>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </LabelPaper>
            <Dialog
                maxWidth={'md'}
                fullWidth
                fullScreen={fullScreen}
                open={zoneDialogOpen}
                onClose={() => setZoneDialogOpen(false)}
                aria-labelledby="zone-dialog-title"
            >
                <DialogTitle id="zone-dialog-title">
                    {zoneEdit ? '编辑区域' : '新建区域'}
                </DialogTitle>
                <Box display="flex" flexDirection="column" component={DialogContent}>
                    <TextField
                        sx={{mt: 2}}
                        label={'名称'}
                        disabled={!permanent}
                        value={zone.newName ?? zone.name}
                        onChange={event => {
                            zoneEdit ? setZone({...zone, newName: event.target.value}) : setZone({
                                ...zone,
                                name: event.target.value
                            })
                        }}
                    />
                    <TextField
                        sx={{mt: 2}}
                        multiline
                        disabled={!permanent}
                        label={'描述'}
                        value={zone.description}
                        onChange={event => setZone({...zone, description: event.target.value})}
                    />
                    <FormControl sx={{mt: 2}}>
                        <FormLabel>入站策略</FormLabel>
                        <ToggleButtonGroup
                            fullWidth
                            disabled={!permanent}
                            color="primary"
                            value={zone.target}
                            exclusive
                            onChange={(event, value) => {
                                if (value)
                                    setZone({...zone, target: value})
                            }}
                        >
                            <ToggleButton value={'default'}>默认</ToggleButton>
                            <ToggleButton value={'ACCEPT'}>接受</ToggleButton>
                            <ToggleButton value={'%%REJECT%%'}>拒绝</ToggleButton>
                            <ToggleButton value={'DROP'}>丢弃</ToggleButton>
                            <ToggleButton value={'CONTINUE'}>Continue</ToggleButton>
                        </ToggleButtonGroup>
                    </FormControl>
                    <Box sx={{display: 'flex'}}>
                        <TextField
                            sx={{mt: 2, width: 100}}
                            label={'入站优先级'}
                            type="number"
                            value={zone.ingressPriority}
                            onChange={event => setZone({
                                ...zone,
                                ingressPriority: parseInt(event.target.value ? event.target.value : '0')
                            })}
                        />
                        <TextField
                            sx={{mt: 2, ml: 2, width: 100}}
                            label={'出站优先级'}
                            type="number"
                            value={zone.egressPriority}
                            onChange={event => setZone({
                                ...zone,
                                egressPriority: parseInt(event.target.value ? event.target.value : '0')
                            })}
                        />
                    </Box>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>接口</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={zone.interfaces}
                            onChange={(e) => setZone({
                                ...zone,
                                interfaces: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {interfaces.map((iface) => (
                                <MenuItem
                                    key={iface}
                                    value={iface}
                                >
                                    <Checkbox checked={zone.interfaces.indexOf(iface) > -1}/>
                                    <ListItemText primary={iface}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>协议</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={zone.protocols}
                            onChange={(e) => setZone({
                                ...zone,
                                protocols: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {protocols.map((protocol) => (
                                <MenuItem
                                    key={protocol}
                                    value={protocol}
                                >
                                    <Checkbox checked={zone.protocols.indexOf(protocol) > -1}/>
                                    <ListItemText primary={protocol}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>ICMP 黑名单</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={zone.icmpBlocks}
                            onChange={(e) => setZone({
                                ...zone,
                                icmpBlocks: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {icmpTypes.map((icmpType) => (
                                <MenuItem
                                    key={icmpType}
                                    value={icmpType}
                                >
                                    <Checkbox checked={zone.icmpBlocks.indexOf(icmpType) > -1}/>
                                    <ListItemText primary={icmpType}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControlLabel
                        label="反转 ICMP 黑名单"
                        control={
                            <Checkbox
                                checked={zone.icmpBlockInversion}
                                onChange={(event, checked) => setZone({
                                    ...zone,
                                    icmpBlockInversion: checked
                                })}
                            />}
                    />
                    <FormControlLabel
                        label="转发"
                        control={
                            <Checkbox
                                checked={zone.forward}
                                onChange={(event, checked) => setZone({
                                    ...zone,
                                    forward: checked
                                })}
                            />}
                    />
                    <FormControlLabel
                        label="IP 动态伪装"
                        control={
                            <Checkbox
                                checked={zone.masquerade}
                                onChange={(event, checked) => setZone({
                                    ...zone,
                                    masquerade: checked
                                })}
                            />}
                    />
                </Box>
                <DialogActions>
                    <Button autoFocus onClick={() => setZoneDialogOpen(false)}>
                        取消
                    </Button>
                    <Button onClick={() => {
                        zoneEdit ? handleZoneChange(zone) : handleZoneAdd()
                        setZoneDialogOpen(false)
                    }} autoFocus>
                        完成
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                maxWidth={'md'}
                fullWidth
                fullScreen={fullScreen}
                open={policyDialogOpen}
                onClose={() => setPolicyDialogOpen(false)}
                aria-labelledby="policy-dialog-title"
            >
                <DialogTitle id="policy-dialog-title">
                    {zoneEdit ? '编辑策略' : '新建策略'}
                </DialogTitle>
                <Box display="flex" flexDirection="column" component={DialogContent}>
                    <TextField
                        sx={{mt: 2}}
                        label={'名称'}
                        disabled={!permanent}
                        value={policy.newName ?? policy.name}
                        onChange={event => {
                            policyEdit
                                ? setPolicy({...policy, newName: event.target.value})
                                : setPolicy({...policy, name: event.target.value})
                        }}
                    />
                    <TextField
                        sx={{mt: 2}}
                        disabled={!permanent}
                        label={'短描述'}
                        value={policy.short}
                        onChange={event => setPolicy({...policy, short: event.target.value})}
                    />
                    <TextField
                        sx={{mt: 2}}
                        multiline
                        disabled={!permanent}
                        label={'描述'}
                        value={policy.description}
                        onChange={event => setPolicy({...policy, description: event.target.value})}
                    />
                    <FormControl sx={{mt: 2}}>
                        <FormLabel>策略</FormLabel>
                        <ToggleButtonGroup
                            fullWidth
                            disabled={!permanent}
                            color="primary"
                            value={policy.target}
                            exclusive
                            onChange={(event, value) => {
                                if (value)
                                    setPolicy({...policy, target: value})
                            }}
                        >
                            <ToggleButton value={'ACCEPT'}>接受</ToggleButton>
                            <ToggleButton value={'REJECT'}>拒绝</ToggleButton>
                            <ToggleButton value={'DROP'}>丢弃</ToggleButton>
                            <ToggleButton value={'CONTINUE'}>Continue</ToggleButton>
                        </ToggleButtonGroup>
                    </FormControl>
                    <TextField
                        sx={{mt: 2}}
                        fullWidth
                        label={'优先级'}
                        type="number"
                        value={policy.priority}
                        onChange={event => setPolicy({
                            ...policy,
                            priority: parseInt(event.target.value ? event.target.value : '0')
                        })}
                    />
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>入站区域</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={policy.ingressZones}
                            onChange={(e) => setPolicy({
                                ...policy,
                                ingressZones: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            <MenuItem
                                key={'ingress-host'}
                                value={'HOST'}
                            >
                                <Checkbox checked={policy.ingressZones.indexOf('HOST') > -1}/>
                                <ListItemText primary={'HOST'}/>
                            </MenuItem>
                            <MenuItem
                                key={'ingress-any'}
                                value={'ANY'}
                            >
                                <Checkbox checked={policy.ingressZones.indexOf('ANY') > -1}/>
                                <ListItemText primary={'ANY'}/>
                            </MenuItem>
                            {zones.map((zone) => (
                                <MenuItem
                                    key={`ingress-${zone.name}`}
                                    value={zone.name}
                                >
                                    <Checkbox checked={policy.ingressZones.indexOf(zone.name) > -1}/>
                                    <ListItemText primary={zone.name}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>出站区域</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={policy.egressZones}
                            onChange={(e) => setPolicy({
                                ...policy,
                                egressZones: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            <MenuItem
                                key={'egress-host'}
                                value={'HOST'}
                            >
                                <Checkbox checked={policy.egressZones.indexOf('HOST') > -1}/>
                                <ListItemText primary={'HOST'}/>
                            </MenuItem>

                            <MenuItem
                                key={'egress-any'}
                                value={'ANY'}
                            >
                                <Checkbox checked={policy.egressZones.indexOf('ANY') > -1}/>
                                <ListItemText primary={'ANY'}/>
                            </MenuItem>
                            {zones.map((zone) => (
                                <MenuItem
                                    key={`egress-${zone.name}`}
                                    value={zone.name}
                                >
                                    <Checkbox checked={policy.egressZones.indexOf(zone.name) > -1}/>
                                    <ListItemText primary={zone.name}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>服务</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={policy.services}
                            onChange={(e) => setPolicy({
                                ...policy,
                                services: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {services.map((service) => (
                                <MenuItem
                                    key={service}
                                    value={service}
                                >
                                    <Checkbox checked={policy.services.indexOf(service) > -1}/>
                                    <ListItemText primary={service}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>协议</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={policy.protocols}
                            onChange={(e) => setPolicy({
                                ...policy,
                                protocols: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {protocols.map((protocol) => (
                                <MenuItem
                                    key={protocol}
                                    value={protocol}
                                >
                                    <Checkbox checked={policy.protocols.indexOf(protocol) > -1}/>
                                    <ListItemText primary={protocol}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl sx={{mt: 2}}>
                        <InputLabel>ICMP 黑名单</InputLabel>
                        <Select
                            fullWidth
                            multiple
                            value={policy.icmpBlocks}
                            onChange={(e) => setPolicy({
                                ...policy,
                                icmpBlocks: e.target.value === 'string' ? e.target.value.split(',') : e.target.value
                            })}
                            input={<OutlinedInput label="Chip"/>}
                            renderValue={(selected) => (
                                <Box sx={{display: 'flex', flexWrap: 'wrap', gap: 0.5}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value}/>
                                    ))}
                                </Box>
                            )}
                        >
                            {icmpTypes.map((icmpType) => (
                                <MenuItem
                                    key={icmpType}
                                    value={icmpType}
                                >
                                    <Checkbox checked={policy.icmpBlocks.indexOf(icmpType) > -1}/>
                                    <ListItemText primary={icmpType}/>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Box sx={{mt: 2}}>
                        <InputLabel>端口</InputLabel>
                        <List>
                            {policy.ports.map((port) => (
                                <ListItem
                                    secondaryAction={
                                        <IconButton onClick={() => setPolicy({
                                            ...policy,
                                            ports: policy.ports.filter(p => p !== port)
                                        })}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    }
                                >
                                    <ListItemText
                                        primary={port.port}
                                        secondary={port.protocol}
                                    />
                                </ListItem>
                            ))}
                        </List>
                        <Box sx={{mt: 1, display: 'flex', alignItems: 'center'}}>
                            <TextField sx={{flexGrow: 1}}
                                       label={'端口'}
                                       helperText="端口号或范围，如：8080 或 8080-8090"
                                       placeholder="number[-number]"
                                       value={policyNewPort.port}
                                       onChange={event => setPolicyNewPort({
                                           ...policyNewPort,
                                           port: event.target.value
                                       })}
                            />
                            <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                            <TextField
                                select
                                sx={{minWidth: 80}}
                                helperText="协议"
                                value={policyNewPort.protocol}
                                onChange={(event) => setPolicyNewPort({
                                    ...policyNewPort,
                                    protocol: event.target.value
                                })}
                            >
                                <MenuItem value={'tcp'}>TCP</MenuItem>
                                <MenuItem value={'udp'}>UDP</MenuItem>
                            </TextField>
                            <IconButton
                                size={'large'}
                                disabled={!policyNewPort.port}
                                color={'primary'}
                                onClick={() => {
                                    setPolicy({
                                        ...policy,
                                        ports: [...policy.ports, policyNewPort]
                                    })
                                    setPolicyNewPort({port: '', protocol: 'tcp'})
                                }}>
                                <AddCircleIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                    </Box>
                    <Box sx={{mt: 2}}>
                        <InputLabel>源端口</InputLabel>
                        <List>
                            {policy.sourcePorts.map((port) => (
                                <ListItem
                                    secondaryAction={
                                        <IconButton onClick={() => setPolicy({
                                            ...policy,
                                            sourcePorts: policy.sourcePorts.filter(p => p !== port)
                                        })}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    }
                                >
                                    <ListItemText
                                        primary={port.port}
                                        secondary={port.protocol}
                                    />
                                </ListItem>
                            ))}
                        </List>
                        <Box sx={{mt: 1, display: 'flex', alignItems: 'center'}}>
                            <TextField sx={{flexGrow: 1}}
                                       label={'端口'}
                                       helperText="端口号或范围，如：8080 或 8080-8090"
                                       placeholder="number[-number]"
                                       value={policyNewSourcePort.port}
                                       onChange={event => setPolicyNewSourcePort({
                                           ...policyNewSourcePort,
                                           port: event.target.value
                                       })}
                            />
                            <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                            <TextField
                                select
                                sx={{minWidth: 80}}
                                helperText="协议"
                                value={policyNewSourcePort.protocol}
                                onChange={(event) => setPolicyNewSourcePort({
                                    ...policyNewSourcePort,
                                    protocol: event.target.value
                                })}
                            >
                                <MenuItem value={'tcp'}>TCP</MenuItem>
                                <MenuItem value={'udp'}>UDP</MenuItem>
                            </TextField>
                            <IconButton
                                size={'large'}
                                disabled={!policyNewSourcePort.port}
                                color={'primary'}
                                onClick={() => {
                                    setPolicy({
                                        ...policy,
                                        sourcePorts: [...policy.sourcePorts, policyNewSourcePort]
                                    })
                                    setPolicyNewSourcePort({port: '', protocol: 'tcp'})
                                }}>
                                <AddCircleIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                    </Box>
                    <Box sx={{mt: 2}}>
                        <InputLabel>转发端口</InputLabel>
                        <List>
                            {policy.forwardPorts.map((forwardPort) => (
                                <ListItem
                                    secondaryAction={
                                        <IconButton onClick={() => setPolicy({
                                            ...policy,
                                            forwardPorts: policy.forwardPorts.filter(f => f !== forwardPort)
                                        })}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    }
                                >
                                    <ListItemText
                                        primary={`${forwardPort.port} => ${forwardPort.toAddress ? forwardPort.toAddress : '127.0.0.1'}:${forwardPort.toPort}`}
                                        secondary={forwardPort.protocol}/>
                                </ListItem>
                            ))}
                        </List>
                        <Box sx={{mt: 1, display: 'flex', alignItems: 'center'}}>
                            <TextField sx={{flexGrow: 1}}
                                       label={'端口'}
                                       helperText="端口号或范围，如：8080 或 8080-8090"
                                       placeholder="number[-number]"
                                       value={policyNewForwardPort.port}
                                       onChange={event => setPolicyNewForwardPort({
                                           ...policyNewForwardPort,
                                           port: event.target.value
                                       })}
                            />
                            <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                            <TextField
                                select
                                sx={{minWidth: 80}}
                                helperText="协议"
                                value={policyNewForwardPort.protocol}
                                onChange={(event) => setPolicyNewForwardPort({
                                    ...policyNewForwardPort,
                                    protocol: event.target.value
                                })}
                            >
                                <MenuItem value={'tcp'}>TCP</MenuItem>
                                <MenuItem value={'udp'}>UDP</MenuItem>
                                <MenuItem value={'sctp'}>SCTP</MenuItem>
                                <MenuItem value={'dccp'}>DCCP</MenuItem>
                            </TextField>
                            <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                            <TextField sx={{width: 180}}
                                       label={'目标 IP'}
                                       helperText="出站区域不为 HOST 时必填"
                                       placeholder="127.0.0.1"
                                       value={policyNewForwardPort.toAddress}
                                       onChange={event => setPolicyNewForwardPort({
                                           ...policyNewForwardPort,
                                           toAddress: event.target.value
                                       })}
                            />
                            <Divider sx={{height: 48, m: 0.5, alignSelf: 'baseline'}} orientation="vertical"/>
                            <TextField sx={{minWidth: 100}}
                                       label={'目标端口'}
                                       helperText="端口号或范围，如：8080 或 8080-8090"
                                       placeholder="number[-number]"
                                       value={policyNewForwardPort.toPort}
                                       onChange={event => setPolicyNewForwardPort({
                                           ...policyNewForwardPort,
                                           toPort: event.target.value
                                       })}
                            />
                            <IconButton
                                size={'large'}
                                disabled={!(policyNewForwardPort.port && policyNewForwardPort.toPort)}
                                color={'primary'}
                                onClick={() => {
                                    setPolicy({
                                        ...policy,
                                        forwardPorts: [...policy.forwardPorts, policyNewForwardPort]
                                    })
                                    setPolicyNewForwardPort({port: '', protocol: 'tcp', toPort: '', toAddress: ''})
                                }}>
                                <AddCircleIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                    </Box>
                    <Box sx={{mt: 2}}>
                        <InputLabel>Rich Rules</InputLabel>
                        <List>
                            {policy.richRules.map((rule) => (
                                <ListItem
                                    secondaryAction={
                                        <IconButton onClick={() => setPolicy({
                                            ...policy,
                                            richRules: policy.richRules.filter(r => r !== rule)
                                        })}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    }
                                >
                                    <ListItemText primary={rule}/>
                                </ListItem>
                            ))}
                        </List>
                        <Box sx={{mt: 1, display: 'flex', alignItems: 'center'}}>
                            <TextField sx={{flexGrow: 1}}
                                       placeholder="rule ..."
                                       value={policyNewRichRule}
                                       onChange={event => setPolicyNewRichRule(event.target.value)}
                            />
                            <IconButton
                                size={'large'}
                                disabled={!policyNewRichRule}
                                color={'primary'}
                                onClick={() => {
                                    setPolicy({
                                        ...policy,
                                        richRules: [...policy.richRules, policyNewRichRule]
                                    })
                                    setPolicyNewRichRule('')
                                }}>
                                <AddCircleIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                    </Box>
                    <FormControlLabel
                        label="IP 动态伪装"
                        control={
                            <Checkbox
                                checked={policy.masquerade}
                                onChange={(event, checked) => setPolicy({
                                    ...policy,
                                    masquerade: checked
                                })}
                            />}
                    />
                </Box>
                <DialogActions>
                    <Button autoFocus onClick={() => setPolicyDialogOpen(false)}>
                        取消
                    </Button>
                    <Button onClick={() => {
                        policyEdit ? handlePolicyChange() : handlePolicyAdd()
                        setPolicyDialogOpen(false)
                    }} autoFocus>
                        完成
                    </Button>
                </DialogActions>
            </Dialog>
        </TabPanel>
    )
})

export default BaseConfigPanel