import React, { useEffect, useRef, useState } from 'react'
import TabContext from '@mui/lab/TabContext'
import Paper from '@mui/material/Paper'
import TabList from '@mui/lab/TabList'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import { FirewallApi } from '../../../api/firewall'
import { useSnackbar } from 'notistack'
import BaseConfigPanel from './BaseConfigPanel'
import TrafficRulePanel from './TrafficRulePanel'
import PortForwardPanel from './PortForwardPanel'
import SpeedDial from '@mui/lab/SpeedDial'
import SpeedDialAction from '@mui/lab/SpeedDialAction'
import SpeedDialIcon from '@mui/lab/SpeedDialIcon'
import { Reload as ReloadIcon, Restore as RestoreIcon } from 'mdi-material-ui'

const fabStyle = {
    position: 'fixed',
    bottom: 24,
    right: 24,
    zIndex: (theme) => theme.zIndex.fab
}

export default function Firewall (props) {
    const [tabValue, setTabValue] = useState('1')

    const [config, setConfig] = useState({
        defaultZone: '',
    })
    const [zoneNames, setZoneNames] = useState([])

    const baseConfigRef = useRef(null)
    const portForwardRef = useRef(null)
    const trafficRuleRef = useRef(null)

    const {enqueueSnackbar} = useSnackbar()

    useEffect(() => getAll(), [])

    const getAll = () => {
        FirewallApi.getConfig()
            .then((data) => setConfig(data))
            .catch((msg) =>
                enqueueSnackbar('获取配置失败，错误:' + msg, {variant: 'error'}),
            )

        FirewallApi.getZoneNames(true)
            .then((data) => setZoneNames(data))
            .catch((msg) =>
                enqueueSnackbar('获取区域名称列表失败，错误:' + msg, {
                    variant: 'error',
                }),
            )
    }

    return (
        <Box>
            <TabContext value={tabValue}>
                <Paper sx={{borderRadius: 4, pl: 1, pr: 1}}>
                    <TabList onChange={(event, newValue) => setTabValue(newValue)}>
                        <Tab label={'基础设置'} value={'1'}/>
                        <Tab label={'流量规则'} value={'2'}/>
                        <Tab label={'端口转发'} value={'3'}/>
                    </TabList>
                </Paper>
                <BaseConfigPanel
                    ref={baseConfigRef}
                    config={config}
                    zoneNames={zoneNames}
                    onConfigChange={setConfig}
                />
                <TrafficRulePanel
                    ref={trafficRuleRef}
                    defaultZone={config.defaultZone}
                    zoneNames={zoneNames}
                />
                <PortForwardPanel
                    ref={portForwardRef}
                    defaultZone={config.defaultZone}
                    zoneNames={zoneNames}
                />
            </TabContext>
            <SpeedDial
                ariaLabel="SpeedDial basic example"
                sx={fabStyle}
                icon={<SpeedDialIcon/>}
            >
                <SpeedDialAction
                    icon={<ReloadIcon/>}
                    tooltipOpen
                    tooltipTitle={'重载防火墙'}
                    onClick={() => {
                        FirewallApi.reload()
                            .then(() => {
                                baseConfigRef.current.reloadTrigger()
                                trafficRuleRef.current.reloadTrigger()
                                portForwardRef.current.reloadTrigger()
                                enqueueSnackbar('防火墙已重载', {variant: 'success',})
                            })
                            .catch(err => enqueueSnackbar('重载防火墙失败，错误:' + err.toString(), {variant: 'error',}))
                    }}
                />
                <SpeedDialAction
                    icon={<RestoreIcon/>}
                    tooltipOpen
                    tooltipTitle={'恢复默认值'}
                    onClick={() => {
                        FirewallApi.reset()
                            .then(() => {
                                getAll()
                                baseConfigRef.current.resetTrigger()
                                trafficRuleRef.current.resetTrigger()
                                portForwardRef.current.resetTrigger()
                                enqueueSnackbar('防火墙已重载', {variant: 'success',})
                            })
                            .catch(err => enqueueSnackbar('恢复默认值失败，错误:' + err.toString(), {variant: 'error'}))
                    }}
                />
            </SpeedDial>
        </Box>
    )
}
