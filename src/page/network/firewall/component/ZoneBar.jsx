import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'
import ToggleButton from '@mui/material/ToggleButton'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import React from 'react'
import Box from '@mui/material/Box'

export default function ZoneBar (props) {
    const {zones, currentZone, onZoneChange, permanent, onPermanentChange} = props

    return (
        <Box sx={{mt: 3, display: 'flex', justifyContent: 'space-between'}}>
            <FormControl sx={{minWidth: 150}} size="small">
                <InputLabel>当前区域</InputLabel>
                <Select
                    value={currentZone}
                    label="当前区域"
                    onChange={(event) =>
                        onZoneChange(event.target.value)
                    }
                >
                    {zones.map((name) => (
                        <MenuItem value={name}>{name}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <ToggleButtonGroup
                color="primary"
                size="small"
                value={permanent}
                exclusive
                onChange={(event, value) => {
                    if (value !== null)
                        onPermanentChange(value)
                }}
            >
                <ToggleButton sx={{minWidth: 80}} value={false}>运行时</ToggleButton>
                <ToggleButton sx={{minWidth: 80}} value={true}>持久性</ToggleButton>
            </ToggleButtonGroup>
        </Box>
    )
}