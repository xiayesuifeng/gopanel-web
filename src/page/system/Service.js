import React, {useEffect, useState} from 'react'
import Typography from '@mui/material/Typography'
import Switch from '@mui/material/Switch'
import Paper from '@mui/material/Paper'
import CircularProgress from '@mui/material/CircularProgress'
import {useSnackbar} from 'notistack'
import {ServiceApi} from '../../api/service'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Checkbox from '@mui/material/Checkbox'
import Backdrop from '@mui/material/Backdrop'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContentText from '@mui/material/DialogContentText'
import Button from '@mui/material/Button'
import FormControlLabel from '@mui/material/FormControlLabel'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import Box from '@mui/material/Box'

export default function Service() {
    const [serviceList, setServiceList] = useState([])

    const [loading, setLoading] = useState(false)
    const [startingServices, setStartingServices] = useState([])
    const [enablingServices, setEnablingServices] = useState([])
    const [dialogOpen, setDialogOpen] = useState(false)
    const [stopService, setStopService] = useState({name: '', triggeredBy: []})
    const [stopTriggeredBy, setStopTriggeredBy] = useState(false)
    const [loadingService, setLoadingService] = useState(false)
    const [restartService, setRestartService] = useState('')

    const {enqueueSnackbar} = useSnackbar()

    useEffect(() => getService(), [])

    const getService = () => {
        setLoading(true)
        ServiceApi.getService()
            .then(data => setServiceList(data.data))
            .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
            .finally(() => setLoading(false))
    }

    const handleActiveStateChange = (service, checked) => {
        if (checked) {
            const name = service.name
            setStartingServices((prevState) => [name, ...prevState])
            ServiceApi.startService(name)
                .then(() => {
                    ServiceApi.getService()
                        .then(data => setServiceList(data.data))
                        .catch(err => enqueueSnackbar('获取失败，错误:' + err, {variant: 'error'}))
                        .finally(() => {
                            setStartingServices(prevState => [...prevState.filter(n => n !== name)])
                        })
                })
                .catch(msg => {
                    setStartingServices(prevState => [...prevState.filter(n => n !== name)])
                    enqueueSnackbar('启动失败，错误:' + msg, {variant: 'error'})
                })
        } else {
            setStopService(service)
            setStopTriggeredBy(false)
            setRestartService('')
            setDialogOpen(true)
        }
    }

    const handleRestartButtonClick = (name) => {
        setRestartService(name)
        setDialogOpen(true)
    }

    const handleStopService = () => {
        setLoadingService(true)
        ServiceApi.stopService(stopService.name, stopTriggeredBy)
            .then(() => {
                ServiceApi.getService()
                    .then(data => setServiceList(data.data))
                    .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
                    .finally(() => {
                        setLoadingService(false)
                        setDialogOpen(false)
                    })
            })
            .catch(msg => {
                setLoadingService(false)
                setDialogOpen(false)
                enqueueSnackbar('关闭失败，错误:' + msg, {variant: 'error'})
            })
    }

    const handleRestartService = () => {
        setLoadingService(true)
        ServiceApi.restartService(restartService)
            .then(() => {
                ServiceApi.getService()
                    .then(data => setServiceList(data.data))
                    .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
                    .finally(() => {
                        setLoadingService(false)
                        setDialogOpen(false)
                    })
            })
            .catch(msg => {
                setLoadingService(false)
                setDialogOpen(false)
                enqueueSnackbar('重启失败，错误:' + msg, {variant: 'error'})
            })
    }

    const handleEnableChange = (service, checked) => {
        setLoadingService(true)
        setEnablingServices((prevState) => [service.name, ...prevState])
        if (checked) {
            ServiceApi.enableService(service.name)
                .then(() => {
                    ServiceApi.getService()
                        .then(data => setServiceList(data.data))
                        .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
                        .finally(() => {
                            setEnablingServices(prevState => [...prevState.filter(n => n !== service.name)])
                        })
                })
                .catch(msg => {
                    setEnablingServices(prevState => [...prevState.filter(n => n !== service.name)])
                    enqueueSnackbar('启用失败，错误:' + msg, {variant: 'error'})
                })
        } else {
            ServiceApi.disableService(service.name)
                .then(() => {
                    ServiceApi.getService()
                        .then(data => setServiceList(data.data))
                        .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
                        .finally(() => {
                            setEnablingServices(prevState => [...prevState.filter(n => n !== service.name)])
                        })
                })
                .catch(msg => {
                    setEnablingServices(prevState => [...prevState.filter(n => n !== service.name)])
                    enqueueSnackbar('禁用失败，错误:' + msg, {variant: 'error'})
                })
        }
    }

    return (
        <div>
            <Backdrop sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}} open={loading}>
                <CircularProgress size={50} color="inherit"/>
                <Typography variant={'h3'}>加载中...</Typography>
            </Backdrop>
            <Dialog open={dialogOpen}>
                <DialogTitle>
                    警告
                </DialogTitle>
                <DialogContent>
                    {loadingService ? <Box
                            sx={{minWidth: 200, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                            <CircularProgress size={50} sx={{m: 1}} color="primary"/>
                            <Typography variant={'body1'}>{restartService ? '重启中...' : '关闭中...'}</Typography>
                        </Box>
                        : <>
                            <DialogContentText>
                                {restartService ? `确定要重启 ${restartService} ?` : `确定要关闭 ${stopService.name} ?`}
                            </DialogContentText>
                            {!restartService && stopService.triggeredBy.length > 0 &&
                                <FormControlLabel control={<Checkbox checked={stopTriggeredBy}
                                                                     onChange={e => setStopTriggeredBy(e.target.checked)}/>}
                                                  label="关闭以下 TriggeredBy"/>}
                            <Box pl={5}>
                                {!restartService && stopService.triggeredBy.map(triggeredBy => <Typography
                                    variant={'subtitle1'}>
                                    {triggeredBy}
                                </Typography>)}
                            </Box>
                        </>
                    }
                </DialogContent>
                {!loadingService &&
                    <DialogActions>
                        <Button onClick={() => setDialogOpen(false)}>取消</Button>
                        <Button onClick={restartService ? handleRestartService : handleStopService} autoFocus>
                            确定
                        </Button>
                    </DialogActions>}
            </Dialog>
            <Paper sx={{borderRadius: 4, pl: 1, pr: 1}}>
                <TableContainer>
                    <Table
                        sx={{minWidth: 750}}
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell>名称</TableCell>
                                <TableCell>描述</TableCell>
                                <TableCell align="center">运行中</TableCell>
                                <TableCell align="center">自动启动</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {serviceList.map(service => (
                                <TableRow
                                    key={service.name}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell>
                                        {service.name.substring(0, service.name.lastIndexOf('.service'))}
                                    </TableCell>
                                    <TableCell>{service.description}</TableCell>
                                    <TableCell align="center">
                                        {startingServices.indexOf(service.name) === -1
                                            ? <Switch
                                                onChange={e => handleActiveStateChange(service, e.target.checked)}
                                                checked={service.activeState}/>
                                            : <CircularProgress size={35} color="primary"/>}
                                    </TableCell>
                                    <TableCell align="center">
                                        {enablingServices.indexOf(service.name) === -1
                                            ? <Checkbox
                                                onChange={e => handleEnableChange(service, e.target.checked)}
                                                checked={service.enabled}/>
                                            : <CircularProgress size={35} color="primary"/>}
                                    </TableCell>
                                    <TableCell align="center">
                                        <Button variant={'outlined'}
                                                onClick={e => handleRestartButtonClick(service.name)}>重启</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    )
}
