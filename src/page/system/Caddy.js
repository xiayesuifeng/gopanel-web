import TabList from '@mui/lab/TabList'
import Tab from '@mui/material/Tab'
import React, {useContext, useEffect, useState} from 'react'
import TabContext from '@mui/lab/TabContext'
import TabPanel from '@mui/lab/TabPanel'
import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import Paper from '@mui/material/Paper'
import {CaddyApi} from '../../api/caddy'
import DnsChallengesTemplateEditor from '../../component/DnsChallengesTemplateEditor'
import WildcardDomainsEditor from '../../component/WildcardDomainsEditor'
import Fab from '@mui/material/Fab'
import DoneIcon from '@mui/icons-material/Done'
import CircularProgress from '@mui/material/CircularProgress'
import {useSnackbar} from 'notistack'
import DDNSTabPanel from './caddyPanel/DDNSTabPanel'
import {GlobalContext} from '../../globalState/Context'
import PluginTabPanel from './caddyPanel/PluginTabPanel'
import LabelPaper from '../../component/LabelPaper'

const fabStyle = {
    position: 'fixed',
    bottom: 24,
    right: 24,
    zIndex: (theme) => theme.zIndex.fab
}

export default function Caddy() {
    const {configuration, globalDispatch} = useContext(GlobalContext)

    const [tabValue, setTabValue] = useState('1')
    const [httpPort, setHttpPort] = useState(80)
    const [httpsPort, setHttpsPort] = useState(443)
    const [allowH2C, setAllowH2C] = useState(false)

    const [dnsChallenges, setDnsChallenges] = useState({})
    const [wildcardDomains, setWildcardDomains] = useState([])

    const [loading, setLoading] = useState(false)

    const {enqueueSnackbar} = useSnackbar()

    useEffect(() => {
        setHttpPort(configuration.general.HTTPPort)
        setHttpsPort(configuration.general.HTTPSPort)
        setAllowH2C(configuration.general.allowH2C)
        setDnsChallenges(configuration.tls.dnsChallenges)
        setWildcardDomains(configuration.tls.wildcardDomains)
    }, [configuration])

    const getConfiguration = () => {
        CaddyApi.getConfiguration()
            .then(data => globalDispatch({type: 'SET_CONFIGURATION', data: data.configuration}))
            .catch(msg => enqueueSnackbar('获取失败，错误:' + msg, {variant: 'error'}))
    }

    const handleDoneFabClick = () => {
        setLoading(true)

        CaddyApi.setConfiguration({
            general: {
                HTTPPort: httpPort,
                HTTPSPort: httpsPort,
                allowH2C: allowH2C
            },
            tls: {
                dnsChallenges: dnsChallenges,
                wildcardDomains: wildcardDomains
            }
        }).then(data => {
            enqueueSnackbar('更新成功', {variant: 'success'})

            getConfiguration()
        })
            .catch(err => enqueueSnackbar('更新失败，错误:' + err, {variant: 'error'}))
            .finally(() => setLoading(false))
    }

    return (
        <div>
            <TabContext value={tabValue}>
                <Paper sx={{borderRadius: 4, pl: 1, pr: 1}}>
                    <TabList onChange={(event, newValue) => setTabValue(newValue)}>
                        <Tab label={'设置'} value={'1'}/>
                        <Tab label={'动态 DNS'} value={'2'}/>
                        <Tab label={'插件管理'} value={'3'}/>
                        <Tab label={'日志'} value={'4'} disabled/>
                    </TabList>
                </Paper>
                <TabPanel value={'1'} sx={{p: 0}}>
                    <LabelPaper label={'常规设置'} >
                        <TextField inputProps={{inputMode: 'numeric', pattern: '[0-9]*'}} margin="normal"
                                   label={'HTTP 端口'} value={httpPort}
                                   onChange={e => setHttpPort(Number(e.target.value))}/>
                        <TextField inputProps={{inputMode: 'numeric', pattern: '[0-9]*'}} margin="normal"
                                   label={'HTTPS 端口'} value={httpsPort}
                                   onChange={e => setHttpsPort(Number(e.target.value))}/>
                        <FormControlLabel control={<Switch/>}
                                          label="启用 H2C（“明文 HTTP/2”或“H2 over TCP”）支持。"
                                          checked={allowH2C} onChange={e => setAllowH2C(e.target.checked)}/>
                    </LabelPaper>
                    <LabelPaper label={'TLS'} >
                        <DnsChallengesTemplateEditor value={dnsChallenges} onChange={setDnsChallenges}/>
                        <WildcardDomainsEditor value={wildcardDomains} onChange={setWildcardDomains}/>
                    </LabelPaper>
                    <Fab color={'primary'} sx={fabStyle} onClick={handleDoneFabClick} disabled={loading}>
                        {loading ? <CircularProgress size={31}/> : <DoneIcon/>}
                    </Fab>
                </TabPanel>
                <DDNSTabPanel value={'2'} sx={{p: 0}}/>
                <PluginTabPanel value={'3'} sx={{p: 0}} />
                <TabPanel value={'4'}>
                </TabPanel>
            </TabContext>
        </div>
    )
}
