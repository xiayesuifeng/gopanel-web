import TabPanel from '@mui/lab/TabPanel'
import React, { useContext, useEffect, useState } from 'react'
import { CaddyApi } from '../../../api/caddy'
import Paper from '@mui/material/Paper'
import { Stack, ToggleButton, ToggleButtonGroup } from '@mui/material'
import List from '@mui/material/List'
import Link from '@mui/material/Link'
import ListItem from '@mui/material/ListItem'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Unstable_Grid2'
import { GlobalContext } from '../../../globalState/Context'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Dialog from '@mui/material/Dialog'
import CircularProgress from '@mui/material/CircularProgress'
import DialogContentText from '@mui/material/DialogContentText'
import DialogActions from '@mui/material/DialogActions'
import { ServiceApi } from '../../../api/service'
import LabelPaper from '../../../component/LabelPaper'

export default function PluginTabPanel (props) {
    const {value} = props

    const {caddyModules, globalDispatch} = useContext(GlobalContext)

    const [allPlugins, setAllPlugins] = useState([])
    const [keyword, setKeyword] = useState('')

    const [category, setCategory] = useState(0)

    // 0 => close
    // 1 => installing
    // 2 => removing
    // 3 => restart caddy
    // 4 => restart caddy ing
    // 5 => error
    const [dialogState, setDialogState] = useState(0)
    const [dialogErrorText, setDialogErrorText] = useState('')

    useEffect(() => {
        CaddyApi.getOfficialPlugins()
            .then(data => setAllPlugins(data))
    }, [])

    const formatTime = time => {
        time = time.replace('T', ' ')
        time = time.replace('Z', '')
        return new Date(time).toLocaleDateString('af')
    }

    const getModules = () => CaddyApi.getModules()
        .then(data => globalDispatch({type: 'SET_CADDY_MODULES', data: data.modules}))

    const install = packageName => {
        setDialogState(1)

        CaddyApi.installPlugin({
            packages: [packageName]
        })
            .then(r => {
                setDialogState(3)
                getModules()
            })
            .catch(err => {
                setDialogErrorText(`安装失败: ${err.toString()}`)
                setDialogState(5)
            })
    }

    const remove = packageName => {
        setDialogState(2)

        CaddyApi.removePlugin([packageName])
            .then(r => {
                setDialogState(3)
                getModules()
            })
            .catch(err => {
                setDialogErrorText(`卸载失败: ${err.toString()}`)
                setDialogState(5)
            })
    }

    const restartService = () => {
        setDialogState(4)

        ServiceApi.restartService('caddy.service')
            .then(() => setDialogState(0))
            .catch(err => {
                setDialogErrorText(`重启 caddy 服务失败: ${err.toString()}`)
                setDialogState(5)
            })
    }

    return (
        <TabPanel value={value} sx={{p: 0}}>
            <LabelPaper>
                <Box display={'flex'} justifyContent={'space-between'} flexDirection={{xs: 'column', sm: 'row'}}>
                    <ToggleButtonGroup
                        color="primary"
                        value={category}
                        exclusive
                        onChange={(e) => setCategory(Number(e.target.value))}
                    >
                        <ToggleButton value={0}>全部</ToggleButton>
                        <ToggleButton value={1}>已安装</ToggleButton>
                        <ToggleButton value={2}>可安装</ToggleButton>
                    </ToggleButtonGroup>

                    <TextField placeholder={'过滤模块或者包'} value={keyword}
                               onChange={e => setKeyword(e.target.value)}/>
                </Box>
                <List>
                    {allPlugins
                        .filter(plugin => {
                            switch (category) {
                                case 1:
                                    return caddyModules.nonStandard.hasOwnProperty(plugin.path)
                                case 2:
                                    return !caddyModules.nonStandard.hasOwnProperty(plugin.path)
                            }

                            return true
                        })
                        .filter(plugin => plugin.path.includes(keyword)
                            || plugin.modules?.filter(module => module.name.includes(keyword)).length > 0)
                        .map(plugin => (
                            <ListItem sx={{pl: 0, pr: 0}} key={plugin.id}>
                                <Paper sx={{width: '100%', p: 3}}>
                                    <Box display={'flex'}>
                                        <Box sx={{fontSize: 48, pr: 2}}>📦</Box>
                                        <Grid container spacing={2} flex={1}>
                                            <Grid xs={12} md={8}>
                                                <Typography variant={'h6'}><Link href={plugin.repo}>{plugin.path}</Link></Typography>
                                            </Grid>
                                            <Grid
                                                md={4}
                                                xs={12}
                                                container
                                                justifyContent="space-between"
                                                flexDirection={{xs: 'column', sm: 'row'}}
                                            >
                                                <Typography variant={'body1'}>下载量：{plugin.downloads}</Typography>
                                                <Typography
                                                    variant={'body1'}>最后更新：{formatTime(plugin.updated)}</Typography>
                                            </Grid>

                                            <Grid
                                                xs={12}
                                                container
                                                justifyContent="space-between"
                                                alignItems="flex-end"
                                                flexDirection={{xs: 'column', sm: 'row'}}
                                            >
                                                <Grid>
                                                    <Stack pl={1}>
                                                        {plugin.modules?.map(module =>
                                                            <Typography>🔌 {module.name}</Typography>)}
                                                    </Stack>
                                                </Grid>
                                                <Grid container columnSpacing={1}>
                                                    {caddyModules.nonStandard.hasOwnProperty(plugin.path)
                                                        ? <Button variant={'outlined'}
                                                                  onClick={() => remove(plugin.path)}>卸载</Button>
                                                        : <Button variant={'outlined'}
                                                                  onClick={() => install(plugin.path)}>安装</Button>}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Paper>
                            </ListItem>
                        ))}
                </List>
            </LabelPaper>
            <Dialog open={dialogState !== 0}>
                {dialogState === 5 && <DialogTitle>警告</DialogTitle>}
                <DialogContent>
                    {dialogState === 3 && <DialogContentText>需要重启 caddy 服务才会生效，是否立即重启</DialogContentText>}
                    {dialogState === 5 && <DialogContentText>{dialogErrorText}</DialogContentText>}
                    {(dialogState === 1 || dialogState === 2 || dialogState === 4) &&
                        <Box sx={{minWidth: 200, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                            <CircularProgress size={50} sx={{m: 1}} color="primary"/>
                            <Typography variant={'body1'}>{dialogState === 1 ? '安装中...' : dialogState === 2 ? '卸载中...': '重启服务中...'}</Typography>
                        </Box>}
                </DialogContent>
                {dialogState === 3 && <DialogActions>
                    <Button onClick={() => setDialogState(0)}>
                        否
                    </Button>
                    <Button onClick={() => restartService()} autoFocus>
                        重启
                    </Button>
                </DialogActions>}
                {dialogState === 5 && <DialogActions>
                    <Button onClick={() => setDialogState(0)} autoFocus>
                        确定
                    </Button>
                </DialogActions>}
            </Dialog>
        </TabPanel>
    )
}