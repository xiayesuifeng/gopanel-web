import Paper from '@mui/material/Paper'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import TextField from '@mui/material/TextField'
import TabPanel from '@mui/lab/TabPanel'
import React, { useEffect, useState } from 'react'
import DNSProviderEditor from '../../../component/DNSProviderEditor'
import Typography from '@mui/material/Typography'
import { CaddyApi } from '../../../api/caddy'
import CircularProgress from '@mui/material/CircularProgress'
import DoneIcon from '@mui/icons-material/Done'
import Fab from '@mui/material/Fab'
import { useSnackbar } from 'notistack'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import IconButton from '@mui/material/IconButton'
import DeleteIcon from '@mui/icons-material/Delete'
import TableContainer from '@mui/material/TableContainer'
import Tooltip from '@mui/material/Tooltip'
import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { NetworkApi } from '../../../api/network'
import { Radio, RadioGroup } from '@mui/material'
import LabelPaper from '../../../component/LabelPaper'

const fabStyle = {
    position: 'fixed',
    bottom: 24,
    right: 24,
    zIndex: (theme) => theme.zIndex.fab
}

export default function DDNSTabPanel (props) {
    const {value} = props

    const [enabled, setEnabled] = useState(false)
    const [loading, setLoading] = useState(false)

    const [dynamicDomains, setDynamicDomains] = useState(false)
    const [checkInterval, setCheckInterval] = useState('')
    const [domains, setDomains] = useState({})
    const [addDomain, setAddDomain] = useState(false)
    const [addDomainName, setAddDomainName] = useState('')
    const [addDomainRecord, setAddDomainRecord] = useState('')

    const [provider, setProvider] = useState('')
    const [providerOption, setProviderOption] = useState({})

    const [simpleHTTPEnable, setSimpleHTTPEnable] = useState(false)
    const [simpleHTTPEndpoints, setSimpleHTTPEndpoints] = useState('')

    const [upnpEnable, setUPNPEnable] = useState(false)

    const [interfaceEnable, setInterfaceEnable] = useState(false)

    const [interfaceDevices, setInterfaceDevices] = useState([])
    const [interfaceName, setInterfaceName] = useState('')

    const [ipv4Enable, setIPv4Enable] = useState(true)
    const [ipv6Enable, setIPv6Enable] = useState(true)

    const {enqueueSnackbar} = useSnackbar()

    const getDynamicDNS = () => {
        CaddyApi.getDynamicDNS()
            .then(data => {
                setEnabled(data.config.enabled)

                setCheckInterval(data.config.config.check_interval ?? '')
                setDynamicDomains(data.config.config.dynamic_domains ?? false)

                if (data.config.config.dns_provider && data.config.config.dns_provider.name) {
                    setProvider(data.config.config.dns_provider.name)
                    setProviderOption(data.config.config.dns_provider)
                }

                if (data.config.config?.ip_sources) {
                    for (let i = 0; i < data.config.config.ip_sources.length; i++) {
                        const source = data.config.config.ip_sources[i]
                        if (source.source === 'simple_http') {
                            setSimpleHTTPEnable(true)

                            let value = ''
                            for (let i = 0; i < source.endpoints.length; i++) {
                                value += source.endpoints[i]
                                if (i < source.endpoints.length - 1)
                                    value += ','
                            }

                            setSimpleHTTPEndpoints(value)
                        } else if (source.source === 'upnp') {
                            setUPNPEnable(true)
                        } else if (source.source === 'interface') {
                            setInterfaceEnable(true)
                            setInterfaceName(source.name)
                        }
                    }
                }

                if (data.config.config.versions?.ipv4 !== undefined) {
                    setIPv4Enable(data.config.config.versions.ipv4)
                }

                if (data.config.config.versions?.ipv6 !== undefined) {
                    setIPv6Enable(data.config.config.versions.ipv6)
                }

                setDomains(data.config.config.domains ?? {})
            })
    }

    useEffect(() => {
        getDynamicDNS()

        NetworkApi.getDevices().then(data => setInterfaceDevices(data.map(device => device.name)))
    }, [])

    const handleDoneFabClick = () => {
        if (!provider) {
            enqueueSnackbar('DNS 提供商不能为空', {variant: 'error'})
            return
        }

        setLoading(true)

        let data = {
            enabled: enabled,
            config: {
                dns_provider: {
                    ...providerOption,
                    name: provider,
                },
                domains: domains,
                dynamic_domains: dynamicDomains,
                versions: {
                    ipv4: ipv4Enable,
                    ipv6: ipv6Enable
                }
            }
        }

        if (checkInterval)
            data.config.check_interval = checkInterval

        let ip_sources = []

        if (interfaceEnable && interfaceName)
            ip_sources.push({
                source: 'interface',
                name: interfaceName
            })

        if (simpleHTTPEnable) {
            let ip_source = {
                source: 'simple_http'
            }

            if (simpleHTTPEndpoints)
                ip_source.endpoints = simpleHTTPEndpoints.split(',')

            ip_sources.push(ip_source)
        }

        if (upnpEnable)
            ip_sources.push({
                source: 'upnp'
            })

        if (ip_sources.length > 0)
            data.config.ip_sources = ip_sources

        CaddyApi.setDynamicDNS(data)
            .then(data => {
                enqueueSnackbar('更新成功', {variant: 'success'})

                getDynamicDNS()
            })
            .catch(err => enqueueSnackbar('更新失败，错误:' + err, {variant: 'error'}))
            .finally(() => setLoading(false))
    }

    const handleDeleteDomain = (domain) => {
        let tmp = {...domains}
        delete tmp[domain]
        setDomains(tmp)
    }

    const handleAddDomain = () => {
        if (addDomainName && addDomainRecord)
            setDomains({
                ...domains,
                [addDomainName]: addDomainRecord.split(',')
            })

        setAddDomainName('')
        setAddDomainRecord('')
        setAddDomain(false)
    }

    const handleEditDomain = (domain, record) => {
        setDomains({
            ...domains,
            [domain]: record.split(',')
        })
    }

    return (
        <TabPanel value={value} sx={{p: 0}}>
            <LabelPaper>
                <FormControlLabel control={<Switch/>} label="启用动态 DNS"
                                  checked={enabled}
                                  onChange={e => setEnabled(e.target.checked)}/>
                <FormControlLabel control={<Switch/>} label="启用动态域名(自动添加应用中添加的域名)"
                                  checked={dynamicDomains}
                                  onChange={e => setDynamicDomains(e.target.checked)}
                                  disabled={!enabled}/>
                <TextField
                    margin={'normal'}
                    label={'检测频率'}
                    helperText={'默认：30m，可以是整数或字符串。整数被解释为纳秒。如果是字符串，则为 Go time.Duration 值，例如 300ms、1.5h 或 2h45m; 有效单位是 ns, us/ µs, ms, s, m, h, 和d.'}
                    value={checkInterval}
                    onChange={e => setCheckInterval(e.target.value)}
                    disabled={!enabled}/>

                <FormControlLabel control={<Switch/>} label="启用 IPv4"
                                  checked={ipv4Enable}
                                  onChange={e => setIPv4Enable(e.target.checked)}
                                  disabled={!enabled}/>

                <FormControlLabel control={<Switch/>} label="启用 IPv6"
                                  checked={ipv6Enable}
                                  onChange={e => setIPv6Enable(e.target.checked)}
                                  disabled={!enabled}/>
            </LabelPaper>
            <LabelPaper>
                <Box sx={{width: '100%', display: 'flex'}}>
                    <Typography sx={{flex: '1 1 100%'}} variant="h6">域名</Typography>
                    <Tooltip title="添加域名" disabled={!enabled}>
                        <IconButton onClick={() => setAddDomain(true)}>
                            <AddIcon/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <TableContainer>
                    <Table sx={{minWidth: 650}}>
                        <TableHead>
                            <TableRow>
                                <TableCell>顶级域名</TableCell>
                                <TableCell>子域名</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {addDomain &&
                                <TableRow
                                >
                                    <TableCell component="th" scope="row">
                                        <TextField
                                            size={'small'}
                                            helperText={'请输入 DNS 提供商中显示的域名名称，如 example.com'}
                                            value={addDomainName}
                                            disabled={!enabled}
                                            onChange={e => setAddDomainName(e.target.value)}/>
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        <TextField
                                            size={'small'}
                                            helperText={'要引用域名本身请输入 "@"，多个用半角逗号分割，如 @,www'}
                                            value={addDomainRecord}
                                            disabled={!enabled}
                                            onChange={e => setAddDomainRecord(e.target.value)}/>
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <IconButton onClick={handleAddDomain} disabled={!enabled}>
                                            <DoneIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>}
                            {Object.keys(domains).map((domain) => (
                                <TableRow
                                    key={domain}
                                >
                                    <TableCell component="th" scope="row">
                                        {domain}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        <TextField size={'small'}
                                                   value={domains[domain]}
                                                   disabled={!enabled}
                                                   onChange={e => handleEditDomain(domain, e.target.value)}/>
                                    </TableCell>
                                    <TableCell align={'right'}>
                                        <IconButton onClick={() => handleDeleteDomain(domain)} disabled={!enabled}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </LabelPaper>
            <LabelPaper label={'提供商'} >
                <DNSProviderEditor provider={provider} onProviderChange={setProvider} providerOption={providerOption}
                                   onProviderOptionChange={setProviderOption} disabled={!enabled}/>
            </LabelPaper>
            <LabelPaper label={'IP 获取方式'} >
                <FormControlLabel control={<Switch/>} label="通过网络接口获取"
                                  disabled={!enabled}
                                  checked={interfaceEnable}
                                  onChange={e => setInterfaceEnable(e.target.checked)}/>
                {interfaceEnable && interfaceDevices.length > 0 &&
                    <RadioGroup value={interfaceName === '' ? interfaceDevices[0] : interfaceName}
                                onChange={(event, value) => setInterfaceName(value)}
                                row>
                        {interfaceDevices.map(d => (<FormControlLabel value={d} control={<Radio/>} label={d}/>))}
                    </RadioGroup>}
                <FormControlLabel control={<Switch/>} label="通过 HTTP 接口获取(无任何可用获取方式时默认开启)"
                                  disabled={!enabled}
                                  checked={simpleHTTPEnable}
                                  onChange={e => setSimpleHTTPEnable(e.target.checked)}/>
                <TextField label={'接口地址'}
                           disabled={!enabled}
                           value={simpleHTTPEndpoints}
                           onChange={e => setSimpleHTTPEndpoints(e.target.value)}
                           helperText={<Typography component={'span'}>支持多个接口，使用半角逗号分割，如
                               https://api64.ipify.org,https://myip.addr.space，留空默认:<br/>
                               https://api64.ipify.org<br/>
                               https://myip.addr.space<br/>
                               https://ifconfig.me<br/>
                               https://icanhazip.com<br/>
                               https://ident.me<br/>
                               https://bot.whatismyipaddress.com<br/>
                               https://ipecho.net/plain</Typography>}/>
                <FormControlLabel control={<Switch/>} label="通过 UPNP 获取"
                                  disabled={!enabled}
                                  checked={upnpEnable}
                                  onChange={e => setUPNPEnable(e.target.checked)}/>
            </LabelPaper>
            <Fab color={'primary'} sx={fabStyle} onClick={handleDoneFabClick} disabled={loading}>
                {loading ? <CircularProgress size={31}/> : <DoneIcon/>}
            </Fab>
        </TabPanel>
    )
}
