import { Dialog } from '@mui/material'
import React, { useEffect, useState } from 'react'
import DialogTitle from '@mui/material/DialogTitle'
import DialogActions from '@mui/material/DialogActions'
import Button from '@mui/material/Button'
import DialogContent from '@mui/material/DialogContent'
import Cookies from 'js-cookie'
import TextField from '@mui/material/TextField'
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery'
import { useSnackbar } from 'notistack'
import Chip from '@mui/material/Chip'

export default function AppLogDialog(props) {
    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'))

    const {enqueueSnackbar} = useSnackbar()

    const {name} = props

    const [log,setLog] = useState('')
    const [status,setStatus] = useState('')
    const [ws,setWs] = useState(null)

    useEffect(() => {
        if (props.open && name) {
            const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:'
            setWs(new WebSocket(`${protocol}//${window.location.host}/api/backend/${name}/ws`, Cookies.get('token')))
        }
    },[props.name,props.open])

    useEffect(() => {
        if (ws) {
            ws.onmessage = function (e) {
                const data = JSON.parse(e.data)
                setLog(data.log)
                setStatus(data.status)
            }
            ws.onerror = () => enqueueSnackbar('连接错误', {variant: 'error'})
        }
    },[ws])

    const handleClose = () => {
        ws.close()

        props.onClose()
    }

    const getStatusChip = () => {
        switch (status) {
            case 'Run':
                return (<Chip color={'primary'} label={'运行中'}/>)
            case 'Stop':
                return (<Chip color={'secondary'} label={'已停止'}/>)
            default:
                return (<Chip label={'Unknown'}/>)
        }
    }

    return (
        <Dialog maxWidth={'lg'} scroll={'paper'} fullScreen={fullScreen} fullWidth open={props.open} onClose={handleClose}>
            <DialogTitle>应用运行日志 {getStatusChip()}</DialogTitle>
            <DialogContent dividers>
                <TextField variant={'filled'} fullWidth disabled value={log} multiline/>
            </DialogContent>
            <DialogActions onClick={handleClose}>
                <Button color="primary">关闭</Button>
            </DialogActions>
        </Dialog>
    )
}