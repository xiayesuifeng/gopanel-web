import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import TableContainer from '@mui/material/TableContainer'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TableBody from '@mui/material/TableBody'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import React, {useState} from 'react'
import Button from '@mui/material/Button'
import CircularProgress from '@mui/material/CircularProgress'
import {useSnackbar} from 'notistack'
import DNSProviderEditor from "./DNSProviderEditor";

export default function DnsChallengesTemplateEditor(props) {
    const {value, onChange} = props

    const [dialogEdit, setDialogEdit] = useState(false)
    const [dialogOpen, setDialogOpen] = useState(false)
    const [loading, setLoading] = useState(false)

    const [domain, setDomain] = useState('')
    const [provider, setProvider] = useState('')
    const [providerOption, setProviderOption] = useState({})

    const {enqueueSnackbar} = useSnackbar()

    const handleAddTemplate = () => {
        setLoading(true)

        if (!domain) {
            enqueueSnackbar('域名不能为空', {variant: 'error'})
            setLoading(false)

            return
        }

        if (!provider) {
            enqueueSnackbar('提供商不能为空', {variant: 'error'})
            setLoading(false)

            return
        }

        if (!dialogEdit && Object.keys(value).indexOf(domain) !== -1) {
            enqueueSnackbar('该域名的模板已存在', {variant: 'error'})
            setLoading(false)

            return
        }

        let dnsChallenge = {}
        dnsChallenge[domain] = {provider: {name: provider, ...providerOption}}

        onChange({...value, ...dnsChallenge})

        setLoading(false)
        setDialogOpen(false)
    }

    const handleTemplateDialogClose = () => {
        if (loading)
            setLoading(false)

        setDialogOpen(false)
    }

    const handleTemplateDialogOpen = () => {
        if (dialogEdit) {
            setDomain('')
            setProvider('')
            setProviderOption({})
        }

        setDialogEdit(false)
        setDialogOpen(true)
    }

    const handleDeleteTemplate = (domain) => {
        let newValue = Object.assign({}, value)
        delete newValue[domain]

        onChange(newValue)
    }

    const handleEditTemplate = (domain) => {
        setDomain(domain)
        if (value[domain].provider) {
            setProvider(value[domain].provider?.name)
            setProviderOption(value[domain].provider)
        }

        setDialogEdit(true)
        setDialogOpen(true)
    }

    const templateEditorDialog = (
        <Dialog onClose={handleTemplateDialogClose} open={dialogOpen}>
            <DialogTitle>{dialogEdit ? '修改 DNS Challenge 模板' : '添加 DNS Challenge 模板'}</DialogTitle>
            <DialogContent sx={{display: 'flex', flexDirection: 'column'}}>
                <TextField label="域名"
                           value={domain}
                           helperText="DNS 提供商中显示的域名，如 example.com 而不是 www.example.com"
                           margin={'normal'}
                           onChange={e => setDomain(e.target.value)}/>
                <DNSProviderEditor provider={provider} onProviderChange={setProvider} providerOption={providerOption}
                                   onProviderOptionChange={setProviderOption}/>
            </DialogContent>
            <DialogActions>
                {loading ? <CircularProgress size={31}/> : <Button autoFocus onClick={handleAddTemplate}>完成</Button>}
            </DialogActions>
        </Dialog>
    )

    return (
        <>
            <Toolbar sx={{width: '100%'}}>
                <Typography sx={{flex: '1 1 100%'}} variant="h6">DNS Challenge 模板</Typography>
                <Tooltip title="添加模板">
                    <IconButton onClick={handleTemplateDialogOpen}>
                        <AddIcon/>
                    </IconButton>
                </Tooltip>
            </Toolbar>
            <TableContainer>
                <Table sx={{minWidth: 650}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>域名</TableCell>
                            <TableCell>提供商</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.keys(value).map((domain) => (
                            <TableRow
                                key={domain}
                            >
                                <TableCell component="th" scope="row">
                                    {domain}
                                </TableCell>
                                <TableCell>{value[domain].provider?.name}</TableCell>
                                <TableCell align={'right'}>
                                    <IconButton onClick={() => handleEditTemplate(domain)}>
                                        <EditIcon/>
                                    </IconButton>
                                    <IconButton onClick={() => handleDeleteTemplate(domain)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            {templateEditorDialog}
        </>
    )
}
