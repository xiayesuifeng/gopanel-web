import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import React, { useEffect, useState } from 'react'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import Typography from '@mui/material/Typography'
import ListenHostEditor from './ListenHostEditor'

export function CaddyEasyEditor (props) {
    let {config} = props
    const {onConfigChange} = props

    const [rootPath, setRootPath] = useState('')

    const [fileServerEnabled, setFileServerEnabled] = useState(false)
    const [reverseProxyEnabled, setReverseProxyEnabled] = useState(false)
    const [routerRewriteEnabled, setRouterRewriteEnabled] = useState(false)

    const [fileServerMatchPath, setFileServerMatchPath] = useState('')

    const [reverseProxyUpstreams, setReverseProxyUpstreams] = useState('')
    const [reverseProxyMatchPath, setReverseProxyMatchPath] = useState('')

    const handleConfigChange = config => {
        onConfigChange(config)
    }

    useEffect(() => {
        setFileServerEnabled(false)
        setReverseProxyEnabled(false)
        setRouterRewriteEnabled(false)

        if (config.routes !== undefined && config.routes.length > 0) {
            for (const route of config.routes) {
                if (!route.handle)
                    continue

                for (let j = 0; j < route.handle.length; j++) {
                    if (route.handle[j].handler === 'subroute') {
                        for (let k = 0; k < route.handle[j].routes.length; k++) {
                            route.handle[j].routes[k].handle.map(handle => {
                                switch (handle.handler) {
                                    case 'file_server':
                                        setFileServerEnabled(true)

                                        if (route.handle[j].routes[k].match !== undefined) {
                                            route.handle[j].routes[k].match.map(match => setFileServerMatchPath(array2str(match.path)))
                                        }

                                        break
                                    case 'reverse_proxy':
                                        setReverseProxyEnabled(true)

                                        setReverseProxyUpstreams(array2str(handle.upstreams.map(upstream => upstream.dial)))

                                        if (route.handle[j].routes[k].match !== undefined) {
                                            route.handle[j].routes[k].match.map(match => setReverseProxyMatchPath(array2str(match.path)))
                                        }

                                        break
                                    case 'rewrite':
                                        setRouterRewriteEnabled(true)
                                        break
                                }

                                return handle
                            })
                        }
                    } else if (route.handle[j].handler === 'vars') {
                        setRootPath(route.handle[j].root)
                    }
                }
            }
        }
    }, [config])

    const array2str = array => {
        let value = ''

        if (array === undefined) {
            return value
        }

        for (let j = 0; j < array.length; j++) {
            value += array[j]
            if (j < array.length - 1)
                value += ','
        }

        return value
    }

    const str2array = str => {
        return str.split(',')
    }

    const handleFileServerEnabledChange = (event) => {
        if (event.target.checked) {
            let tmpConfig = {...config}

            let result = checkCaddyJsonTreeExist(tmpConfig)

            tmpConfig.routes[result.routeHandleIdx]
                .handle[result.subRouteIdx]
                .routes.push({handle: [{handler: 'file_server'}]})

            handleConfigChange(tmpConfig)
        } else {
            let tmpConfig = {...config}

            let result = checkCaddyJsonTreeExist(tmpConfig)
            let subroute = tmpConfig.routes[result.routeHandleIdx]
                .handle[result.subRouteIdx]

            for (let i = 0; i < subroute.routes.length; i++) {
                for (let j = 0; j < subroute.routes[i].handle.length; j++) {
                    if (subroute.routes[i].handle[j].handler === 'file_server') {
                        subroute.routes.splice(i, 1)
                        handleConfigChange(tmpConfig)
                        return
                    }
                }
            }

        }
    }

    const handleReverseProxyEnabledChange = (event) => {
        if (event.target.checked) {
            let tmpConfig = {...config}

            let result = checkCaddyJsonTreeExist(tmpConfig)
            tmpConfig.routes[result.routeHandleIdx]
                .handle[result.subRouteIdx]
                .routes.push({
                handle: [{
                    handler: 'reverse_proxy',
                    upstreams: []
                }]
            })

            handleConfigChange(tmpConfig)
        } else {
            let tmpConfig = {...config}

            let result = checkCaddyJsonTreeExist(tmpConfig)
            let subroute = tmpConfig.routes[result.routeHandleIdx]
                .handle[result.subRouteIdx]

            for (let i = 0; i < subroute.routes.length; i++) {
                for (let j = 0; j < subroute.routes[i].handle.length; j++) {
                    if (subroute.routes[i].handle[j].handler === 'reverse_proxy') {
                        subroute.routes.splice(i, 1)
                        handleConfigChange(tmpConfig)
                        return
                    }
                }
            }

        }
    }

    const handleRootPathChange = (event) => {
        let tmpConfig = {...config}

        let result = checkCaddyJsonTreeExist(tmpConfig)

        tmpConfig.routes[result.routeHandleIdx]
            .handle[result.varsIdx].root = event.target.value

        handleConfigChange(tmpConfig)
    }

    const handleDomainChange = (event) => {
        handleConfigChange({...config, domain: str2array(event.target.value)})
    }

    const handlePathMatchChange = (value, handler) => {
        let tmpConfig = {...config}

        let result = checkCaddyJsonTreeExist(tmpConfig)

        let matchRoute = undefined
        tmpConfig.routes[result.routeHandleIdx]
            .handle[result.subRouteIdx]
            .routes.map(route => {
            for (let i = 0; i < route.handle.length; i++) {
                if (route.handle[i].handler === handler) {
                    matchRoute = route
                }
            }

            return route
        })

        if (matchRoute !== undefined) {
            if (matchRoute.match === undefined) {
                matchRoute.match = []
            }

            let paths = str2array(value)

            let findPathMatch = false
            for (let i = 0; i < matchRoute.match.length; i++) {
                if (matchRoute.match[i].path !== undefined) {
                    if (value)
                        matchRoute.match[i].path = paths
                    else
                        matchRoute.match.splice(i, 1)
                    findPathMatch = true
                    break
                }
            }

            if (!findPathMatch && value) {
                matchRoute.match.push({
                    path: paths
                })
            }
        }

        handleConfigChange(tmpConfig)
    }

    const handleReverseProxyUpstreamsChange = (event) => {
        let tmpConfig = {...config}

        let result = checkCaddyJsonTreeExist(tmpConfig)

        let routes = tmpConfig.routes[result.routeHandleIdx]
            .handle[result.subRouteIdx]
            .routes

        for (let i = 0; i < routes.length; i++) {
            for (let j = 0; j < routes[i].handle.length; j++) {
                if (routes[i].handle[j].handler === 'reverse_proxy') {
                    let upstreams = event.target.value.split(',').map(value => {
                        return {
                            dial: value
                        }
                    })

                    routes[i].handle[j].upstreams = upstreams

                    handleConfigChange(tmpConfig)

                    return
                }
            }
        }
    }

    const handleListenCheckedChange = e => {
        if (e.target.checked) {
            handleConfigChange({listenPort: 0, ...config})
        } else {
            delete config.listenPort
            handleConfigChange(config)
        }
    }

    const handleListenPortChange = port => {
        if (port) {
            handleConfigChange({...config,listenPort: port})
        } else {
            delete config.listenPort
            handleConfigChange(config)
        }
    }

    const checkCaddyJsonTreeExist = config => {
        let routeHandleIdx = -1

        let varsIdx = -1
        let subRouteIdx = -1

        if (config.routes === undefined || config.routes.length === 0) {
            config.routes = [{handle: [], match: []}]
        }

        for (let i = 0; i < config.routes.length; i++) {
            routeHandleIdx = i

            // find vars and subroute handler exist
            for (let k = 0; k < config.routes[i].handle.length; k++) {
                if (config.routes[i].handle[k].handler === 'vars') {
                    varsIdx = k
                } else if (config.routes[i].handle[k].handler === 'subroute') {
                    subRouteIdx = k
                }
            }

            if (varsIdx === -1) {
                config.routes[i].handle.push({handler: 'vars'})
                varsIdx = config.routes[i].handle.length - 1
            }

            if (subRouteIdx === -1) {
                config.routes[i].handle.push({handler: 'subroute', routes: []})
                subRouteIdx = config.routes[i].handle.length - 1
            }

            if (varsIdx !== -1 && subRouteIdx !== -1)
                break
        }

        return {
            routeHandleIdx,
            subRouteIdx,
            varsIdx,
        }
    }

    const fileServerEditor = (
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
            >
                <Typography>文件服务</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Box sx={{display: 'flex', flexDirection: 'column'}}>
                    <FormControlLabel
                        control={
                            <Switch
                                color="primary"
                                checked={fileServerEnabled}
                                onChange={e => handleFileServerEnabledChange(e)}
                            />
                        }
                        label="启用"
                    />
                    <TextField
                        sx={{m: 1}}
                        label={'匹配路由'}
                        disabled={!fileServerEnabled}
                        value={fileServerMatchPath}
                        onChange={e => handlePathMatchChange(e.target.value, 'file_server')}
                    />
                </Box>
            </AccordionDetails>
        </Accordion>
    )

    const reverseProxyEditor = (
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
            >
                <Typography>反向代理</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Box sx={{display: 'flex', flexDirection: 'column'}}>
                    <FormControlLabel
                        control={
                            <Switch
                                color="primary"
                                checked={reverseProxyEnabled}
                                onChange={e => handleReverseProxyEnabledChange(e)}
                            />
                        }
                        label="启用"
                    />
                    <TextField
                        sx={{m: 1}}
                        label={'上游'}
                        disabled={!reverseProxyEnabled}
                        value={reverseProxyUpstreams}
                        onChange={e => handleReverseProxyUpstreamsChange(e)}
                    />
                    <TextField
                        sx={{m: 1}}
                        label={'匹配路由'}
                        disabled={!reverseProxyEnabled}
                        value={reverseProxyMatchPath}
                        onChange={e => handlePathMatchChange(e.target.value, 'reverse_proxy')}
                    />
                </Box>
            </AccordionDetails>
        </Accordion>
    )

    const routerRewriteEditor = (
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
            >
                <Typography>路由改写</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Box sx={{display: 'flex', flexDirection: 'column'}}>
                    <FormControlLabel
                        control={
                            <Switch
                                color="primary"
                                checked={routerRewriteEnabled}
                                disabled={true}
                            />
                        }
                        label="启用"
                    />
                </Box>
            </AccordionDetails>
        </Accordion>
    )

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
        }}>
            <ListenHostEditor
                disableSSL={config.disableSSL}
                domain={config.domain}
                port={config.listenPort}
                onDisableSSLChange={val => handleConfigChange({...config, disableSSL: val})}
                onDomainChange={domain => handleConfigChange({...config, domain: [domain]})}
                onPortChange={port => handleListenPortChange(port)}
            />
            <TextField
                sx={{mt: 1, mb: 1}}
                label={'根目录路径(用于指定文件服务或者 php_fastcgi 服务默认根目录等)'}
                value={rootPath}
                onChange={e => handleRootPathChange(e)}
            />
            {fileServerEditor}
            {reverseProxyEditor}
            {routerRewriteEditor}
        </Box>
    )
}
