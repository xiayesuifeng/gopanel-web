import React, { useState } from 'react'
import TextField from '@mui/material/TextField'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import TreeItem from '@mui/lab/TreeItem'
import SubrouteItem from './handle/SubrouteItem'
import ReverseProxyItem from './handle/ReverseProxyItem'
import TreeItemLabel from '../../TreeItemLabel'
import VarsItem from './handle/VarsItem'
import TemplatesItem from './handle/TemplatesItem'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import RewriteItem from './handle/RewriteItem'
import TypeDialog from './TypeDialog'
import MatchItem from './MatchItem'
import StaticResponseItem from './handle/StaticResponseItem'
import AcmeServerItem from './handle/AcmeServerItem'
import ErrorItem from './handle/ErrorItem'
import EncodeItem from './handle/EncodeItem'
import RequestBodyItem from './handle/RequestBodyItem'
import FileServerItem from './handle/FileServerItem'
import AuthenticationItem from './handle/AuthenticationItem'
import HeadersItem from './handle/HeadersItem'

const handlerType = [
    'acme_server',
    'authentication',
    'encode',
    'error',
    'file_server',
    'headers',
    'request_body',
    'reverse_proxy',
    'rewrite',
    'static_response',
    'subroute',
    'templates',
    'vars'
]

export default function RouteItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [handleTypeOpen, setHandleTypeOpen] = useState(false)

    const handleMatchChange = (index, newConfig) => {
        config.match[index] = newConfig
        onConfigChange(config)
    }

    const handleHandleChange = (index, newConfig) => {
        config.handle[index] = newConfig
        onConfigChange(config)
    }

    const getHandleItem = (index, item) => {
        switch (item.handler) {
            case 'acme_server':
                return (<AcmeServerItem key={`${props.id}-acme_server-${index}`}
                                        id={`${props.id}-acme_server-${index}`} config={item}
                                        onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'authentication':
                return (<AuthenticationItem key={`${props.id}-authentication-${index}`}
                                            id={`${props.id}-authentication-${index}`} config={item}
                                            onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'encode':
                return (<EncodeItem key={`${props.id}-encode-${index}`}
                                    id={`${props.id}-encode-${index}`} config={item}
                                    onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'error':
                return (<ErrorItem key={`${props.id}-error-${index}`}
                                   id={`${props.id}-error-${index}`} config={item}
                                   onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'headers':
                return (<HeadersItem key={`${props.id}-headers-${index}`}
                                     id={`${props.id}-headers-${index}`} config={item}
                                     onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'file_server':
                return (<FileServerItem key={`${props.id}-file_server-${index}`}
                                        id={`${props.id}-file_server-${index}`} config={item}
                                        onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'request_body':
                return (<RequestBodyItem key={`${props.id}-request_body-${index}`}
                                         id={`${props.id}-request_body-${index}`} config={item}
                                         onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'reverse_proxy':
                return (<ReverseProxyItem key={`${props.id}-reverse_proxy-${index}`}
                                          id={`${props.id}-reverse_proxy-${index}`} config={item}
                                          onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'subroute':
                return (<SubrouteItem key={`${props.id}-subroute-${index}`} id={`${props.id}-subroute-${index}`}
                                      config={item}
                                      onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'vars':
                return (<VarsItem key={`${props.id}-vars-${index}`} id={`${props.id}-vars-${index}`} config={item}
                                  onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'templates':
                return (<TemplatesItem key={`${props.id}-templates-${index}`} id={`${props.id}-templates-${index}`}
                                       config={item}
                                       onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'rewrite':
                return (
                    <RewriteItem key={`${props.id}-rewrite-${index}`} id={`${props.id}-rewrite-${index}`} config={item}
                                 onConfigChange={c => handleHandleChange(index, c)}/>)
            case 'static_response':
                return (
                    <StaticResponseItem key={`${props.id}-static_response-${index}`}
                                        id={`${props.id}-static_response-${index}`} config={item}
                                        onConfigChange={c => handleHandleChange(index, c)}/>)
            default:
                break
        }
    }

    const handleAddMatchClick = () => {
        if (config.match) {
            config.match.push({})
            onConfigChange(config)
        } else {
            onConfigChange({...config, match: [{}]})
        }
    }

    const handleTypeDialogClose = type => () => {
        if (type) {
            if (config.handle) {
                config.handle.push({handler: type})
                onConfigChange(config)
            } else {
                onConfigChange({...config, handle: [{handler: type}]})
            }
        }

        setHandleTypeOpen(false)
    }

    return (<TreeItem
        nodeId={props.id}
        label={<TreeItemLabel label={props.index}/>}
    >
        <TypeDialog
            open={handleTypeOpen}
            types={handlerType}
            onClose={handleTypeDialogClose}/>
        <TextField
            label={'group'}
            variant={'filled'}
            fullWidth
            value={config.group}
        />
        <TreeItem
            nodeId={`${props.id}-match`}
            label={<TreeItemLabel label={'match'}
                                  action={<IconButton color={'primary'} size={'small'}
                                                      onClick={handleAddMatchClick}><AddIcon/></IconButton>}/>}>
            {config.match && config.match.map((item, index) => (
                <MatchItem key={`${props.id}-match-${index}`} id={`${props.id}-match-${index}`} index={index}
                           config={item}
                           onConfigChange={c => handleMatchChange(index, c)}/>
            ))}
        </TreeItem>
        <TreeItem
            nodeId={`${props.id}-handle`}
            label={<TreeItemLabel label={'handle'}
                                  action={<IconButton color={'primary'} size={'small'}
                                                      onClick={() => setHandleTypeOpen(true)}><AddIcon/></IconButton>}/>}>
            {config.handle && config.handle.map((item, index) => (
                getHandleItem(index, item)
            ))}
        </TreeItem>
        <FormControlLabel
            control={
                <Switch
                    color="primary"
                    checked={config.terminal}
                />
            }
            label="terminal"
        />
    </TreeItem>)
}