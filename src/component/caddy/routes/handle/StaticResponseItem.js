import React from 'react'
import TextField from '@mui/material/TextField'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'

export default function StaticResponseItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleHeadersChange = (value, newKey) => {
        if (onConfigChange) {
            if (typeof value === 'object') {
                config.headers = {...config.headers, ...value}
                onConfigChange(config)
            } else if (typeof value === 'string' && newKey !== '') {
                const tmp = config.headers[value]
                delete config.headers[value]
                config.headers = {...config.headers, [newKey]: tmp}
                onConfigChange(config)
            }
        }
    }

    const handleHeadersDelete = key => () => {
        if (onConfigChange) {
            delete config.headers[key]
            onConfigChange(config)
        }
    }

    const handleConfigChange = e => {
        if (e.target.name === 'close')
            onConfigChange({...config, [e.target.name]: e.target.checked})
        else
            onConfigChange({...config, [e.target.name]: e.target.value})
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'static_response'}/>}
            nodeId={props.id}>
            <TextField
                name={'status_code'}
                label={'status_code'}
                fullWidth
                variant={'filled'}
                value={config.status_code ? config.status_code : ''}
                onChange={handleConfigChange}/>
            <TreeItem
                label={<TreeItemLabel label={'headers'}/>}
                nodeId={`${props.id}-headers`}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Value</TableCell>
                            <TableCell>
                                <IconButton color={'primary'} size="small"
                                            onClick={() => handleHeadersChange({newVariable: ''})}>
                                    <AddIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {config.headers && Object.keys(config.headers).map(key => (
                            <TableRow key={key}>
                                <TableCell>
                                    <TextField
                                        value={key}
                                        onChange={e => handleHeadersChange(key, e.target.value)}
                                        fullWidth
                                    />
                                </TableCell>
                                <TableCell>
                                    <TextField
                                        value={config.headers[key]}
                                        onChange={e => handleHeadersChange({[key]: e.target.value})}
                                        fullWidth
                                    />
                                </TableCell>
                                <TableCell>
                                    <IconButton color={'primary'} size="small" onClick={handleHeadersDelete(key)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TreeItem>
            <TextField
                name={'body'}
                label={'body'}
                fullWidth
                variant={'filled'}
                value={config.body ? config.body : ''}
                onChange={handleConfigChange}/>
            <FormControlLabel
                control={
                    <Switch
                        color="primary"
                        checked={config.close ? config.close : false}
                        onChange={handleConfigChange}
                    />
                }
                name='close'
                label='close'
            />
        </TreeItem>
    )
}