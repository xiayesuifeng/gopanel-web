import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function AcmeServerItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleConfigChange = e => onConfigChange({...config, [e.target.name]: e.target.value})

    return (
        <TreeItem
            label={<TreeItemLabel label={'acme_server'}/>}
            nodeId={props.id}>
            <TextField
                name={'ca'}
                label={'ca'}
                fullWidth
                variant={'filled'}
                value={config.ca ? config.ca : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'host'}
                label={'host'}
                fullWidth
                variant={'filled'}
                value={config.host ? config.host : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'path_prefix'}
                label={'path_prefix'}
                fullWidth
                variant={'filled'}
                value={config.path_prefix ? config.path_prefix : ''}
                onChange={handleConfigChange}/>
        </TreeItem>
    )
}