import React from 'react'
import TextField from '@mui/material/TextField'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function RewriteItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (onConfigChange) {
            onConfigChange({...config, [e.target.name]: e.target.value})
        }
    }

    const handleAddUSS = () => {
        if (onConfigChange) {
            if (config.uri_substring) {
                config.uri_substring.push({})
                onConfigChange(config)
            } else {
                onConfigChange({...config,uri_substring: [{}]})
            }
        }
    }

    const handleUSSChange = (index,value) => {
        if (onConfigChange) {
            config.uri_substring[index] = {...config.uri_substring[index],...value}
            onConfigChange(config)
        }
    }

    const handleUSSDelete = index => () => {
        if (onConfigChange) {
            config.uri_substring.splice(index,1)
            onConfigChange(config)
        }
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'rewrite'}/>}
            nodeId={props.id}>
            <TextField
                name={'method'}
                label={'method'}
                fullWidth
                variant={'filled'}
                value={config.method}
                onChange={handleChange}/>
            <TextField
                name={'uri'}
                label={'uri'}
                fullWidth
                variant={'filled'}
                value={config.uri}
                onChange={handleChange}/>
            <TextField
                name={'strip_path_prefix'}
                label={'strip_path_prefix'}
                fullWidth
                variant={'filled'}
                value={config.strip_path_prefix}
                onChange={handleChange}/>
            <TextField
                name={'strip_path_suffix'}
                label={'strip_path_suffix'}
                fullWidth
                variant={'filled'}
                value={config.strip_path_suffix}
                onChange={handleChange}/>
            <TreeItem
                label={<TreeItemLabel label={'uri_substring'}/>}
                nodeId={`${props.id}-uri_substring`}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>find</TableCell>
                            <TableCell>replace</TableCell>
                            <TableCell>limit</TableCell>
                            <TableCell>
                                <IconButton color={'primary'} size="small"
                                            onClick={handleAddUSS}>
                                    <AddIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {config.uri_substring && config.uri_substring.map((uss, index) => (
                            <TableRow>
                                <TableCell>
                                    <TextField
                                        value={uss.find}
                                        onChange={e => handleUSSChange(index, {find :e.target.value})}
                                        fullWidth
                                    />
                                </TableCell>
                                <TableCell>
                                    <TextField
                                        value={uss.replace}
                                        onChange={e => handleUSSChange(index, {replace :e.target.value})}
                                        fullWidth
                                    />
                                </TableCell>
                                <TableCell>
                                    <TextField
                                        value={uss.limit}
                                        onChange={e => handleUSSChange(index, {limit :e.target.value})}
                                        fullWidth
                                    />
                                </TableCell>
                                <TableCell>
                                    <IconButton color={'primary'} size="small" onClick={handleUSSDelete(index)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TreeItem>
        </TreeItem>
    )
}