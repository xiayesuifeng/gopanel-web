import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function RequestBodyItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleConfigChange = e => onConfigChange({...config, [e.target.name]: parseInt(e.target.value)})

    return (
        <TreeItem
            label={<TreeItemLabel label={'request_body'}/>}
            nodeId={props.id}>
            <TextField
                name={'max_size'}
                label={'max_size'}
                fullWidth
                variant={'filled'}
                type="number"
                value={config.max_size ? config.max_size : '0'}
                onChange={handleConfigChange}/>
        </TreeItem>
    )
}