import React, { useEffect, useState } from 'react'
import TextField from '@mui/material/TextField'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function VarsItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [vars, setVars] = useState({})

    useEffect(() => {
        if (config) {
            setVars(JSON.parse(JSON.stringify(config, function (key, value) {
                if (key === 'handler') {
                    return undefined
                } else {
                    return value
                }
            })))
        }
    }, [config])

    const handleChange = (value, newKey) => {
        if (onConfigChange) {
            if (typeof value === 'object') {
                onConfigChange({handler: 'vars', ...vars, ...value})
            } else if (typeof value === 'string' && newKey !== '') {
                const tmp = vars[value]
                delete vars[value]
                onConfigChange({handler: 'vars', ...vars, [newKey]: tmp})
            }
        }
    }

    const handleDelete = key => () => {
        if (onConfigChange) {
            delete vars[key]
            onConfigChange({handler: 'vars', ...vars})
        }
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'vars'}/>}
            nodeId={props.id}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Value</TableCell>
                        <TableCell>
                            <IconButton color={'primary'} size="small" onClick={() => handleChange({newVariable: ''})}>
                                <AddIcon/>
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {Object.keys(vars).map(key => (
                        <TableRow key={key}>
                            <TableCell>
                                <TextField
                                    value={key}
                                    onChange={e => handleChange(key, e.target.value)}
                                    fullWidth
                                />
                            </TableCell>
                            <TableCell>
                                <TextField
                                    value={vars[key]}
                                    onChange={e => handleChange({[key]: e.target.value})}
                                    fullWidth
                                />
                            </TableCell>
                            <TableCell>
                                <IconButton color={'primary'} size="small" onClick={handleDelete(key)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TreeItem>
    )
}