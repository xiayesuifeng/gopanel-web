import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import Box from '@mui/material/Box'

export default function FileServerItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleConfigChange = e => {
        if (e.target.name === 'canonical_uris' || e.target.name === 'pass_thru')
            onConfigChange({...config, [e.target.name]: e.target.checked})
        else if (e.target.name === 'template_file')
            onConfigChange({...config, browse: {template_file: e.target.value}})
        else if (e.target.name === 'hide' || e.target.name === 'index_names')
            onConfigChange({...config, [e.target.name]: e.target.value.split(',')})
        else
            onConfigChange({...config, [e.target.name]: e.target.value})
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'file_server'}/>}
            nodeId={props.id}>
            <TextField
                name={'root'}
                label={'root'}
                fullWidth
                variant={'filled'}
                value={config.root ? config.root : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'hide'}
                label={'hide(多个用 \',\' 分隔)'}
                fullWidth
                variant={'filled'}
                value={config.hide ? config.hide : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'index_names'}
                label={'index_names(多个用 \',\' 分隔)'}
                fullWidth
                variant={'filled'}
                value={config.index_names ? config.index_names : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'template_file'}
                label={'browse/template_file'}
                fullWidth
                variant={'filled'}
                value={config.browse ? config.browse.template_file ? config.browse.template_file : '' : ''}
                onChange={handleConfigChange}/>
            <Box>
                <FormControlLabel
                    control={
                        <Switch
                            color="primary"
                            checked={config.canonical_uris ? config.canonical_uris : false}
                            onChange={handleConfigChange}
                        />
                    }
                    name='canonical_uris'
                    label='canonical_uris'
                />
            </Box>
            <Box>
                <FormControlLabel
                    control={
                        <Switch
                            color="primary"
                            checked={config.pass_thru ? config.pass_thru : false}
                            onChange={handleConfigChange}
                        />
                    }
                    name='pass_thru'
                    label='pass_thru'
                />
            </Box>
        </TreeItem>
    )
}