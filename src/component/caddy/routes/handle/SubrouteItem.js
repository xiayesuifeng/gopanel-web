import React from 'react'
import RouteItem from '../RouteItem'
import TreeItem from '@mui/lab/TreeItem'
import TreeItemLabel from '../../../TreeItemLabel'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'

export default function SubrouteItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = (index, newConfig) => {
        config.routes[index] = newConfig
        onConfigChange(config)
    }

    const handleAddButtonClick = () => {
        if (config.routes) {
            config.routes.push({})
            onConfigChange(config)
        } else {
            onConfigChange({...config,routes: [{}]})
        }
    }

    return (<div>
        <TreeItem
            label={
                <TreeItemLabel
                    label={'subroute'}
                    action={<IconButton color={'primary'} size={'small'}
                                        onClick={handleAddButtonClick}><AddIcon/></IconButton>}/>}
            nodeId={props.id}>
            {config.routes && config.routes.map((route, index) => (
                <RouteItem key={`${props.id}-route-${index}`} id={`${props.id}-route-${index}`} index={index}
                           config={route}
                           onConfigChange={c => handleChange(index, c)}/>
            ))}
        </TreeItem>
    </div>)
}