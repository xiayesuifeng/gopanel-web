import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function ErrorItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleConfigChange = e => onConfigChange({...config, [e.target.name]: e.target.value})

    return (
        <TreeItem
            label={<TreeItemLabel label={'error'}/>}
            nodeId={props.id}>
            <TextField
                name={'error'}
                label={'error'}
                fullWidth
                variant={'filled'}
                value={config.error ? config.error : ''}
                onChange={handleConfigChange}/>
            <TextField
                name={'status_code'}
                label={'status_code'}
                fullWidth
                variant={'filled'}
                value={config.status_code ? config.status_code : ''}
                onChange={handleConfigChange}/>
        </TreeItem>
    )
}