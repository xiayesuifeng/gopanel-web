import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function TemplatesItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (e.target.name === 'mime_types' || e.target.name === 'delimiters') {
            if (e.target.value) {
                const tmp = e.target.value.split(',')
                onConfigChange({...config, [e.target.name]: tmp})
            } else {
                onConfigChange({...config, [e.target.name]: undefined})
            }
        } else {
            onConfigChange({...config, [e.target.name]: e.target.value})
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'templates'}/>}
        nodeId={props.id}>
        <TextField
            name={'file_root'}
            label={'file_root'}
            fullWidth
            variant={'filled'}
            value={config.file_root}
            onChange={handleChange}/>
        <TextField
            name={'mime_types'}
            label={'mime_types(多个用 \',\' 分隔)'}
            fullWidth
            variant={'filled'}
            value={config.mime_types}
            onChange={handleChange}/>
        <TextField
            name={'delimiters'}
            label={'delimiters(多个用 \',\' 分隔)'}
            fullWidth
            variant={'filled'}
            value={config.delimiters}
            onChange={handleChange}/>
    </TreeItem>)
}