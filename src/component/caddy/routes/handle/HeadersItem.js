import React, { useEffect, useState } from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function HeadersItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [json,setJson] = useState('')

    useEffect(() => {
        setJson(JSON.stringify(config))
    },[config])

    const handleChange = e => {
        if (onConfigChange) {
            setJson(e.target.value)

            try {
                const json = JSON.parse(e.target.value)
                onConfigChange(json)
            } catch (e) {}
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'headers'}/>}
        nodeId={props.id}>
        <TextField
            variant={'outlined'}
            multiline
            rows={10}
            value={json}
            onChange={handleChange}
            fullWidth
        />
    </TreeItem>)
}