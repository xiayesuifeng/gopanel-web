import React, { useEffect, useState } from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import Box from '@mui/material/Box'

export default function EncodeItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [gzip, setGzip] = useState(false)
    const [zstd, setZstd] = useState(false)

    useEffect(() => {
        if (config.encodings) {
            setGzip(typeof config.encodings.gzip === 'object')
            setZstd(typeof config.encodings.zstd === 'object')
        } else {
            setGzip(false)
            setZstd(false)
        }
    }, [config.encodings])

    const handleConfigChange = e => {
        if (e.target.name === 'gzip' || e.target.name === 'zstd') {
            if (e.target.checked) {
                onConfigChange({...config, encodings: {...config.encodings, [e.target.name]: {}}})
            } else {
                delete config.encodings[e.target.name]
                if (e.target.name === 'gzip')
                    setGzip(false)
                else
                    setZstd(false)
                onConfigChange(config)
            }
        } else if (e.target.name === 'level') {
            config.encodings.gzip = {level: parseInt(e.target.value)}
            onConfigChange(config)
        } else {
            onConfigChange({...config, [e.target.name]: parseInt(e.target.value)})
        }
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'encode'}/>}
            nodeId={props.id}>
            <Box>
                <FormControlLabel
                    control={
                        <Switch
                            color="primary"
                            checked={gzip}
                            onChange={handleConfigChange}
                        />
                    }
                    name='gzip'
                    label='启用 gzip'
                />
                {gzip &&
                <TextField
                    name={'level'}
                    label={'level'}
                    fullWidth
                    variant={'filled'}
                    type="number"
                    value={config.encodings && config.encodings.gzip && config.encodings.gzip.level ? config.encodings.gzip.level : '0'}
                    onChange={handleConfigChange}/>}
            </Box>
            <Box>
                <FormControlLabel
                    control={
                        <Switch
                            color="primary"
                            checked={zstd}
                            onChange={handleConfigChange}
                        />
                    }
                    name='zstd'
                    label='启用 zstd'
                />
            </Box>
            <TextField
                name={'minimum_length'}
                label={'minimum_length'}
                fullWidth
                variant={'filled'}
                type="number"
                value={config.minimum_length ? config.minimum_length : '0'}
                onChange={handleConfigChange}/>
        </TreeItem>
    )
}