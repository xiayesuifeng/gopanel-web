import TextField from '@mui/material/TextField'
import React from 'react'
import TreeItem from '@mui/lab/TreeItem'
import TreeItemLabel from '../../../TreeItemLabel'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import IconButton from '@mui/material/IconButton'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'

export default function ReverseProxyItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleConfigChange = e => {
        if (e.target.name === 'buffer_requests')
            onConfigChange({...config, [e.target.name]: e.target.checked})
    }

    const handleUpstreamsChange = (index, event) => {
        config.upstreams[index] = {...config.upstreams[index], [event.target.name]: event.target.value}
        onConfigChange(config)
    }

    const handleAddButtonClick = () => {
        config.upstreams.push({})
        onConfigChange(config)
    }

    const handleDeleteButtonClick = index => () => {
        config.upstreams.splice(index)
        onConfigChange(config)
    }

    return (<div>
        <TreeItem
            nodeId={props.id}
            label={<TreeItemLabel label='upstreams'
                                  action={<IconButton color={'primary'} size={'small'}
                                                      onClick={handleAddButtonClick}><AddIcon/></IconButton>}/>}>
            {config.upstreams && config.upstreams.map((upstream, index) => (
                <TreeItem
                    key={`${props.id}-upstream-${index}`}
                    nodeId={`${props.id}-upstream-${index}`}
                    label={<TreeItemLabel label={index} action={<IconButton color={'primary'} size={'small'}
                                                                            onClick={handleDeleteButtonClick(index)}><DeleteIcon/></IconButton>}/>}>
                    <TextField
                        name={'dial'}
                        label={'dial'}
                        fullWidth
                        variant={'filled'}
                        value={upstream.dial}
                        onChange={e => handleUpstreamsChange(index, e)}/>
                    <TextField
                        name={'lookup_srv'}
                        label={'lookup_srv'}
                        fullWidth
                        variant={'filled'}
                        value={upstream.lookup_srv}
                        onChange={e => handleUpstreamsChange(index, e)}/>
                    <TextField
                        name={'max_requests'}
                        label={'max_requests'}
                        fullWidth
                        variant={'filled'}
                        value={upstream.max_requests}
                        onChange={e => handleUpstreamsChange(index, e)}/>
                </TreeItem>
            ))}
        </TreeItem>
        <FormControlLabel
            control={
                <Switch
                    color="primary"
                    name={'buffer_requests'}
                    checked={config.buffer_requests}
                    onChange={handleConfigChange}
                />
            }
            label="buffer_requests"
        />
    </div>)
}