import React from 'react'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import DialogActions from '@mui/material/DialogActions'
import Button from '@mui/material/Button'

export default function TypeDialog (props) {
    return (
        <Dialog onClose={props.onClose(null)} open={props.open}>
            <DialogTitle>请选择</DialogTitle>
            <List>
                {props.types.map(type => (
                    <ListItem button onClick={props.onClose(type)} key={type}>
                        <ListItemText primary={type}/>
                    </ListItem>
                ))}
            </List>
            <DialogActions>
                <Button onClick={props.onClose(null)} color="primary">
                    取消
                </Button>
            </DialogActions>
        </Dialog>
    )
}