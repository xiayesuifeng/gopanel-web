import React, { useEffect, useState } from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function HostItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [hostValue, setHostValue] = useState('')

    useEffect(() => {
        if (config) {
            let value = ''
            for (let i = 0; i < config.length; i++) {
                value += config[i]
                if (i < config.length - 1)
                    value += ','
            }
            setHostValue(value)
        } else {
            setHostValue('')
        }
    }, [config])

    const handleChange = e => {
        if (onConfigChange) {
            if (e.target.value) {
                const host = e.target.value.split(',')
                onConfigChange(host)
            } else {
                onConfigChange([])
            }
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'host'}/>}
        nodeId={props.id}>
        <TextField
            label={'host(多个用 \',\' 分隔)'}
            variant={'filled'}
            value={hostValue}
            onChange={handleChange}
            fullWidth
        />
    </TreeItem>)
}