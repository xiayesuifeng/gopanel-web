import React from 'react'
import TextField from '@mui/material/TextField'
import Table from '@mui/material/Table'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function VarsItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = (value, newKey) => {
        if (onConfigChange) {
            if (typeof value === 'object') {
                onConfigChange({...config, ...value})
            } else if (typeof value === 'string' && newKey !== '') {
                const tmp = config[value]
                delete config[value]
                onConfigChange({...config, [newKey]: tmp})
            }
        }
    }

    const handleDelete = key => () => {
        if (onConfigChange) {
            delete config[key]
            onConfigChange(config)
        }
    }

    return (
        <TreeItem
            label={<TreeItemLabel label={'vars'}/>}
            nodeId={props.id}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Value</TableCell>
                        <TableCell>
                            <IconButton color={'primary'} size="small" onClick={() => handleChange({newVariable: ''})}>
                                <AddIcon/>
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {Object.keys(config).map(key => (
                        <TableRow key={key}>
                            <TableCell>
                                <TextField
                                    value={key}
                                    onChange={e => handleChange(key, e.target.value)}
                                    fullWidth
                                />
                            </TableCell>
                            <TableCell>
                                <TextField
                                    value={config[key]}
                                    onChange={e => handleChange({[key]: e.target.value})}
                                    fullWidth
                                />
                            </TableCell>
                            <TableCell>
                                <IconButton color={'primary'} size="small" onClick={handleDelete(key)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TreeItem>
    )
}