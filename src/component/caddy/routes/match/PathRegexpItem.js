import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function PathRegexpItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => onConfigChange({...config, [e.target.name]: e.target.value})

    return (<TreeItem
        label={<TreeItemLabel label={'path_regexp'}/>}
        nodeId={props.id}>
        <TextField
            name={'name'}
            label={'name'}
            fullWidth
            variant={'filled'}
            value={config.name ? config.name : ''}
            onChange={handleChange}/>
        <TextField
            name={'pattern'}
            label={'pattern'}
            fullWidth
            variant={'filled'}
            value={config.pattern ? config.pattern : ''}
            onChange={handleChange}/>
    </TreeItem>)
}