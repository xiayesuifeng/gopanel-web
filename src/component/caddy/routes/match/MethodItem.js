import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function MethodItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (onConfigChange) {
            if (e.target.value) {
                onConfigChange(e.target.value.split(','))
            } else {
                onConfigChange([])
            }
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'method'}/>}
        nodeId={props.id}>
        <TextField
            label={'method(多个用 \',\' 分隔)'}
            variant={'filled'}
            value={config}
            onChange={handleChange}
            fullWidth
        />
    </TreeItem>)
}