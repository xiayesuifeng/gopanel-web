import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function RemoteIPItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (onConfigChange) {
            onConfigChange({ranges: e.target.value.split(',')})
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'remote_ip'}/>}
        nodeId={props.id}>
        <TextField
            label={'ranges(多个用 \',\' 分隔)'}
            variant={'filled'}
            value={config.ranges ? config.ranges : ''}
            onChange={handleChange}
            fullWidth
        />
    </TreeItem>)
}