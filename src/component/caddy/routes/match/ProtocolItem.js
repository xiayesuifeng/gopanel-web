import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function ProtocolItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (onConfigChange)
            onConfigChange(e.target.value)
    }

    return (<TreeItem
        label={<TreeItemLabel label={'protocol'}/>}
        nodeId={props.id}>
        <TextField
            label={'protocol'}
            variant={'filled'}
            value={config}
            onChange={handleChange}
            fullWidth
        />
    </TreeItem>)
}