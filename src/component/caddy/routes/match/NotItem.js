import React from 'react'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import MatchItem from '../MatchItem'

export default function NotItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleNotMatchChange = (index, newConfig) => {
        config[index] = newConfig
        onConfigChange(config)
    }

    const handleAddNotMatchClick = () => {
        onConfigChange([...config,{}])
    }

    return (<TreeItem
        nodeId={`${props.id}-not`}
        label={<TreeItemLabel label={'not'}
                              action={<IconButton color={'primary'} size={'small'}
                                                  onClick={handleAddNotMatchClick}><AddIcon/></IconButton>}/>}>
        {config.map((item, index) => (
            <MatchItem key={`${props.id}-not-${index}`} id={`${props.id}-not-${index}`} index={index} config={item}
                       onConfigChange={c => handleNotMatchChange(index, c)}/>
        ))}
    </TreeItem>)
}