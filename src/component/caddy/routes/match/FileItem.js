import React from 'react'
import TextField from '@mui/material/TextField'
import TreeItemLabel from '../../../TreeItemLabel'
import TreeItem from '@mui/lab/TreeItem'

export default function FileItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (e.target.name === 'try_files' || e.target.name === 'split_path') {
            if (e.target.value)
                onConfigChange({...config, [e.target.name]: e.target.value.split(',')})
        } else {
            onConfigChange({...config, [e.target.name]: e.target.value})
        }
    }

    return (<TreeItem
        label={<TreeItemLabel label={'file'}/>}
        nodeId={props.id}>
        <TextField
            name={'root'}
            label={'root'}
            fullWidth
            variant={'filled'}
            value={config.root ? config.root : ''}
            onChange={handleChange}/>
        <TextField
            name={'try_files'}
            label={'try_files(多个用 \',\' 分隔)'}
            fullWidth
            variant={'filled'}
            value={config.try_files ? config.try_files : ''}
            onChange={handleChange}/>

        <TextField
            name={'try_policy'}
            label={'try_policy'}
            fullWidth
            variant={'filled'}
            value={config.try_policy ? config.try_policy : ''}
            onChange={handleChange}/>
        <TextField
            name={'split_path'}
            label={'split_path(多个用 \',\' 分隔)'}
            fullWidth
            variant={'filled'}
            value={config.split_path ? config.split_path : ''}
            onChange={handleChange}/>
    </TreeItem>)
}