import TreeItemLabel from '../../TreeItemLabel'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import TreeItem from '@mui/lab/TreeItem'
import React, { useState } from 'react'
import TypeDialog from './TypeDialog'
import HostItem from './match/HostItem'
import VarsMatchItem from './match/VarsItem'
import PathItem from './match/PathItem'
import ProtocolItem from './match/ProtocolItem'
import MethodItem from './match/MethodItem'
import ExpressionItem from './match/ExpressionItem'
import FileItem from './match/FileItem'
import HeaderItem from './match/HeaderItem'
import HeaderRegexpItem from './match/HeaderRegexpItem'
import PathRegexpItem from './match/PathRegexpItem'
import QueryItem from './match/QueryItem'
import RemoteIPItem from './match/RemoteIPItem'
import VarsRegexpItem from './match/VarsRegexpItem'
import NotItem from './match/NotItem'

const matchType = [
    'expression',
    'file',
    'header',
    'header_regexp',
    'host',
    'method',
    'not',
    'path',
    'path_regexp',
    'protocol',
    'query',
    'remote_ip',
    'vars',
    'vars_regexp'
]

export default function MatchItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [matchTypeOpen, setMatchTypeOpen] = useState(false)
    const [types, setTypes] = useState(matchType)

    const getMatchItem = (key, config) => {
        switch (key) {
            case 'expression':
                return (<ExpressionItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                        onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'file':
                return (<FileItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                  onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'header':
                return (<HeaderItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                    onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'header_regexp':
                return (<HeaderRegexpItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                          onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'host':
                return (<HostItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                  onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'method':
                return (<MethodItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                    onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'not':
                return (<NotItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                 onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'path':
                return (<PathItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                  onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'path_regexp':
                return (<PathRegexpItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                        onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'protocol':
                return (<ProtocolItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                      onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'query':
                return (<QueryItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                   onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'remote_ip':
                return (<RemoteIPItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                      onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'vars':
                return (<VarsMatchItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                       onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            case 'vars_regexp':
                return (<VarsRegexpItem key={`${props.id}-${key}`} id={`${props.id}-${key}`} config={config}
                                        onConfigChange={c => handleMatchConfigChange(key, c)}/>)
            default:
                break
        }
    }

    const handleMatchConfigChange = (type, c) => {
        if (onConfigChange && type) {
            onConfigChange({...config, [type]: c})
        }
    }

    const matchTypeDialogClose = type => () => {
        if (onConfigChange && type) {
            if (type === 'path' || type === 'method' || type === 'not')
                onConfigChange({...config, [type]: []})
            else if (type === 'protocol')
                onConfigChange({...config, [type]: ''})
            else
                onConfigChange({...config, [type]: {}})
        }

        setMatchTypeOpen(false)
    }

    const handleTypeDialogOpen = () => {
        let tmp = []
        for (let i = 0; i < matchType.length; i++) {
            if (!config[matchType[i]])
                tmp.push(matchType[i])
        }

        setTypes(tmp)
        setMatchTypeOpen(true)
    }

    return (
        <div>
            <TypeDialog
                open={matchTypeOpen}
                types={types}
                onClose={matchTypeDialogClose}/>
            <TreeItem
                nodeId={props.id}
                label={<TreeItemLabel label={props.index} action={<IconButton color={'primary'} size={'small'}
                                                                              onClick={handleTypeDialogOpen}><AddIcon/></IconButton>}/>}
            >
                {Object.keys(config).map(key => (
                    getMatchItem(key, config[key])
                ))}
            </TreeItem>
        </div>

    )
}