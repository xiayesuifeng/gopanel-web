import React, { useEffect } from 'react'
import TreeView from '@mui/lab/TreeView'
import TreeItem from '@mui/lab/TreeItem'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import TreeItemLabel from '../TreeItemLabel'
import ListenItem from './ListenItem'
import Box from '@mui/material/Box'
import LogsItem from './LogsItem'
import RouteItem from './routes/RouteItem'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import AutomaticHttpsItem from './AutomaticHttpsItem'
import ListenHostEditor from '../ListenHostEditor'

export default function CaddyEditor (props) {
    let {config} = props
    const {onConfigChange} = props

    useEffect(() => {
        if (!config.routes)
            handleConfigChange({...config, routes: []})
    }, [config])

    const handleConfigChange = config => {
        onConfigChange(config)
    }

    const handleRouteChange = (index, newConfig) => {
        config.routes[index] = newConfig
        onConfigChange(config)
    }

    const handleAddButtonClick = () => {
        config.routes.push({})
        onConfigChange(config)
    }

    const array2str = array => {
        let value = ''

        if (array === undefined) {
            return value
        }

        for (let j = 0; j < array.length; j++) {
            value += array[j]
            if (j < array.length - 1)
                value += ','
        }

        return value
    }

    return (
        <Box>
            <ListenHostEditor
                disableSSL={config.disableSSL}
                domain={config.domain}
                port={config.listenPort}
                onDisableSSLChange={val => handleConfigChange({disableSSL: val})}
                onDomainChange={domain => handleConfigChange({domain: [domain]})}
                onPortChange={port => handleConfigChange({listenPort: port ? port : undefined})}
            />
            <TreeView
                defaultExpanded={['root']}
                disableSelection
                defaultCollapseIcon={<ArrowDropDownIcon/>}
                defaultExpandIcon={<ArrowRightIcon/>}
                defaultEndIcon={<div style={{width: 24}}/>}>
                <TreeItem
                    nodeId={'root'}
                    label={<TreeItemLabel label={'Config'}/>}>
                    {/*<ListenItem config={config.listen} onConfigChange={handleConfigChange}/>*/}
                    {/*<TextField*/}
                    {/*    label={'read_timeout'}*/}
                    {/*    variant={'filled'}*/}
                    {/*    fullWidth*/}
                    {/*    value={config.read_timeout}*/}
                    {/*    onChange={e => handleConfigChange({read_timeout: e.target.value ? Number(e.target.value) : undefined})}*/}
                    {/*/>*/}
                    {/*<TextField*/}
                    {/*    label={'read_header_timeout'}*/}
                    {/*    variant={'filled'}*/}
                    {/*    fullWidth*/}
                    {/*    value={config.read_header_timeout}*/}
                    {/*    onChange={e => handleConfigChange({read_header_timeout: e.target.value ? Number(e.target.value) : undefined})}*/}
                    {/*/>*/}
                    {/*<TextField*/}
                    {/*    label={'write_timeout'}*/}
                    {/*    variant={'filled'}*/}
                    {/*    fullWidth*/}
                    {/*    value={config.write_timeout}*/}
                    {/*    onChange={e => handleConfigChange({write_timeout: e.target.value ? Number(e.target.value) : undefined})}*/}
                    {/*/>*/}
                    {/*<TextField*/}
                    {/*    label={'idle_timeout'}*/}
                    {/*    variant={'filled'}*/}
                    {/*    fullWidth*/}
                    {/*    value={config.idle_timeout}*/}
                    {/*    onChange={e => handleConfigChange({idle_timeout: e.target.value ? Number(e.target.value) : undefined})}*/}
                    {/*/>*/}
                    {/*<TextField*/}
                    {/*    label={'max_header_bytes'}*/}
                    {/*    variant={'filled'}*/}
                    {/*    fullWidth*/}
                    {/*    value={config.max_header_bytes}*/}
                    {/*    onChange={e => handleConfigChange({max_header_bytes: e.target.value ? Number(e.target.value) : undefined})}*/}
                    {/*/>*/}
                    <TreeItem nodeId={'routes'}
                              label={<TreeItemLabel label={'routes'}
                                                    action={<IconButton color={'primary'} size={'small'}
                                                                        onClick={handleAddButtonClick}><AddIcon/></IconButton>}/>}>
                        {config.routes && config.routes.map((route, index) => (
                            <RouteItem key={`routes-${index}`} id={`routes-${index}`} index={index} config={route}
                                       onConfigChange={c => handleRouteChange(index, c)}/>
                        ))}
                    </TreeItem>
                    {/*<AutomaticHttpsItem config={config.automatic_https} onConfigChange={handleConfigChange}/>*/}
                    {/*<LogsItem config={config.logs} onConfigChange={handleConfigChange}/>*/}
                    {/*<Box>*/}
                    {/*    <FormControlLabel*/}
                    {/*        control={*/}
                    {/*            <Switch*/}
                    {/*                color="primary"*/}
                    {/*                checked={config.strict_sni_host}*/}
                    {/*                onChange={e => handleConfigChange({strict_sni_host: e.target.checked})}*/}
                    {/*            />*/}
                    {/*        }*/}
                    {/*        label="strict_sni_host"*/}
                    {/*    />*/}
                    {/*</Box>*/}
                    {/*<Box>*/}
                    {/*    <FormControlLabel*/}
                    {/*        control={*/}
                    {/*            <Switch*/}
                    {/*                color="primary"*/}
                    {/*                checked={config.experimental_http3}*/}
                    {/*                onChange={e => handleConfigChange({experimental_http3: e.target.checked})}*/}
                    {/*            />*/}
                    {/*        }*/}
                    {/*        label="实验性 HTTP3"*/}
                    {/*    />*/}
                    {/*</Box>*/}
                    {/*<Box>*/}
                    {/*    <FormControlLabel*/}
                    {/*        control={*/}
                    {/*            <Switch*/}
                    {/*                color="primary"*/}
                    {/*                checked={config.allow_h2c ? config.allow_h2c : false}*/}
                    {/*                onChange={e => handleConfigChange({allow_h2c: e.target.checked})}*/}
                    {/*            />*/}
                    {/*        }*/}
                    {/*        label="allow_h2c"*/}
                    {/*    />*/}
                    {/*</Box>*/}
                </TreeItem>
            </TreeView>
        </Box>
    )
}
