import React from 'react'
import TreeItem from '@mui/lab/TreeItem'
import TextField from '@mui/material/TextField'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import TreeItemLabel from '../TreeItemLabel'
import Box from '@mui/material/Box'

export default function AutomaticHttpsItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const handleChange = e => {
        if (onConfigChange) {
            if (e.target.name === 'disable' || e.target.name === 'disable_redirects' || e.target.name === 'ignore_loaded_certificates')
                onConfigChange({automatic_https: {...config, [e.target.name]: e.target.checked}})
            else {
                if (e.target.value) {
                    onConfigChange({automatic_https: {...config, [e.target.name]: e.target.value.split(',')}})
                } else {
                    onConfigChange({automatic_https: {...config, [e.target.name]: undefined}})
                }
            }
        }
    }

    return (<TreeItem
        nodeId={'automatic_https'}
        label={<TreeItemLabel label={'automatic_https'}/>}>
        <Box>
            <FormControlLabel
                control={
                    <Switch
                        name={'disable'}
                        color="primary"
                        checked={config && config.disable ? config.disable : false}
                        onChange={handleChange}
                    />
                }
                label="disable"
            />
        </Box>
        <Box>
            <FormControlLabel
                control={
                    <Switch
                        name={'disable_redirects'}
                        color="primary"
                        checked={config && config.disable_redirects ? config.disable_redirects : false}
                        onChange={handleChange}
                    />
                }
                label="disable_redirects"
            />
        </Box>
        <TextField
            label={'skip(多个用 \',\' 分隔)'}
            name={'skip'}
            variant={'filled'}
            value={config && config.skip ? config.skip : ''}
            onChange={handleChange}
            fullWidth
        />
        <TextField
            label={'skip_certificates(多个用 \',\' 分隔)'}
            name={'skip_certificates'}
            variant={'filled'}
            value={config && config.skip_certificates ? config.skip_certificates : ''}
            onChange={handleChange}
            fullWidth
        />
        <FormControlLabel
            control={
                <Switch
                    name={'ignore_loaded_certificates'}
                    color="primary"
                    checked={config && config.ignore_loaded_certificates ? config.ignore_loaded_certificates : false}
                    onChange={handleChange}
                />
            }
            label="ignore_loaded_certificates"
        />
    </TreeItem>)
}