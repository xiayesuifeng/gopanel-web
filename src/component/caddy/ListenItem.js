import TextField from '@mui/material/TextField'
import React, { useEffect, useState } from 'react'

export default function ListenItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [listenValue, setListenValue] = useState('')

    useEffect(() => {
        if (config) {
            let value = ''
            for (let i = 0; i < config.length; i++) {
                value += config[i]
                if (i < config.length - 1)
                    value += ','
            }
            setListenValue(value)
        } else {
            setListenValue('')
        }
    }, [config])

    const handleChange = e => {
        if (onConfigChange) {
            if (e.target.value) {
                const address = e.target.value.split(',')
                onConfigChange({listen: address})
            } else {
                onConfigChange({listen: undefined})
            }
        }
    }

    return (<TextField
            label={'listen(多个用 \',\' 分隔)'}
            variant={'filled'}
            value={listenValue}
            onChange={handleChange}
            fullWidth
        />)
}