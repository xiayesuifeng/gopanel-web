import React, { useEffect, useState } from 'react'
import TreeItem from '@mui/lab/TreeItem'
import TextField from '@mui/material/TextField'
import Switch from '@mui/material/Switch'
import FormControlLabel from '@mui/material/FormControlLabel'
import TreeItemLabel from '../TreeItemLabel'

export default function LogsItem (props) {
    const {config} = props
    const {onConfigChange} = props

    const [hostsValue, setHostsValue] = useState('')

    const handleChange = e => {
        if (onConfigChange) {
            if (e.target.name === 'skip_unmapped_hosts') {
                onConfigChange({logs: {...config, [e.target.name]: e.target.checked}})
            } else if (e.target.name === 'skip_hosts') {
                if (e.target.value) {
                    const hosts = e.target.value.split(',')
                    onConfigChange({logs: {...config, 'skip_hosts': hosts}})
                } else {
                    onConfigChange({logs: {...config, 'skip_hosts': undefined}})
                }
            } else if (e.target.name === 'logger_names') {
                onConfigChange({logs: {...config, 'logger_names': JSON.parse(e.target.value)}})
            } else {
                onConfigChange({logs: {...config, [e.target.name]: e.target.value}})
            }
        }
    }

    useEffect(() => {
        if (config && config.skip_hosts) {
            let value = ''
            for (let i = 0; i < config.skip_hosts.length; i++) {
                value += config.skip_hosts[i]
                if (i < config.skip_hosts.length - 1)
                    value += ','
            }
            setHostsValue(value)
        } else {
            setHostsValue('')
        }
    }, [config])

    return (<TreeItem
        nodeId={'logs'}
        label={<TreeItemLabel label={'logs'}/>}>
        <TextField
            label={'default_logger_name'}
            name={'default_logger_name'}
            variant={'filled'}
            value={config && config.default_logger_name}
            onChange={handleChange}
            fullWidth
        />
        <TextField
            label={'logger_names'}
            name={'logger_names'}
            variant={'filled'}
            value={config && config.logger_names ? JSON.stringify(config.logger_names) : '{}'}
            onChange={handleChange}
            fullWidth
        />
        <TextField
            label={'skip_hosts(多个用 \',\' 分隔)'}
            name={'skip_hosts'}
            variant={'filled'}
            value={hostsValue}
            onChange={handleChange}
            fullWidth
        />
        <FormControlLabel
            control={
                <Switch
                    name={'skip_unmapped_hosts'}
                    color="primary"
                    checked={config && config.skip_unmapped_hosts}
                    onChange={handleChange}
                />
            }
            label="skip_unmapped_hosts"
        />
    </TreeItem>)
}