import Typography from '@mui/material/Typography'
import React from 'react'
import Paper from '@mui/material/Paper'

const paperStyle = {
    p: 3,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    textAlign: 'left',
    borderRadius: 4,
    mt: 3,
}

export default function LabelPaper (props) {
    return (
        <Paper sx={paperStyle}>
            {props.label ?? <Typography variant={'h6'}>{props.label}</Typography>}
            {props.children}
        </Paper>
    )
}