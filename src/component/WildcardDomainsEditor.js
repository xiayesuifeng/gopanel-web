import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import Toolbar from "@mui/material/Toolbar";
import React, {useState} from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import {useSnackbar} from "notistack";

export default function WildcardDomainsEditor(props) {
    const {value, onChange} = props

    const [dialogOpen, setDialogOpen] = useState(false)
    const [wildcardDomain,setWildcardDomain] =useState('')

    const {enqueueSnackbar} = useSnackbar()

    const handleAddWildcardDomain = () => {
        if (value.indexOf(wildcardDomain) !== -1) {
            enqueueSnackbar('该泛域名已存在，无需重复添加', {variant: 'error'})
            return
        }

        value.push(wildcardDomain)

        setDialogOpen(false)
        setWildcardDomain('')
        onChange(value)
    }

    const handleDeleteWildcardDomain = (domain) => {
        onChange(value.filter(value => value !== domain))
    }

    const dialog = (
        <Dialog onClose={() => setDialogOpen(false)} open={dialogOpen}>
            <DialogTitle>添加泛域名</DialogTitle>
            <DialogContent sx={{display: 'flex', flexDirection: 'column'}}>
                <DialogContentText>
                    泛域名的 DNS Challenge 信息会根据 DNS Challenge 模板自动匹配，请确保对应的模板已添加。如存在域名 example.com 的模板，则 *.example.com 会匹配此模板，否则将视为无效信息而跳过。
                </DialogContentText>
                <TextField label="泛域名"
                           value={wildcardDomain}
                           helperText="例: *.example.com"
                           margin={'normal'}
                           onChange={e => setWildcardDomain(e.target.value)}/>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleAddWildcardDomain}>完成</Button>
            </DialogActions>
        </Dialog>
    )

    return (
        <>
            <Toolbar sx={{width: '100%'}}>
                <Typography sx={{flex: '1 1 100%'}} variant="h6">泛域名</Typography>
                <Tooltip title="添加泛域名">
                    <IconButton onClick={() => setDialogOpen(true)}>
                        <AddIcon/>
                    </IconButton>
                </Tooltip>
            </Toolbar>
            <List sx={{width: '100%'}}>
                {value.map((domain,index) => (
                    <ListItem
                        key={index}
                        secondaryAction={
                            <IconButton aria-label="删除泛域名" onClick={() => handleDeleteWildcardDomain(domain)}>
                                <DeleteIcon />
                            </IconButton>
                        }>
                        <ListItemText primary={domain} />
                    </ListItem>
                ))}
            </List>
            {dialog}
        </>
    )
}
