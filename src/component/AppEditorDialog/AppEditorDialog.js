import React, { useEffect, useRef, useState } from 'react'
import Dialog from '@mui/material/Dialog'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import CloseIcon from '@mui/icons-material/Close'
import Button from '@mui/material/Button'
import { styled, useTheme } from '@mui/material/styles'
import Slide from '@mui/material/Slide'
import { Box, Collapse, Stack } from '@mui/material'
import TextField from '@mui/material/TextField'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogTitle from '@mui/material/DialogTitle'
import Hidden from '@mui/material/Hidden'
import DialogActions from '@mui/material/DialogActions'
import CaddyEditor from '../caddy/CaddyEditor'
import Tabs from '@mui/material/Tabs'
import Tab, { tabClasses } from '@mui/material/Tab'
import { CaddyEasyEditor } from '../CaddyEasyEditor'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Paper from '@mui/material/Paper'
import DoneIcon from '@mui/icons-material/Done'
import { blue } from '@mui/material/colors'
import Template, { templates } from './Teamplates/Templates'
import SvelteJSONEditor from '../SvelteJSONEditor'

const templatePaper = {
    display: 'flex',
    padding: theme => theme.spacing(1, 1, 1, 3),
    height: 100,
    width: 250,
    margin: theme => theme.spacing(1, 0, 1, 0),
    alignItems: 'center',
    position: 'relative'
}
const paperChecked = {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: theme => theme.palette.primary.main,
    color: '#FFFFFF',
    width: 34,
    height: 34,
    textAlign: 'right',
    borderRadius: '0 4px 0 90px'
}

const steps = [
    {
        label: '选择模板'
    },
    {
        label: '常规设置'
    },
    {
        label: '路由设置'
    }
]

const Transition = React.forwardRef(function Transition (props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

function ExecBackendConfigEditor (props) {
    const {config, onChange} = props

    const handleConfigChange = event => {
        switch (event.target.name) {
            case 'autoReboot':
                onChange({...config, [event.target.name]: event.target.checked})
                break
            default:
                onChange({...config, [event.target.name]: event.target.value})
                break
        }
    }

    return (
        <>
            <Box p={1}>
                <TextField
                    fullWidth
                    variant={'outlined'}
                    label={'工作目录'}
                    name={'workingDirectory'}
                    value={config.workingDirectory ?? ''}
                    onChange={handleConfigChange}
                />
            </Box>
            <Box p={1}>
                <TextField
                    fullWidth
                    variant={'outlined'}
                    label={'程序路径'}
                    name={'path'}
                    value={config.path ?? ''}
                    onChange={handleConfigChange}
                />
            </Box>
            <Box p={1}>
                <TextField
                    fullWidth
                    variant={'outlined'}
                    label={'运行参数'}
                    name={'argument'}
                    value={config.argument ?? ''}
                    onChange={handleConfigChange}
                />
            </Box>
            <Box p={1}>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={config.autoReboot ?? true}
                            color="primary"
                            name={'autoReboot'}
                            onChange={handleConfigChange}
                        />
                    }
                    label="自动重启后端"
                />
            </Box>
        </>
    )
}

const ModeTabs = styled(Tabs)`
  border-radius: 45px;
  background-color: #ebebeb;
  min-height: 25px;
`

const ModeTab = styled(Tab)`
  font-size: 0.5rem;
  border-radius: 45px;
  min-height: 20px;
  min-width: 50px;
  padding: 5px 7px 5px 7px;

  &.${tabClasses.selected} {
    background-color: ${blue[50]};
    color: ${blue[600]};
  }
`

export default function AppEditorDialog (props) {
    const theme = useTheme()
    const mdDown = useMediaQuery(theme.breakpoints.down('md'))

    const {config, onConfigChange, onDone, onClose} = props

    const [routeModeIdx, setRouteModeIdx] = useState(0)
    const [activeStep, setActiveStep] = useState(0)
    const [template, setTemplate] = useState('')

    const [nameError, setNameError] = useState(false)

    const templateRef = useRef()

    const handleConfigChange = event => {
        switch (event.target.name) {
            case 'caddyConfig':
                onConfigChange({...config, [event.target.name]: JSON.parse(event.target.value)})
                break
            case 'name':
                if (nameError && event.target.value !== '')
                    setNameError(false)
                onConfigChange({...config, [event.target.name]: event.target.value})
                break
            default:
                onConfigChange({...config, [event.target.name]: event.target.value})
                break

        }
    }

    const handleCaddyChange = c => {
        onConfigChange({...config, caddyConfig: {...config.caddyConfig, ...c}})
    }

    const handleBack = () => activeStep === 0 ? onClose() : setActiveStep((prevActiveStep) => prevActiveStep - 1)

    const handleNext = () => {
        if (config.editName === '' && activeStep === 0 && templateRef.current?.renderConfig) {
            const {config, ok} = templateRef.current?.renderConfig()
            if (!ok)
                return

            onConfigChange(config)
        }

        if (activeStep === (config.editName === '' ? 1 : 0) && !config.name) {
            setNameError(true)
            return
        }

        if (activeStep === (config.editName === '' ? 2 : 1)) {
            setActiveStep(0)
            setTemplate('')

            onDone()
        } else
            setActiveStep((prevActiveStep) => prevActiveStep + 1)

    }

    const TemplateStep = (
        <>
            <Collapse in={template === ''} collapsedSize={100}>
                <Box p={3} sx={{
                    display: 'flex',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                    width: '100%'
                }}>
                    {templates.map((t, idx) => (
                        <Paper sx={templatePaper} key={idx} onClick={() => setTemplate(t.name)}>
                            <Typography>{t.name}</Typography>
                            {template === t.name &&
                                <Box component={'span'} sx={paperChecked}><DoneIcon/></Box>}
                        </Paper>
                    ))}
                </Box>
            </Collapse>

            <Collapse sx={{width: '100%', bottom: 0}} in={template !== ''} collapsedSize={45}>
                <Paper sx={{
                    borderRadius: '45px 45px 0 0',
                    padding: '15px 45px 15px 45px',
                    marginTop: '4px',
                    boxShadow: `0px -2px 2px -8px rgb(0 0 0 / 20%), 
                       0px -4px 2px -4px rgb(0 0 0 / 14%), 
                       0px -4px 5px -2px rgb(0 0 0 / 12%)`
                }}>
                    <Box display={'flex'} justifyContent={'space-between'}>
                        <Typography variant={'h6'}>{template === '' ? '请选择模板' : template}</Typography>
                        {template !== '' && <Button variant={'text'} onClick={() => setTemplate('')}>重新选择</Button>}
                    </Box>
                    {template !== '' && <Template ref={templateRef} name={template}/>}
                </Paper>
            </Collapse>
        </>
    )

    const GeneralStep = (
        <Box p={3} sx={{display: 'flex', flexDirection: 'column'}}>
            <FormControl sx={{m: 1}}>
                <InputLabel>应用类型</InputLabel>
                <Select
                    value={config.type}
                    name={'type'}
                    label={'应用类型'}
                    onChange={handleConfigChange}
                >
                    <MenuItem value={1}>Go</MenuItem>
                    <MenuItem value={2}>JAVA</MenuItem>
                    <MenuItem value={3}>PHP</MenuItem>
                    <MenuItem value={5}>反向代理</MenuItem>
                    <MenuItem value={6}>静态网页</MenuItem>
                    <MenuItem value={4}>其他</MenuItem>
                </Select>
            </FormControl>
            {config.editName === '' &&
                <Box p={1}>
                    <TextField
                        fullWidth
                        variant={'outlined'}
                        label={'应用名'}
                        name={'name'}
                        error={nameError}
                        helperText={nameError ? '应用名不能为空' : ''}
                        value={config.name}
                        onChange={handleConfigChange}
                    />
                </Box>}
            <FormControl sx={{m: 1}}>
                <InputLabel>后端类型</InputLabel>
                <Select
                    value={config.backendType}
                    label="后端类型"
                    name={'backendType'}
                    onChange={handleConfigChange}
                >
                    <MenuItem value={'none'}>none</MenuItem>
                    <MenuItem value={'exec'}>exec</MenuItem>
                </Select>
            </FormControl>
            {config.backendType === 'exec' &&
                <ExecBackendConfigEditor
                    config={config.backendConfig ?? {}}
                    onChange={c => onConfigChange({...config, backendConfig: c})}
                />}
        </Box>
    )

    const RouteStep = (
        <Box p={1} pl={3} pr={3}>
            <Box sx={{display: 'flex', justifyContent: 'center', width: '100%', bgcolor: 'background.paper', m: 1}}>
                <ModeTabs indicatorColor="primary"
                          textColor="primary"
                          value={routeModeIdx}
                          onChange={(e, value) => setRouteModeIdx(value)}>
                    <ModeTab label="简单"/>
                    <ModeTab label="高级"/>
                    <ModeTab label="专家"/>
                </ModeTabs>
            </Box>
            {routeModeIdx === 0 &&
                <CaddyEasyEditor config={config.caddyConfig} onConfigChange={handleCaddyChange}/>}
            {routeModeIdx === 1 && <CaddyEditor config={config.caddyConfig} onConfigChange={handleCaddyChange}/>}
            {routeModeIdx === 2 && <SvelteJSONEditor content={{json: config.caddyConfig}}
                                                     onChange={value => onConfigChange({...config, caddyConfig: value.json})}/>}
        </Box>
    )

    return (
        <Dialog fullWidth fullScreen={mdDown} open={props.open} onClose={() => {
            setActiveStep(0)
            setTemplate('')

            props.onClose()
        }}
                TransitionComponent={Transition}>
            <Hidden mdUp>
                <AppBar sx={{
                    position: 'relative',
                    marginBottom: 3
                }}>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={props.onClose}
                            aria-label="close"
                            size="large">
                            <CloseIcon/>
                        </IconButton>
                        <Typography variant="h6" sx={{marginLeft: 2, flex: 1}}>
                            {config.editName === '' ? '添加' : '编辑'}应用
                        </Typography>
                        <Button autoFocus color="inherit" onClick={props.onClose} variant={'outlined'}>
                            确定
                        </Button>
                    </Toolbar>
                </AppBar>
            </Hidden>
            <Hidden mdDown>
                <DialogTitle>{config.editName === '' ? '添加' : '编辑'}应用</DialogTitle>
            </Hidden>
            <Box sx={{p: 3}}>
                <Stepper activeStep={activeStep}>
                    {steps.filter(step => !(config.editName !== '' && step.label === '选择模板'))
                        .map((step) =>
                            (
                                <Step key={step.label}>
                                    <StepLabel>
                                        {step.label}
                                    </StepLabel>
                                </Step>
                            )
                        )}
                </Stepper>
            </Box>
            {activeStep === 0 - (config.editName !== '' ? 1 : 0) && TemplateStep}
            {activeStep === 1 - (config.editName !== '' ? 1 : 0) && GeneralStep}
            {activeStep === 2 - (config.editName !== '' ? 1 : 0) && RouteStep}
            <Hidden mdDown>
                <DialogActions>
                    <Button onClick={handleBack}>{activeStep === 0 ? '取消' : '上一步'}</Button>
                    <Button onClick={handleNext}>
                        {activeStep === (config.editName === '' ? 2 : 1) ? '完成' : config.editName === '' && activeStep === 0 && template === '' ? '跳过' : '下一步'}
                    </Button>
                </DialogActions>
            </Hidden>
        </Dialog>
    )
}