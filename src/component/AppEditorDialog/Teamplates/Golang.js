import TextField from '@mui/material/TextField'
import React, { forwardRef, useImperativeHandle, useState } from 'react'
import Typography from '@mui/material/Typography'
import { FormGroup } from '@mui/material'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'

export default forwardRef(function GolangRender (props, ref) {
    const [workPath, setWorkPath] = useState('')
    const [execPath, setExecPath] = useState('')
    const [args, setArgs] = useState('')
    const [port, setPort] = useState(8080)
    const [prefixURI, setPrefixURI] = useState('/api/*')
    const [autoReboot, setAutoReboot] = useState(true)

    const [execPathError, setExecPathError] = useState(false)
    const [portError, setPortError] = useState(false)

    useImperativeHandle(ref, () => ({renderConfig}))

    const renderConfig = () => {
        let ok = true

        if (execPath === '') {
            setExecPathError(true)
            ok = false
        }

        if (port === 0) {
            setPortError(true)
            ok = false
        }

        if (!ok)
            return {ok: ok}

        let config = {
            type: 1,
            backendType: 'exec',
            backendConfig: {
                argument: args,
                autoReboot: autoReboot,
                path: execPath,
                workingDirectory: workPath
            },
            routes: [
                {
                    handle: [
                        {
                            handler: 'subroute',
                            routes: [
                                {
                                    handle: [
                                        {
                                            handler: 'reverse_proxy',
                                            upstreams: [{dial: `localhost:${port}`}]
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    terminal: true
                }
            ]
        }

        if (prefixURI)
            config.routes[0].handle[0].routes[0].match = [{path: [prefixURI]}]

        return {config: config, ok: true}
    }

    const handleExecPathChange = path => {
        setExecPath(path)
        if (execPath && execPathError)
            setExecPathError(false)
    }

    return (
        <>
            <Typography variant={'subtitle2'}>后端应用</Typography>
            <TextField label={'工作目录'} value={workPath} onChange={e => setWorkPath(e.target.value)}/>
            <TextField
                required
                label={'程序路径'}
                value={execPath}
                error={execPathError}
                helperText={execPathError ? '程序路径不能为空' : ''}
                onChange={e => handleExecPathChange(e.target.value)}/>
            <TextField label={'运行参数'} value={args} onChange={e => setArgs(e.target.value)}/>
            <FormGroup>
                <FormControlLabel
                    control={<Switch checked={autoReboot} onChange={e => setAutoReboot(e.target.checked)}/>}
                    label="程序退出自动重启"/>
            </FormGroup>
            <Typography variant={'subtitle2'}>后端反向代理</Typography>
            <TextField
                required
                label={'监听端口'}
                type="number"
                value={port}
                error={portError}
                helperText={portError ? '端口不能为 0' : ''} onChange={e => setPort(Number(e.target.value))}/>
            <TextField label={'路由匹配前缀'} value={prefixURI} onChange={e => setPrefixURI(e.target.value)}/>
        </>
    )
})