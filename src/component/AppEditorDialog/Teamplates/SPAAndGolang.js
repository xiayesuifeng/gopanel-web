import TextField from '@mui/material/TextField'
import React, { forwardRef, useImperativeHandle, useState } from 'react'
import Typography from '@mui/material/Typography'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import { FormGroup } from '@mui/material'

export default forwardRef(function SPAAndGolangRender (props, ref) {
    const [path, setPath] = useState('')
    const [workPath, setWorkPath] = useState('')
    const [execPath, setExecPath] = useState('')
    const [args, setArgs] = useState('')
    const [port, setPort] = useState(8080)
    const [prefixURI, setPrefixURI] = useState('/api/*')
    const [autoReboot, setAutoReboot] = useState(true)

    const [pathError, setPathError] = useState(false)
    const [execPathError, setExecPathError] = useState(false)
    const [portError, setPortError] = useState(false)

    useImperativeHandle(ref, () => ({renderConfig}))

    const renderConfig = () => {
        let ok = true

        if (execPath === '') {
            setExecPathError(true)
            ok = false
        }

        if (port === 0) {
            setPortError(true)
            ok = false
        }

        if (path === '') {
            setPathError(true)
            ok = false
        }

        if (!ok)
            return {ok: ok}

        let config = {
            type: 1,
            backendType: 'exec',
            backendConfig: {
                argument: args,
                autoReboot: autoReboot,
                path: execPath,
                workingDirectory: workPath
            },
            routes: [
                {
                    handle: [
                        {
                            handler: 'subroute',
                            routes: [
                                {
                                    handle: [
                                        {
                                            handler: 'vars',
                                            root: path
                                        }
                                    ]
                                },
                                {
                                    handle: [
                                        {
                                            handler: 'rewrite',
                                            uri: '{http.matchers.file.relative}'
                                        }
                                    ],
                                    match: [
                                        {
                                            file: {
                                                try_files: [
                                                    '{http.request.uri.path}',
                                                    '{http.request.uri.path}/',
                                                    '/index.html'
                                                ]
                                            },
                                            not: [
                                                {
                                                    path: ['/api/*']
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    handle: [
                                        {
                                            handler: 'reverse_proxy',
                                            upstreams: [{dial: `localhost:${port}`}]
                                        }
                                    ]
                                },
                                {
                                    handle: [
                                        {
                                            handler: 'file_server'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            terminal: true
        }

        if (prefixURI)
            config.routes[0].handle[0].routes[2].match = [{path: [prefixURI]}]

        return {config: config, ok: true}
    }

    const handleExecPathChange = path => {
        setExecPath(path)
        if (execPath && execPathError)
            setExecPathError(false)
    }

    const handlePathChange = path => {
        setPath(path)
        if (path && pathError)
            setPathError(false)
    }

    return (
        <>
            <Typography variant={'subtitle2'}>单页前端应用</Typography>
            <TextField
                required
                error={pathError}
                helperText={pathError ? '路径不能为空' : ''}
                label={'前端路径'}
                value={path}
                onChange={e => handlePathChange(e.target.value)}/>
            <Typography variant={'subtitle2'}>后端应用</Typography>
            <TextField label={'工作目录'} value={workPath} onChange={e => setWorkPath(e.target.value)}/>
            <TextField
                required
                label={'程序路径'}
                value={execPath}
                error={execPathError}
                helperText={execPathError ? '程序路径不能为空' : ''}
                onChange={e => handleExecPathChange(e.target.value)}/>
            <TextField label={'运行参数'} value={args} onChange={e => setArgs(e.target.value)}/>
            <FormGroup>
                <FormControlLabel
                    control={<Switch checked={autoReboot} onChange={e => setAutoReboot(e.target.checked)}/>}
                    label="程序退出自动重启"/>
            </FormGroup>
            <Typography variant={'subtitle2'}>后端反向代理</Typography>
            <TextField
                required
                label={'监听端口'}
                type="number"
                value={port}
                error={portError}
                helperText={portError ? '端口不能为 0' : ''} onChange={e => setPort(Number(e.target.value))}/>
            <TextField label={'路由匹配前缀'} value={prefixURI} onChange={e => setPrefixURI(e.target.value)}/>
        </>
    )
})