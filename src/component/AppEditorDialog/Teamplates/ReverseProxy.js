import TextField from '@mui/material/TextField'
import React, { forwardRef, useImperativeHandle, useState } from 'react'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import { FormGroup, FormHelperText, Select } from '@mui/material'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import IconButton from '@mui/material/IconButton'
import ClearIcon from '@mui/icons-material/Clear'
import AddIcon from '@mui/icons-material/Add'
import Button from '@mui/material/Button'

const networks = ['tcp', 'tcp4', 'tcp6',
    'udp', 'udp4', 'udp6',
    'ip', 'ip4', 'ip6',
    'unix', 'unixgram', 'unixpacket']

export default forwardRef(function ReverseProxyRender (props, ref) {
    const [upstreams, setUpstreams] = useState([{network: '', address: '', port: ''}])
    const [tls, setTLS] = useState(false)
    const [insecureSkipVerify, setInsecureSkipVerify] = useState(false)
    const [error, setError] = useState(false)

    useImperativeHandle(ref, () => ({renderConfig}))

    const handleDeleteUpstream = (index) => () => {
        if (index === 0)
            return

        upstreams.splice(index, 1)
        setUpstreams([...upstreams])
    }

    const handleAddUpstream = () => {
        setUpstreams([...upstreams, {network: '', address: '', port: ''}])
    }

    const handleUpdateUpstreamNetwork = (index, value) => {
        upstreams[index].network = value
        setUpstreams([...upstreams])
    }

    const handleUpdateUpstreamAddress = (index, value) => {
        if (value && error)
            setError(false)

        upstreams[index].address = value
        setUpstreams([...upstreams])
    }

    const handleUpdateUpstreamPort = (index, value) => {
        upstreams[index].port = value
        setUpstreams([...upstreams])
    }

    const handleTLSChange = checked => {
        setTLS(checked)
    }

    const handleInsecureSkipVerifyChange = checked => {
        setInsecureSkipVerify(checked)
    }

    const renderConfig = () => {
        let upstreamsConfig = []

        for (const upstream of upstreams) {
            if (!upstream.address && !upstream.port)
                continue

            let dial = upstream.network

            if (dial)
                dial += '/'

            if (!upstream.network.startsWith('unix') && upstream.port)
                dial += `${upstream.address}:${upstream.port}`
            else if (!upstream.network.startsWith('unix') && !upstream.port)
                dial += `${upstream.address}:${tls ? 443 : 80}`
            else
                dial += `${upstream.address}`

            upstreamsConfig.push({dial: dial})
        }

        if (upstreamsConfig.length === 0) {
            setError(true)
            return {ok: false}
        }

        let config = {
            type: 5,
            backendType: 'none',
            backendConfig: {},
            routes: [
                {
                    handle: [
                        {
                            handler: 'subroute',
                            routes: [
                                {
                                    handle: [
                                        {
                                            handler: 'reverse_proxy',
                                            upstreams: upstreamsConfig
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    terminal: true
                }
            ]
        }

        if (tls)
            config.routes[0].handle[0].routes[0].handle[0].transport = {
                protocol: 'http',
                tls: {
                    insecure_skip_verify: insecureSkipVerify
                }
            }

        return {config: config, ok: true}
    }

    return (
        <>
            <Typography variant={'subtitle2'}>上游地址</Typography>
            <Stack direction={'column'} spacing={3} sx={{marginTop: '15px'}}>
                {upstreams.map((upstream, index) => (
                    <Box key={index} display={'flex'} alignItems={'baseline'}>
                        <FormControl sx={{minWidth: 100}}>
                            <InputLabel>网络</InputLabel>
                            <Select autoWidth label={'网络'} value={upstream.network}
                                    onChange={e => handleUpdateUpstreamNetwork(index, e.target.value)}>
                                <MenuItem value="">
                                    默认
                                </MenuItem>
                                {networks.map(network => (
                                    <MenuItem key={network} value={network}>{network}</MenuItem>))}
                            </Select>
                            <FormHelperText>非必选</FormHelperText>
                        </FormControl>
                        <TextField required sx={{flexGrow: 1}} label={'地址'} value={upstream.address}
                                   error={error}
                                   helperText={error ? '地址不能为空' : '主机名/域名/IP/Unix socket路径'}
                                   onChange={e => handleUpdateUpstreamAddress(index, e.target.value)}/>
                        {!upstream.network.startsWith('unix') &&
                            <TextField sx={{maxWidth: 100}} label={'端口'} value={upstream.port}
                                       helperText={'可留空'}
                                       onChange={e => handleUpdateUpstreamPort(index, e.target.value)}/>}
                        <IconButton color={'secondary'} size={'small'} disabled={upstreams.length === 1}
                                    onClick={handleDeleteUpstream(index)}>
                            <ClearIcon fontSize="inherit"/>
                        </IconButton>
                    </Box>
                ))}
                <Button variant={'outlined'}
                        startIcon={<AddIcon/>}
                        onClick={handleAddUpstream}>
                    添加
                </Button>
            </Stack>
            <FormGroup>
                <FormControlLabel control={<Switch checked={tls} onChange={e => handleTLSChange(e.target.checked)}/>}
                                  label="启用 TLS"/>
                <FormControlLabel disabled={!tls} control={<Switch checked={insecureSkipVerify}
                                                                   onChange={e => handleInsecureSkipVerifyChange(e.target.checked)}/>}
                                  label="允许不安全的 TLS 证书"/>
            </FormGroup>
        </>
    )
})