import React, { forwardRef, useImperativeHandle, useRef, useState } from 'react'
import ReverseProxyRender from './ReverseProxy'
import SPARender from './SPA'
import SPAAndGolangRender from './SPAAndGolang'
import GolangRender from './Golang'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'
import TextField from '@mui/material/TextField'

export const templates = [
    {name: '反向代理', render: ReverseProxyRender},
    {name: '单页前端应用 (SPA)', render: SPARender},
    {name: '单页前端应用 (SPA)+Golang 后端应用', render: SPAAndGolangRender},
    {name: 'Golang后端应用', render: GolangRender}
]

function Template (props, ref) {
    const {name} = props

    const [domain, setDomain] = useState('')
    const [error, setError] = useState(false)

    const renderRef = useRef()

    useImperativeHandle(ref, () => ({renderConfig}))

    if (name === '')
        return

    const renderConfig = () => {
        if (!domain) {
            setError(true)
            return {ok: false}
        }

        if (renderRef.current?.renderConfig) {
            const {config, ok} = renderRef.current?.renderConfig()
            if (!ok)
                return {ok: false}
            else
                return {
                    config: {
                        type: config.type,
                        editName: '',
                        name: '',
                        backendType: config.backendType,
                        backendConfig: config.backendConfig,
                        caddyConfig: {
                            domain: [domain],
                            routes: config.routes
                        },
                        version: '1.0.0'
                    },
                    ok: true
                }
        }

        return {config: {caddyConfig: {domain: [domain]}}, ok: true}
    }

    const handleDomainChange = domain => {
        if (domain && error)
            setError(false)

        setDomain(domain)
    }

    let Render = templates.find(t => t.name === name)?.render

    if (Render)
        return (
            <Stack direction={'column'} spacing={3} sx={{marginTop: '15px'}}>
                <TextField required error={error} label={'域名'} value={domain} helperText={error ? '请输入域名' : ''}
                           onChange={e => handleDomainChange(e.target.value)}/>
                <Render ref={renderRef}/>
            </Stack>
        )
    else
        return <Typography>模板不存在</Typography>
}

export default forwardRef(Template)