import TextField from '@mui/material/TextField'
import React, { forwardRef, useImperativeHandle, useState } from 'react'

export default forwardRef(function SPARender (props, ref) {
    const [path, setPath] = useState('')

    const [pathError, setPathError] = useState(false)

    useImperativeHandle(ref, () => ({renderConfig}))

    const renderConfig = () => {
        if (path === '') {
            setPathError(true)
            return {ok: false}
        }

        let config = {
            type: 6,
            backendType: 'none',
            backendConfig: {},
            routes: [
                {
                    handle: [
                        {
                            handler: 'subroute',
                            routes: [
                                {
                                    handle: [
                                        {
                                            handler: 'vars',
                                            root: path
                                        }
                                    ]
                                },
                                {
                                    handle: [
                                        {
                                            handler: 'rewrite',
                                            uri: '{http.matchers.file.relative}'
                                        }
                                    ],
                                    match: [
                                        {
                                            file: {
                                                try_files: [
                                                    '{http.request.uri.path}',
                                                    '{http.request.uri.path}/',
                                                    '/index.html'
                                                ]
                                            }
                                        }
                                    ]
                                },
                                {
                                    handle: [
                                        {
                                            handler: 'file_server'
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    terminal: true
                }
            ]
        }

        return {config: config, ok: true}
    }

    const handlePathChange = path => {
        setPath(path)
        if (path && pathError)
            setPathError(false)
    }

    return (
        <>
            <TextField
                required
                error={pathError}
                helperText={pathError ? '路径不能为空' : ''}
                label={'前端路径'}
                value={path}
                onChange={e => handlePathChange(e.target.value)}/>
        </>
    )
})
