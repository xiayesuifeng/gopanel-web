import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import React from 'react'
import TextField from '@mui/material/TextField'

const providers = ['alidns', 'azure', 'cloudflare', 'digitalocean', 'dnspod', 'duckdns',
    'gandi', 'godaddy', 'googleclouddns', 'hetzner', 'lego_deprecated', 'metaname', 'netcup',
    'openstack-designate', 'tencentcloud', 'route53', 'vultr']

function AlidnsProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Access Key ID"
                       helperText="访问阿里云 API 所需的API Key ID"
                       margin={'normal'}
                       value={value.access_key_id ?? ''}
                       onChange={e => onChange({...value, access_key_id: e.target.value})}/>
            <TextField label="Access Key Secret"
                       helperText="访问阿里云 API 所需的 API Key Secret"
                       margin={'normal'}
                       value={value.access_key_secret ?? ''}
                       onChange={e => onChange({...value, access_key_secret: e.target.value})}/>
            <TextField label="Region ID"
                       helperText="(可选)阿里云服务的区域，默认为 zh-hangzhou"
                       margin={'normal'}
                       value={value.region_id ?? ''}
                       onChange={e => onChange({...value, region_id: e.target.value})}/>
        </>
    )
}

function AzureProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Tenant ID"
                       margin={'normal'}
                       value={value.tenant_id ?? ''}
                       onChange={e => onChange({...value, tenant_id: e.target.value})}/>
            <TextField label="Client ID"
                       margin={'normal'}
                       value={value.client_id ?? ''}
                       onChange={e => onChange({...value, client_id: e.target.value})}/>
            <TextField label="Client Secret"
                       margin={'normal'}
                       value={value.client_secret ?? ''}
                       onChange={e => onChange({...value, client_secret: e.target.value})}/>
            <TextField label="Subscription ID"
                       margin={'normal'}
                       value={value.subscription_id ?? ''}
                       onChange={e => onChange({...value, subscription_id: e.target.value})}/>
            <TextField label="Resource Group Name"
                       margin={'normal'}
                       value={value.resource_group_name ?? ''}
                       onChange={e => onChange({...value, resource_group_name: e.target.value})}/>
        </>
    )
}

function CloudflareProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="API Token"
                       helperText={'API 令牌用于身份验证。确保使用范围 API 令牌，而不是全局 API 密钥。它将需要两个权限：Zone-Zone-Read 和 Zone-DNS-Edit，除非您只使用GetRecords()，在这种情况下，第二个可以更改为 Read。'}
                       margin={'normal'}
                       value={value.api_token ?? ''}
                       onChange={e => onChange({...value, api_token: e.target.value})}/>
        </>
    )
}

function DigitaloceanProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Auth Token"
                       helperText={'Auth Token 是 DigitalOcean API 令牌 - 请参阅 https://www.digitalocean.com/docs/apis-clis/api/create-personal-access-token/'}
                       margin={'normal'}
                       value={value.auth_token ?? ''}
                       onChange={e => onChange({...value, auth_token: e.target.value})}/>
        </>
    )
}

function DnspodProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Auth Token"
                       helperText={'Auth Token 是 DNSPOD API 令牌 - 参见 https://www.dnspod.cn/docs/info.html#common-parameters'}
                       margin={'normal'}
                       value={value.auth_token ?? ''}
                       onChange={e => onChange({...value, auth_token: e.target.value})}/>
        </>
    )
}

function DuckdnsProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="API Token"
                       margin={'normal'}
                       value={value.api_token ?? ''}
                       onChange={e => onChange({...value, api_token: e.target.value})}/>
            <TextField label="Override Domain"
                       margin={'normal'}
                       value={value.override_domain ?? ''}
                       onChange={e => onChange({...value, override_domain: e.target.value})}/>
        </>
    )
}

function GandiProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="API Token"
                       margin={'normal'}
                       value={value.api_token ?? ''}
                       onChange={e => onChange({...value, api_token: e.target.value})}/>
        </>
    )
}

function GoogleclouddnsProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="GCP Project"
                       margin={'normal'}
                       value={value.gcp_project ?? ''}
                       onChange={e => onChange({gcp_project: e.target.value})}/>
            <TextField label="GCP Application Default"
                       margin={'normal'}
                       value={value.gcp_application_default ?? ''}
                       onChange={e => onChange({...value, gcp_application_default: e.target.value})}/>
        </>
    )
}

function HetznerProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Auth API Token"
                       helperText={'Auth API Token 是 Hetzner 身份验证 API 令牌 - 请参阅 https://dns.hetzner.com/api-docs#section/Authentication/Auth-API-Token'}
                       margin={'normal'}
                       value={value.auth_api_token ?? ''}
                       onChange={e => onChange({...value, auth_api_token: e.target.value})}/>
        </>
    )
}

function LegoDeprecatedProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Provider Name"
                       margin={'normal'}
                       value={value.provider_name ?? ''}
                       onChange={e => onChange({...value, provider_name: e.target.value})}/>
        </>
    )
}

function MetanameProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="API Key"
                       margin={'normal'}
                       value={value.api_key ?? ''}
                       onChange={e => onChange({...value, api_key: e.target.value})}/>
            <TextField label="Account Reference"
                       margin={'normal'}
                       value={value.account_reference ?? ''}
                       onChange={e => onChange({...value, account_reference: e.target.value})}/>
            <TextField label="Endpoint"
                       margin={'normal'}
                       value={value.endpoint ?? ''}
                       onChange={e => onChange({...value, endpoint: e.target.value})}/>
        </>
    )
}

function NetcupProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Customer Number"
                       margin={'normal'}
                       value={value.customer_number ?? ''}
                       onChange={e => onChange({...value, customer_number: e.target.value})}/>
            <TextField label="API Key"
                       margin={'normal'}
                       value={value.api_key ?? ''}
                       onChange={e => onChange({...value, api_key: e.target.value})}/>
            <TextField label="API Password"
                       margin={'normal'}
                       value={value.api_password ?? ''}
                       onChange={e => onChange({...value, api_password: e.target.value})}/>
        </>
    )
}

function OpenstackDesignateProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="auth_open_stack"
                       margin={'normal'}
                       value={JSON.stringify(value.auth_open_stack) ?? '{}'}
                       onChange={e => onChange({...value, auth_open_stack: JSON.parse(e.target.value)})}/>
        </>
    )
}

function Route53ProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Max Retries"
                       margin={'normal'}
                       value={value.max_retries ?? ''}
                       onChange={e => onChange({...value, max_retries: e.target.value})}/>
        </>
    )
}

function TencentcloudProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Secret ID"
                       margin={'normal'}
                       value={value.SecretId ?? ''}
                       onChange={e => onChange({...value, SecretId: e.target.value})}/>
            <TextField label="Secret Key"
                       margin={'normal'}
                       value={value.SecretKey ?? ''}
                       onChange={e => onChange({...value, SecretKey: e.target.value})}/>
        </>
    )
}

function Vultr53ProviderItem (props) {
    const {value, onChange} = props

    return (
        <>
            <TextField label="Auth Token"
                       helperText={'Auth Token 是 Vultr API 令牌，请参阅 https://my.vultr.com/settings/#settingsapi'}
                       margin={'normal'}
                       value={value.auth_token ?? ''}
                       onChange={e => onChange({...value, auth_token: e.target.value})}/>
        </>
    )
}

export default function DNSProviderEditor (props) {
    const {provider, onProviderChange} = props
    const {providerOption, onProviderOptionChange} = props
    const {disabled} = props

    const getProviderOptionItem = (name) => {
        switch (name) {
            case 'alidns':
                return (<AlidnsProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'azure':
                return (<AzureProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'cloudflare':
                return (<CloudflareProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'digitalocean':
                return (<DigitaloceanProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'dnspod':
                return (<DnspodProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'duckdns':
                return (<DuckdnsProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'gandi':
                return (<GandiProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'googleclouddns':
                return (<GoogleclouddnsProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'hetzner':
                return (<HetznerProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'lego_deprecated':
                return (<LegoDeprecatedProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'metaname':
                return (<MetanameProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'netcup':
                return (<NetcupProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case'openstack-designate':
                return (<OpenstackDesignateProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'tencentcloud':
                return (<TencentcloudProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'route53':
                return (<Route53ProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
            case 'vultr':
                return (<Vultr53ProviderItem value={providerOption} onChange={onProviderOptionChange}/>)
        }
    }

    return (
        <>
            <FormControl margin={'normal'}>
                <InputLabel>提供商</InputLabel>
                <Select
                    value={provider}
                    label="提供商"
                    disabled={disabled}
                    autoWidth
                    sx={{minWidth: 230}}
                    onChange={e => {
                        onProviderChange(e.target.value)
                        onProviderOptionChange({})
                    }}
                >
                    {providers.map(provider => (
                        <MenuItem key={provider} value={provider}>{provider}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            {!disabled && getProviderOptionItem(provider)}
        </>
    )
}
