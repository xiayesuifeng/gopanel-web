import React, { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../globalState/Context'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { ResponsivePie } from '@nivo/pie'

export default function AppOverview () {
    const {apps} = useContext(GlobalContext)
    const [data, setData] = useState([])
    const [goCount, setGoCount] = useState(0)
    const [javaCount, setJavaCount] = useState(0)
    const [phpCount, setPhpCount] = useState(0)

    useEffect(() => {
        let goCount = 0
        let javaCount = 0
        let phpCount = 0
        let otherCount = 0
        let reverseProxyCount = 0
        let staticWebCount = 0
        apps.forEach(app => {
            switch (app.type) {
                case 1:
                    goCount++
                    break
                case 2:
                    javaCount++
                    break
                case 3:
                    phpCount++
                    break
                case 4:
                    otherCount++
                    break
                case 5:
                    reverseProxyCount++
                    break
                case 6:
                    staticWebCount++
                    break
                default:
                    break
            }
        })

        setGoCount(goCount)
        setJavaCount(javaCount)
        setPhpCount(phpCount)

        setData([
            {
                id: 'Go',
                label: 'Go',
                value: goCount
            },
            {
                id: 'Java',
                label: 'Java',
                value: javaCount
            },
            {
                id: 'PHP',
                label: 'PHP',
                value: phpCount
            },
            {
                id: '反向代理',
                label: '反向代理',
                value: reverseProxyCount
            },
            {
                id: '静态网页',
                label: '静态网页',
                value: staticWebCount
            },
            {
                id: '其他',
                label: '其他',
                value: otherCount
            }
        ])
    }, [apps])

    return (
        <Box display="flex" alignItems="center" justifyContent="center" flexWrap="wrap">
            <Box display="flex" justifyContent="space-between">
                <Box display="flex" flexDirection="column" alignItems="flex-start">
                    <Typography>总应用数：</Typography>
                    <Typography>GO 应用数：</Typography>
                </Box>
                <Box display="flex" flexDirection="column">
                    <Typography>{apps.length}</Typography>
                    <Typography>{goCount}</Typography>
                </Box>
            </Box>
            <Box sx={{width: 450, height: 300}}>
                <ResponsivePie
                    data={data}
                    margin={{top: 40, right: 40, bottom: 80, left: 40}}
                    innerRadius={0.5}
                    padAngle={0.7}
                    cornerRadius={3}
                    activeOuterRadiusOffset={8}
                    borderWidth={1}
                    borderColor={{from: 'color', modifiers: [['darker', 0.2]]}}
                    arcLinkLabelsSkipAngle={10}
                    arcLinkLabelsTextColor="#333333"
                    arcLinkLabelsThickness={2}
                    arcLinkLabelsColor={{from: 'color'}}
                    arcLabelsSkipAngle={10}
                    arcLabelsTextColor={{from: 'color', modifiers: [['darker', 2]]}}
                    valueFormat={(value) => `${Number(value)} 个`}
                    legends={[
                        {
                            anchor: 'bottom',
                            direction: 'row',
                            justify: false,
                            translateX: 0,
                            translateY: 56,
                            itemsSpacing: 0,
                            itemWidth: 90,
                            itemHeight: 18,
                            itemTextColor: '#999',
                            itemDirection: 'left-to-right',
                            itemOpacity: 1,
                            symbolSize: 15,
                            symbolShape: 'circle',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemTextColor: '#000',
                                    },
                                },
                            ],
                        },
                    ]}
                />
            </Box>
            <Box display="flex" justifyContent="space-between">
                <Box display="flex" flexDirection="column" alignItems="flex-start">
                    <Typography>Java 应用数：</Typography>
                    <Typography>PHP 应用数：</Typography>
                </Box>
                <Box display="flex" flexDirection="column">
                    <Typography>{javaCount}</Typography>
                    <Typography>{phpCount}</Typography>
                </Box>
            </Box>
        </Box>
    )
}