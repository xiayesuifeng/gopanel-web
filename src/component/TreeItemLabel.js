import React from 'react'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'

export default function TreeItemLabel (props) {
    return (<Box display={'flex'} alignItems={'center'} sx={{p: theme => theme.spacing(1, 1,1,0)}}>
        <Typography sx={{flexGrow: 1}}>{props.label}</Typography>
        <div onClick={event => event.stopPropagation()}
             onFocus={event => event.stopPropagation()}>
            {props.action}
        </div>
    </Box>)
}