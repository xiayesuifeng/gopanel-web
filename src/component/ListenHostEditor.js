import Paper from '@mui/material/Paper'
import MenuItem from '@mui/material/MenuItem'
import InputAdornment from '@mui/material/InputAdornment'
import InputBase from '@mui/material/InputBase'
import Select from '@mui/material/Select'
import FormControl from '@mui/material/FormControl'
import { useContext } from 'react'
import { GlobalContext } from '../globalState/Context'

export default function ListenHostEditor (props) {
    const {disableSSL, domain, port, onDisableSSLChange, onDomainChange, onPortChange} = props

    const {configuration, globalDispatch} = useContext(GlobalContext)

    return (
        <Paper
            component="form"
            variant={'outlined'}
            sx={{p: '8px 10px', display: 'flex', alignItems: 'center'}}
        >
            <FormControl>
                <Select
                    input={<InputBase/>}
                    value={disableSSL ? 'http' : 'https'}
                    onChange={e => onDisableSSLChange(e.target.value === 'http')}
                >
                    <MenuItem value={'https'}>https://</MenuItem>
                    <MenuItem value={'http'}>http://</MenuItem>
                </Select>
            </FormControl>
            <InputBase
                sx={{ml: 1, flex: 1}}
                placeholder="域名"
                value={domain}
                onChange={e => onDomainChange(e.target.value)}
                endAdornment={<InputAdornment position="end">:</InputAdornment>}
            />
            <InputBase
                sx={{ml: 1, width: 100}}
                placeholder={disableSSL ? `${configuration.general.HTTPPort}` : `${configuration.general.HTTPSPort}`}
                value={port ?? ''}
                type="number"
                onChange={e => onPortChange(e.target.value)}
            />
        </Paper>
    )
}