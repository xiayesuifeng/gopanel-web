const {createProxyMiddleware} = require('http-proxy-middleware')
module.exports = function (app) {
    app.use(createProxyMiddleware({
            target: 'http://localhost:8080',
            pathFilter: '/api',
            ws: true
        })
    )
    app.use(
        createProxyMiddleware({
            target: 'http://localhost:2020',
            pathFilter: '/netdata',
            ws: true
        })
    )
}