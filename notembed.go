//go:build !web

package web

import (
	"embed"
	"io/fs"
)

var assets embed.FS

func Assets() fs.FS {
	return assets
}
