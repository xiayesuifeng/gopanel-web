//go:build web

package web

import (
	"embed"
	"io/fs"
)

//go:embed build/*
var assets embed.FS

func Assets() fs.FS {
	webFS, _ := fs.Sub(assets, "build")
	return webFS
}
