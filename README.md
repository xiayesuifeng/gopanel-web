# gopanel-web

> Gopanel 前端

[![pipeline status](https://gitlab.com/xiayesuifeng/gopanel-web/badges/master/pipeline.svg)](https://gitlab.com/xiayesuifeng/gopanel-web/commits/master)
![license](https://img.shields.io/badge/license-GPL3.0-green.svg)

## 后端

[gopanel](https://gitlab.com/xiayesuifeng/gopanel.git)

## 编译

```
git clone https://gitlab.com/xiayesuifeng/gopanel-web.git
npm install
npm run build
```

## License

gopanel-web is licensed under [GPLv3](LICENSE).